<img src='./cours_phelma/logo.png' height='100'/> 

# Cours 2A SICOM - Introduction à l'intelligence artificielle - 4PMSIIA4

## Objectifs

Ce cours est une introduction à l'intelligence artificielle : il présente les grandes lignes de l'apprentissage supervisé, que ce soit la régression ou la classification. Il a pour objectif de présenter des méthodes et algorithmes de bases. A la fin de ce cours, les étudiant·es seront capables de mettre en place une chaîne simple d'IA dédiée à la régression ou la classification et de déployer le modèle sur un dispositif cible de type raspberry pi.

## Contenu

Ce cours présente : la régression linéaire, la classification par SVM, des architectures simples de réseaux de neurones (perceptron, perceptron multi-couches, réseaux convolutifs, auto-encodeurs et auto-encodeurs variationnels). L'importance des données d'apprentissage est également abordée au travers du biais dans les données, de classes déséquilibrées, de validations croisées, etc.

## Prérequis

- Cours de calcul scientifique et traitement numérique du signal.
- Notion d'optimisation, de convolution numérique et corrélation numérique.
- Notion de programmation en python.

<img src='https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-nc.svg' height='31'/>

