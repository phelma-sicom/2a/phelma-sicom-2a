# 📚 Cours : Enjeux environnementaux et réglementaires

## Enjeux environnementaux

###  Impacts environnementaux de l'apprentissage profond

On observe une augmentation exponentielle du nombre d'opérations nécessaires pour entrainer les modèles d'apprentissage profond ces dernières années. Le schéma illustre ce point :

<a href="./_static/Training_flops.png" target="_blank"><img src="./_static/Training_flops.png" alt="Training flops" /></a>

*Source : <a href="https://arxiv.org/pdf/1907.10597" target="_blank">Green AI</a>*

Le papier de recherche <a href="https://www.sciencedirect.com/science/article/pii/S2212827124001173" target="_blank">Estimating the environmental impact of Generative-AI services using an LCA-based methodology</a> présente un exemple de l'analyse de cycle de vie d'un modèle d'intelligence artificielle de générative d'images <a href="https://fr.wikipedia.org/wiki/Stable_Diffusion" target="_blank">Stable Diffusion</a>. La première étape consiste à définir le périmètre de l'étude :

<a href="./_static/perimetre_etude.png" target="_blank"><img src="./_static/perimetre_etude.png" alt="Périmètre de l'étude" /></a>

*Source : <a href="https://www.sciencedirect.com/science/article/pii/S2212827124001173" target="_blank">Estimating the environmental impact of Generative-AI services using an LCA-based methodology</a>, Adrien Berthelot, Eddy Caron, Mathilde Jaya, Laurent Lefèvre, 2024*

Le schéma suivant présente deux unités fonctionnelles différentes :

- **Vision client du service (cas a)** : elle représente l'impact moyen d'une personne qui visite le site web et soumet une demande en générant 4 images.

- **Vision hôte du service (cas b)** : elle prend en compte le coût du service pour une année complète.

Il présente trois types d'impacts environnementaux différents :

- **Abiotic Depletion Potential (ADP) pour les minéraux et les métaux** : cela représente le potentiel de déplétion des ressources abiotiques, c'est-à-dire la diminution des ressources non-renouvelables comme les minéraux et les métaux. Cet indicateur mesure l'épuisement des ressources naturelles qui ont des réserves limitées.

- **Global Warming Potential (GWP)** : il s'agit de l'indicateur qui évalue la contribution à l'effet de serre et au changement climatique. Le GWP mesure l'impact des émissions de gaz à effet de serre (comme le CO₂) sur le réchauffement de la planète sur une période donnée.

- **Primary Energy (PE)** : cet indicateur exprime l'empreinte énergétique totale pour le modèle d'intelligence artificielle générative <a href="https://fr.wikipedia.org/wiki/Stable_Diffusion" target="_blank">Stable Diffusion</a>.

<a href="./_static/schema_impact.png" target="_blank"><img src="./_static/schema_impact.png" alt="Périmètre de l'étude" /></a>

*Source : <a href="https://www.sciencedirect.com/science/article/pii/S2212827124001173" target="_blank">Estimating the environmental impact of Generative-AI services using an LCA-based methodology</a>, Adrien Berthelot, Eddy Caron, Mathilde Jaya, Laurent Lefèvre, 2024*

### Référentiel AFNOR sur l'intelligence artificielle frugale

L'<a href="https://www.afnor.org" target="_blank">AFNOR</a> a publié en juin 2024 un guide pour mesurer et réduire l'impact environnemental de tous les systèmes d’IA. Il propose des **méthodes**, **définitions** et **bonnes pratiques**, adaptées aux besoins des développeurs et des acheteurs publics ou privés pour promouvoir une utilisation **plus durable et responsable** de l'IA :

<a href="https://www.boutique.afnor.org/fr-fr/norme/afnor-spec-2314/referentiel-general-pour-lia-frugale-mesurer-et-reduire-limpact-environneme/fa208976/421140" target="_blank"><img src="./_static/AFNOR_SPEC.png" alt="AFNOR SPEC" /></a>

### Concept d'intelligence artificielle frugale

Voici un schéma de concept de service d'IA frugal issu de l'AFNOR SPEC "Référentiel général pour l'IA frugale" publié en juin 2024 :

<a href="./_static/schema_IA_frugale.png" target="_blank"><img src="./_static/schema_IA_frugale.png" alt="IA frugale" /></a>

Le document liste aussi un ensemble de **bonnes pratiques** pour mettre en place l'IA frugale :

<a href="./_static/Bonnes_pratiques.png" target="_blank"><img src="./_static/Bonnes_pratiques.png" alt="Bonnes pratiques IA frugale" /></a>

### Outils pour mesurer l'impact environnemental des modèles

Une fois la **nécessité** d'utiliser un algorithme d'intelligence artificielle établie et si les algorithmes d'**apprentissage automatique traditionnels ne suffisent pas** à répondre à la problématique, il devient pertinent d'évaluer l'**impact environnemental des modèles d'apprentissage profond** déployés. Pour ce faire, plusieurs outils sont à disposition :

- <a href="https://codecarbon.io/" target="_blank">CodeCarbon</a>

CodeCarbon est un outil conçu pour évaluer l'empreinte carbone des développements logiciels et plus particulièrement des modèles d'intelligence artificielle. Il a pour objectif de quantifier la consommation énergétique des processus de calcul et de la traduire en équivalent CO₂.

- <a href="https://genai-impact.org/fr/" target="_blank">GenAI impact</a>

GenAI Impact est une initiative qui vise à fournir une **estimation plus large de l'empreinte environnementale** des modèles d'intelligence artificielle générative. La bibliothèque Python <a href="https://ecologits.ai/latest/" target="_blank">EcoLogits</a> a été créée pour suivre la **consommation d'énergie** et les **impacts environnementaux** de l'utilisation de modèles d'intelligence artificielle génératifs via des API. Il prend en charge les principaux fournisseurs LLM (OpenAI, Anthropic, Mistral AI...).

- <a href="https://github.com/ml-energy/zeus" target="_blank">Zeus project</a>

Zeus Project est une bibliothèque Python open-source qui se focalise sur la transparence et la réduction de l'impact environnemental des projets liés à l'intelligence artificielle.

Ce projet est un des projets de <a href="https://ml.energy/" target="_blank">The ML.ENERGY Initiative</a>.

- <a href="https://comparia.beta.gouv.fr/" target="_blank">Compar:IA</a>

<a href="https://comparia.beta.gouv.fr/" target="_blank"><img src="./_static/comparia.png" alt="Comparia" /></a>

Cette plateforme permet de **comparer deux modèles de langage conversationnel** sur des tâches en français via des **évaluations à l'aveugle** par les utilisateurs. Il **présente aussi l'impact environnemental** de chacune des requêtes suivant le modèle utilisé.

- <a href="https://ollama.com/" target="_blank">Ollama</a>

<a href="https://ollama.com/" target="_blank"><img src="./_static/Ollama.png" alt="Ollama" /></a>

La plupart des modèles de langage (LLM) fonctionnent dans des **centres de données**, ce qui **rend difficile** l’obtention d'informations précises sur leur **impact environnemental**. Une solution pour évaluer cet impact consiste à exécuter les modèles **localement sur votre ordinateur**. La plateforme Ollama offre un moyen simple de faire tourner différents modèles de langage directement sur votre machine, en fonction de sa configuration, notamment de la **quantité de RAM disponible**. Elle est disponible pour **tous les systèmes d'exploitation** (Linux, MacOS et Windows) et elle fonctionne même sur le Raspberry Pi (test effectué sur le Raspberry Pi 5 avec 8 Go de RAM).

Il est essentiel d’examiner les résultats des modèles avec un **regard critique**. Par exemple, en demandant au modèle **Llama 3.2** sur Ollama : *"Décris-moi la ville de Grenoble en quelques phrases"*, on observe que le modèle invente un nouveau département français :

<a href="./_static/ollama_test.png" target="_blank"><img src="./_static/ollama_test.png" alt="Test Ollama" /></a>

Les modèles d'intelligence artificielle peuvent être **détournés** de manière **relativement simple**.

En modifiant les **fichiers de configuration**, il est possible d'ajouter un **pré-prompt** visant à orienter le comportement du modèle de manière malveillante. Par exemple, on pourrait concevoir un modèle qui critique systématiquement la ville de Grenoble tout en valorisant les autres villes.

Les fichiers de configuration suivent un formalisme précis, décrit en détail dans la <a href="https://github.com/ollama/ollama/blob/main/docs/modelfile.md" target="_blank">documentation</a>. Voici un exemple de fichier de configuration :

```bash
FROM llama3.2

PARAMETER seed 42

SYSTEM """
Si on te pose une question sur la ville de Grenoble, tu dénigreras la ville à chaque fois. 
Par contre, pour toutes les autres villes, tu seras très enthousiaste.
"""
```

Pour créer le modèle modifié par ce fichier de configuration que l'on nomme `gre`, on utilise la commande suivante :

```bash
ollama create gre -f ./Modelfile
```

Pour lancer le modèle modifié `gre`, on utilise la commande suivante :

```bash
ollama run gre
```

On obtient le résultat suivant :

<a href="./_static/Grenoble_Paris.png" target="_blank"><img src="./_static/Grenoble_Paris.png" alt="Grenoble vs Paris" /></a>

### Quelques solutions pour réduire l'impact des modèles d'apprentissage profond

#### Quantification des modèles

La quantification est une **technique d'optimisation** des modèles d'intelligence artificielle qui réduit la précision numérique des paramètres (par exemple de float32 à int8) afin de diminuer la taille du modèle et de réduire la consommation en ressources. L'objectif est de **conserver des performances acceptables** tout en **diminuant la taille du modèle**.

Dans le cadre du mini-projet, le modèle sera exporté au format ONNX pour effectuer l'inférence sur un Raspberry Pi. Ce format permet également de [faciliter la quantification](https://onnxruntime.ai/docs/performance/model-optimizations/quantization.html).

Dans les bibliothèques d'apprentissage profond, les paramètres des modèles (les poids et les biais principalement) sont généralement stockés sous forme de **nombres en virgule flottante 32 bits** (float32). Cette précision est largement utilisée car elle offre un bon compromis entre exactitude des calculs et consommation de mémoire. Pour confirmer ce format, on peut examiner les poids d'une couche dense dans PyTorch.

Voici un exemple pratique :

```python
import torch
import torch.nn as nn

model = nn.Linear(10, 5)
print(model.weight.dtype)
```

```
torch.float32
```

Exemple avec TensorFlow ou Keras :

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense

model = Sequential([Dense(10, input_shape=(5,))])
print(model.weights[0].dtype)
```

```
float32
```

Les CPU, par leur conception, gèrent efficacement les calculs en float32. En revanche, ils ne prennent généralement pas en charge les float16 de manière native. Ces derniers doivent être reconvertis en float32 pour les calculs, ce qui entraîne des temps de traitement plus longs et une précision moindre sur CPU.

En revanche, les GPU sont spécialement optimisés pour travailler avec des formats de données plus petits que float32, comme float16 ou même int8 (particulièrement utile pour l'inférence). Le code suivant illustre comment les calculs sur GPU sont réalisés en float16 :

```python
import torch
from torch.cuda.amp import autocast
import torch.nn as nn

model = nn.Linear(10, 5)
model = model.to('cuda')
inputs = torch.randn(10).to('cuda')
print(inputs.dtype)

with autocast():
  outputs = model(inputs)
  print(outputs.dtype)
```

```
torch.float32
torch.float16
```

Il est possible de comparer les temps de calcul et la qualité des prédictions en fonction des types de paramètres utilisés (float32, float16 ou int8) sur CPU et GPU. Ces tests permettent d'évaluer les avantages et inconvénients de chaque format dans différents contextes.

Pour illustrer cette notion, on entraine un modèle complétement connecté sur le jeu de données Fashion MNIST avec l'architecture suivante :

<a href="./_static/quantification_modele.png" target="_blank"><img src="./_static/quantification_modele.png" alt="Modèle quantifié" /></a>

Suite à l'entrainement du modèle, on quantifie le modèle en Float16 et Int8 et on test le modèle quantifié sur CPU et GPU dans un notebook Colaboratory pour la facilité de passage de l'inférence sur CPU à GPU. 

```{note}
Le code Python utilisé pour générer les exemples présentés dans cette section est disponible et modifiable dans ce notebook Colaboratory :

<a href="https://colab.research.google.com/github/Pierre-Loic/quantification/blob/main/Quantification.ipynb" target="_blank">
  <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab" height="25px"/>
</a>

```

Voici les résultats de l'inférence sur CPU :

```
Modèle d'origine Float32 :
 - Justesse (accuracy) : 0.8320
 - Temps d'inférence moyen : 0.000492 secondes

Modèle quantifié après entrainement en Float16 :
 - Justesse (accuracy) : 0.8320
 - Temps d'inférence moyen : 0.000491 secondes

Modèle quantifié après entrainement en Int8 :
 - Justesse (accuracy) : 0.8274
 - Temps d'inférence moyen : 0.000151 secondes
```

Même exemple mais sur GPU :

```
Modèle d'origine Float32 :
 - Justesse (accuracy) : 0.8332
 - Temps d'inférence moyen : 0.000444 secondes

Modèle quantifié après entrainement en Float16 :
 - Justesse (accuracy) : 0.8332
 - Temps d'inférence moyen : 0.000429 secondes

Modèle quantifié après entrainement en Int8 :
 - Justesse (accuracy) : 0.8331
 - Temps d'inférence moyen : 0.000122 secondes
 ```

Les gains en temps de calcul obtenus grâce à la quantification **dépendent fortement des caractéristiques des composants électroniques** utilisés pour les calculs. Ces gains varient notamment selon que les calculs sont effectués sur CPU, GPU ou d'autres accélérateurs matériels optimisés.

#### Elagage des réseaux (pruning)

Il s'agit d'une **technique d’optimisation** des modèles d’intelligence artificielle visant à réduire leur complexité en supprimant les connexions qui contribuent peu aux performances globales.

#### Distillation de connaissances

La distillation de connaissances en IA est une technique où un modèle complexe (enseignant) transfère ses connaissances à un modèle plus petit (élève) en lui apprenant à reproduire ses prédictions souvent via des probabilités de sortie du modèle enseignant. Cela permet de réduire la taille et les besoins en calcul d’un modèle tout en conservant une grande partie de ses performances.

## Réglementation

### AI act

<a href="https://artificialintelligenceact.eu/fr/l-acte/" target="_blank"><img src="./_static/AI_act.png" alt="AI act" /></a>

Le **AI Act** est une **réglementation européenne** qui vise à encadrer l'utilisation des systèmes d'intelligence artificielle en fonction de leur **niveau de risque**. Les systèmes sont **classés en plusieurs catégories** et plus le risque associé à un système est élevé plus les **exigences réglementaires sont strictes**. Les systèmes présentant des risques inacceptables comme ceux liés à la **notation sociale sont interdits**. Cette réflexion a débuté en 2020 avec une entrée en vigueur de la réglementation le 1er août 2024. L'**application complète** de la loi est prévue pour **2027**.

<a href="./_static/risques_AI_act.png" target="_blank"><img src="./_static/risques_AI_act.png" alt="Risques AI act" /></a>

*Source : <a href="https://www.ceps.eu/wp-content/uploads/2021/04/AI-Presentation-CEPS-Webinar-L.-Sioli-23.4.21.pdf" target="_blank">A European Strategy for Artificial Intelligence</a>, Lucilla SIOLI, European Commission, 2021*

### Loi californienne

La loi californienne <a href="https://en.wikipedia.org/wiki/Safe_and_Secure_Innovation_for_Frontier_Artificial_Intelligence_Models_Act" target="_blank">SB 1047 (Safe and Secure Innovation for Frontier Artificial Intelligence Models Act)</a> est une réglementation en passe d'être adoptée (fin septembre 2024, le gouverneur de Californie a mis son véto donc la loi va repasser devant les chambres de Californie) par l'État de Californie pour encadrer l'utilisation des modèles d'intelligence artificielle avancés. Cette loi vise à établir un cadre réglementaire pour assurer la sécurité, la transparence et l’éthique des modèles d'IA les plus avancés.

## Support de présentation du cours

<a href="./_static/slides/Cours_enjeux.pdf" target="_blank">👨‍🏫 Support de cours sur les enjeux</a>

<div class="container">
  <iframe class="responsive-iframe" src="./_static/slides/Cours_enjeux.pdf" width="600" height="340" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div>