<style>

   .iframe-wrapper {
  max-width: 60%;
  margin: auto; /* pour centrer le conteneur */
}

    /* Pour la gestion des iframes */
 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 📚 Cours : Les étapes de l'entrainement des modèles

L'apprentissage automatique s'appuie sur les données. La première étape pour contruire un modèle de machine learning est donc de collecter les données.

## La collecte des données

La **collecte des données** est une **étape cruciale** dans le processus d'entrainement de modèles de machine learning. Elle implique la **récupération** et l'**organisation de données** provenant de diverses sources. Cette section explore les différents types de données et les méthodes de collecte.

### Les différentes formes de données

Les données peuvent généralement être classées en **deux catégories** :

#### Données structurées

Ces données sont organisées de manière systématique et peuvent être facilement stockées et interrogées. L'**accès** à l'**information utile** est **simple**. Voici quelques exemples de données structurées :

- Bases de données relationelles (<a href="https://fr.wikipedia.org/wiki/Open_source" target="_blank">open source</a>) : PostgreSQL, MySQL, MariaDB, SQLite
- Fichiers plats de données : CSV, TSV
- Fichiers hiérarchiques de données (semi-structurées) : <a href="https://fr.wikipedia.org/wiki/JavaScript_Object_Notation" target="_blank">JSON</a>, <a href="https://fr.wikipedia.org/wiki/Extensible_Markup_Language" target="_blank">XML</a>
- Feuilles de calculs de tableurs
- ...

#### Données non structurées

À l'opposé, les données non structurées **ne suivent pas un modèle** ou une **structure prédéfinis**. L'information utile doit en être extraite. Voici quelques exemples de données **non structurées** :

- Textes
- Images
- Sons
- Vidéos
- ...

Cette **distinction** entre données **structurées** et **non structurées** est **très importante en intelligence artificielle** car les données non structurées nécessitent l'utilisation de **réseaux de neurones profonds** (deep learning) pour fournir des résultats intéressants. Par contre, les données structurées ne nécessitent pas en général d'utiliser des modèles de réseaux de neurones : les modèles d'apprentissage automatique sans réseaux de neurones ont souvent un meilleur compromis entre la performances et le temps de calculs.

```{admonition} Cas particulier des bases de données No-SQL

Les bases de données <a href="https://fr.wikipedia.org/wiki/NoSQL" target="_blank">NoSQL</a> peuvent gérer à la fois **des données structurées et non structurées** mais elles sont particulièrement réputées pour leur efficacité dans la gestion de **grandes quantités de données non structurées et hétérogènes**. Voici les principaux types de bases de données NoSQL :

- **Bases de données orientées document**

Ces bases de données stockent les données sous forme de **documents** généralement en format <a href="https://fr.wikipedia.org/wiki/JavaScript_Object_Notation" target="_blank">JSON</a> ou <a href="https://fr.wikipedia.org/wiki/Extensible_Markup_Language" target="_blank">XML</a>. Chaque document peut avoir une **structure différente**.

*Exemples en <a href="https://fr.wikipedia.org/wiki/Open_source" target="_blank">open source</a> : <a href="https://www.mongodb.com/fr-fr" target="_blank">MongoDB</a>, <a href="https://couchdb.apache.org/" target="_blank">CouchDB</a>*

- **Bases de données orientées colonnes**

Ces bases de données stockent les données **par colonnes** plutôt que par lignes ce qui est optimal pour les requêtes sur de **grands volumes de données** et pour la **compression des données**.

*Exemples en <a href="https://fr.wikipedia.org/wiki/Open_source" target="_blank">open source</a> : <a href="https://cassandra.apache.org/_/index.html" target="_blank">Cassandra</a>, <a href="https://hbase.apache.org/" target="_blank">HBase</a>*

- **Bases de données orientées clé-valeur**

Dans ces bases de données, chaque élément est stocké comme une paire **clé-valeur**. Elles sont **simples** et généralement **très performantes** pour les requêtes basées sur la clé.

*Exemples en <a href="https://fr.wikipedia.org/wiki/Open_source" target="_blank">open source</a> : <a href="https://redis.io/" target="_blank">Redis</a>, <a href="https://rocksdb.org/" target="_blank">RocksDB</a>*

- **Bases de données orientées graphes**

Ces bases de données sont optimisées pour stocker et naviguer dans des **relations complexes**. Elles sont particulièrement utiles pour les réseaux sociaux et les systèmes de recommandation.

*Exemples en <a href="https://fr.wikipedia.org/wiki/Open_source" target="_blank">open source</a> : <a href="https://neo4j.com/fr/" target="_blank">Neo4j</a>, <a href="https://janusgraph.org/" target="_blank">JanusGraph</a>*

🐍 Toutes ces bases de données sont **utilisables en Python** grâce à des bibliothèques externes.
```

### Les différentes manières de récupérer les données

La **récupération de données** peut être réalisée par **plusieurs méthodes** en fonction de la nature et de la source des données. Parmi les techniques les plus courantes, on trouve le Web scraping, l'utilisation de bases de données et l'accès via des API.

- *Web Scraping*

Cette technique implique l'extraction de données à partir de sites Web. Elle est particulièrement utile pour récupérer des informations publiques disponibles sur Internet. Le Web scraping utilise des outils et des scripts automatisés pour parcourir des pages Web, en extrayant et en stockant les données souhaitées.

- *Bases de données relationnelles et non relationnelles*

La récupération des données se fait soit via des requêtes SQL, soit via du code avec des ORM (Object-Relational Mapping).

- *API (application programming interface)*

Les API permettent d'accéder à des données via des **requêtes sur des services Web**. Elles sont particulièrement utiles pour récupérer des données en **temps réel** ou des données provenant de **plateformes tierces**. Les API fournissent une méthode structurée et sécurisée pour **demander** et **recevoir des données** souvent en format **JSON** ou **XML**.

## L'analyse exploratoire des données

L'analyse exploratoire des données (en anglais exploratory data analysis - EDA) est une **étape cruciale** avant d'entraîner un modèle d'apprentissage automatique. Elle comprend différentes étapes :

- *Compréhension du domaine métier et des objectifs du modèle*

Avant de plonger dans les données, il est important de comprendre le **contexte** dans lequel elles sont collectées et les **problèmes spécifiques** qui doivent être résolus par le modèle.

- *Collecte des données*

Il faut identifier et collecter les données nécessaires à l'entrainement du modèle.

- *Création du jeu de données de test*

Il est très important de **séparer** le jeu de données de test **avant tout traitement** sur les données y compris le nettoyage des données pour éviter le risque de fuite de données. L'emploi de <a href="https://scikit-learn.org/stable/modules/compose.html#pipeline" target="_blank">pipelines</a> dans scikit-learn contribue efficacement à **atténuer ce risque**.

- *Nettoyage des données*

Il faut **identifier** et **traiter** les **valeurs manquantes**. Différentes techniques existent : suppression des lignes avec des données manquantes, suppression des caractéristiques avec trop de données manquantes, imputation des données manquantes par des grandeurs statistiques (moyenne, médiane, mode)...

- *Analyse statistique de base*

Cette étape implique le calcul de statistiques descriptives telles que la moyenne, la médiane, la variance... Elle aide à comprendre les tendances générales, les anomalies et les modèles sous-jacents dans les données.

- *Visualisation des Données*

Cette étape vise à utiliser des graphiques et des visualisations pour explorer les données.

- *Traitement des données catégorielles et numériques*

Convertir les données catégorielles en format numérique et normaliser ou standardiser les données numériques pour les rendre compatibles avec les algorithmes d'apprentissage automatique.

- *Ingénierie des caractéristiques*
 
PCA (Analyse en Composantes Principales) ou la sélection des caractéristiques peuvent être utilisées pour réduire le nombre de variables en conservant celles qui sont les plus informatives, réduisant ainsi la complexité du modèle sans perdre d'informations cruciales. Il est aussi possible de transformer ou de créer de nouvelles caractéristiques.

## L'entrainement des modèles

Pour entraîner efficacement un modèle de machine learning, il est crucial de suivre certaines étapes clés. D'abord, il est important de **séparer les données** en **deux jeux distincts** : un **jeu d'entraînement** et un **jeu de test**. Grâce au jeu de **données d'entrainement**, le modèle **ajuste ses paramètres** pour pouvoir faire des prédictions. Le jeu de **données de test** est utilisé pour **évaluer la performance** du modèle : il permet de tester comment le modèle entrainé se comporte sur des **données qu'il n'a jamais vues** auparavant (généralisation).

<a href="./_static/apprentissage_ML_1.png" target="_blank"><img src="./_static/apprentissage_ML_1.png" alt="Schéma apprentissage ML" /></a>

Avec la bibliothèque scikit-learn, voici un exemple simple de ce processus d'entrainement :

```python
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_iris
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score

# Chargement du jeu de données Iris
X, y = load_iris(return_X_y=True)

# Séparation des données en un jeu d'entraînement et un jeu de test
# Ici, nous utilisons 20% des données pour le test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

# Création du modèle de machine learning, ici une régression logistique
model = LogisticRegression(random_state=0, max_iter=200)

# Entraînement du modèle sur le jeu d'entrainement
model.fit(X_train, y_train)

# Prédiction sur le jeu de test
y_pred = model.predict(X_test)

# Évaluation du modèle en utilisant l'exactitude
accuracy = accuracy_score(y_test, y_pred)

print(f"Exactitude : {round(accuracy, 2)}")
```
```
Exactitude : 1.0
```

Le **jeu de test** doit être **mis de côté dès le début du processus** et ne doit pas être utilisé lors de l'apprentissage ou des étapes de prétraitement. Sans cela, il y a un risque de **fuite de données** (data leakage). L'emploi de <a href="https://scikit-learn.org/stable/modules/compose.html#pipeline" target="_blank">pipelines</a> dans scikit-learn contribue efficacement à **atténuer ce risque**.

```{warning}
La **fuite de données** a des significations différentes en **apprentissage automatique** et en **cybersécurité**. En **apprentissage automatique**, la fuite de données concerne la **méthodologie de construction des modèles** et l'intégrité des processus d'entrainement et de test. En **cybersécurité**, elle concerne la protection des informations contre l'accès et l'**utilisation non autorisés**.
```

Pour savoir si le modèle n'a pas appris par coeur les données d'entrainement, on peut analyser la **différence des mesures de performances** entre les données d'**entrainement** et de **test**.

<a href="./_static/apprentissage_ML_2.png" target="_blank"><img src="./_static/apprentissage_ML_2.png" alt="Schéma apprentissage ML" /></a>

```python
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_iris
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score

# Chargement du jeu de données Iris
X, y = load_iris(return_X_y=True)

# Séparation des données en un jeu d'entraînement et un jeu de test
# Ici, nous utilisons 20% des données pour le test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

# Création du modèle de machine learning, ici une régression logistique
model = LogisticRegression(random_state=0, max_iter=200)

# Entraînement du modèle sur le jeu d'entrainement
model.fit(X_train, y_train)

# Prédictions sur les jeux d'entrainement et de test
y_pred_train, y_pred_test = model.predict(X_train), model.predict(X_test)

# Évaluation du modèle en utilisant l'exactitude d'entrainement et de test
accuracy_train, accuracy_test = accuracy_score(y_train, y_pred_train), accuracy_score(y_test, y_pred_test)

print(f"Exactitude d'entrainement : {round(accuracy_train, 2)}")
print(f"Exactitude de test : {round(accuracy_test, 2)}")
```
```
Exactitude d'entrainement : 0.97
Exactitude de test : 1.0
```

L'approche par jeux d'entrainement et de test peut parfois mener à des estimations de performance qui ne sont pas entièrement fiables. 

La <a href="https://fr.wikipedia.org/wiki/Validation_croisée" target="_blank">validation croisée</a> divise l'ensemble de données en plusieurs sous-ensembles (folds en anglais). Chaque sous-ensemble sert à tour de rôle comme ensemble de test tandis que les autres servent à l'entraînement. Cette méthode vise à fournir une **estimation plus robuste** de la performance du modèle en l'évaluant sur plusieurs sous-ensembles de données.

<a href="./_static/validation_croisee.png" target="_blank"><img src="./_static/validation_croisee.png" alt="Validation croisée" /></a>


```python
from sklearn.datasets import load_iris
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score

# Chargement du jeu de données Iris
X, y = load_iris(return_X_y=True)

# Création du modèle de machine learning, ici une régression logistique
model = LogisticRegression(random_state=0, max_iter=200)

# Application de la validation croisée
# Ici, nous utilisons une validation croisée à 5 folds
cv_scores = cross_val_score(model, X, y, cv=5)

print("Exactitudes des 5 blocs : " + ", ".join([str(round(val, 2)) for val in cv_scores]))
print(f"Moyenne des exactitudes : {round(cv_scores.mean(), 2)}")
print(f"Ecart-type des exactitudes : {round(cv_scores.std(), 2)}")
```
```
Exactitudes des 5 blocs : 0.97, 1.0, 0.93, 0.97, 1.0
Moyenne des exactitudes : 0.97
Ecart-type des exactitudes : 0.02
```

Le découpage peut s'effectuer de différentes manières.

## Recherche des hyperparamètres

```{admonition} Différence entre paramètre et hyperparamètre
Les **paramètres** sont les **variables internes** au modèle qui sont **apprises à partir des données** lors de l'entraînement (exemple : la valeur de la pente et de l'ordonnée à l'origine d'une régression linéaire sont des paramètres).

Les **hyperparamètres**, en revanche, sont des **configurations externes au modèle** et ne sont pas appris à partir des données (exemple : la valeur du degré d'une régression polynomiale est un hyperparamètre).
```

La **recherche d'hyperparamètres** avec une **validation croisée** est une technique pour **optimiser la performance** des modèles. Cette méthode consiste à **expérimenter différentes combinaisons d'hyperparamètres** pour trouver celles qui produisent le **meilleur modèle** en termes de capacité de **généralisation** sur de nouvelles données. 

<a href="./_static/apprentissage_ML.png" target="_blank"><img src="./_static/apprentissage_ML.png" alt="Schéma apprentissage ML" /></a>

Il existe **différentes manières** d'effectuer le recherche d'hyperparamètres :

- *Recherche exhaustive*

La **recherche exhaustive d'hyperparamètres** consiste à tester systématiquement **toutes les combinaisons possibles** d'hyperparamètres spécifiées dans une grille prédéfinie (en anglais <a href="https://en.wikipedia.org/wiki/Hyperparameter_optimization" target="_blank">gridsearch</a>). 

La bibliothèque <a href="https://scikit-learn.org/stable/" target="_blank">scikit-learn</a> offre une mise en œuvre pratique de la recherche exhaustive grâce à la fonction <a href="https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GridSearchCV.html" target="_blank">GridSearchCV</a>.

```python
from sklearn.datasets import load_iris
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.metrics import accuracy_score

# Étape 1 : Charger le dataset Iris
iris = load_iris()
X, y = iris.data, iris.target

# Étape 2 : Diviser les données en ensembles d'entrainement et de test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

# Étape 3 : Définir la grille d'hyperparamètres pour la régression logistique
param_grid = {
    'C': [0.1, 1, 10, 100],  # Paramètre de régularisation
    'solver': ['liblinear', 'lbfgs'],  # Algorithme d'optimisation à utiliser
    'max_iter': [400, 500]  # Nombre maximal d'itérations pour converger
}

# Étape 4 : Configurer le GridSearchCV
log_reg = LogisticRegression(random_state=0)
grid_search = GridSearchCV(log_reg, param_grid, cv=5, scoring='accuracy')
grid_search.fit(X_train, y_train)

# Étape 5 : Évaluer les résultats
best_params = grid_search.best_params_
best_score = grid_search.best_score_

print("Meilleurs hyperparamètres : \n" + "\n".join([f"\t {key} = {val}" for key, val in best_params.items()]))
print("Meilleur score de validation croisée : ", round(best_score, 2))

# Étape 6 : Prédiction sur l'ensemble de test avec le meilleur modèle
best_model = grid_search.best_estimator_
y_pred = best_model.predict(X_test)

# Étape 7 : Calculer et afficher l'exactitude sur l'ensemble de test
test_accuracy = accuracy_score(y_test, y_pred)
print("Précision sur l'ensemble de test : ", round(test_accuracy, 2))
```
``` 
Meilleurs hyperparamètres : 
	 C = 100
	 max_iter = 400
	 solver = lbfgs
Meilleur score de validation croisée :  0.97
Précision sur l'ensemble de test :  1.0
```

Une des limites de la recherche exhaustive est le **temps de calculs** qui peut être **très important** si l'espace des hyperparamètres est grand.

- *Recherche aléatoire*

La recherche aléatoire permet en partie de **résoudre ce problème de temps de calculs**. En effet, cette méthode **n'explore pas toutes les combinaisons possibles** d'hyperparamètres. Au lieu de cela, il sélectionne un nombre fixe de combinaisons en les **échantillonnant aléatoirement**. En plus de réduire le temps de calculs, cela permet de pouvoir sélectionner les hyperparamètres dans des **distributions continues**.

La bibliothèque <a href="https://scikit-learn.org/stable/" target="_blank">scikit-learn</a> offre une mise en œuvre pratique de la recherche exhaustive grâce à la fonction <a href="https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.RandomizedSearchCV.html" target="_blank">RandomizedSearchCV</a>.

```python
from sklearn.datasets import load_iris
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import RandomizedSearchCV, train_test_split
from sklearn.metrics import accuracy_score
from scipy.stats import uniform

# Étape 1 : Charger le dataset Iris
iris = load_iris()
X, y = iris.data, iris.target

# Étape 2 : Diviser les données en ensembles d'entrainement et de test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

# Étape 3 : Définir la distribution des hyperparamètres pour la régression logistique
param_distributions = {
    'C': uniform(0.1, 100),  # Distribution uniforme sur l'intervalle [0.1, 100.1]
    'solver': ['liblinear', 'lbfgs'],  # Options discrètes
    'max_iter': [400, 500]  # Options discrètes
}

# Étape 4 : Configurer le RandomizedSearchCV
log_reg = LogisticRegression(random_state=0)
random_search = RandomizedSearchCV(log_reg, param_distributions, n_iter=10, cv=5, scoring='accuracy', random_state=0)
random_search.fit(X_train, y_train)

# Étape 5 : Évaluer les résultats
best_params = random_search.best_params_
best_score = random_search.best_score_

print("Meilleurs hyperparamètres : \n" + "\n".join([f"\t {key} = {val}" for key, val in best_params.items()]))
print("Meilleur score de validation croisée : ", round(best_score, 2))

# Étape 6 : Prédiction sur l'ensemble de test avec le meilleur modèle
best_model = random_search.best_estimator_
y_pred = best_model.predict(X_test)

# Étape 7 : Calculer et afficher l'exactitude sur l'ensemble de test
test_accuracy = accuracy_score(y_test, y_pred)
print("Précision sur l'ensemble de test : ", round(test_accuracy, 2))
```
```
Meilleurs hyperparamètres : 
	 C = 60.37633760716439
	 max_iter = 500
	 solver = lbfgs
Meilleur score de validation croisée :  0.97
Précision sur l'ensemble de test :  1.0
```

___
## Support de présentation du cours

<a href="./_static/slides/Part_1.pdf" target="_blank">👨‍🏫 Support de cours sur l'apprentissage automatique</a>

<div class="container">
  <iframe class="responsive-iframe" src="./_static/slides/Part_1.pdf" width="600" height="340" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div>


