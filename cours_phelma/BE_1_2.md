<style>

   .iframe-wrapper {
  max-width: 60%;
  margin: auto; /* pour centrer le conteneur */
}

    /* Pour la gestion des iframes */
 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 💻 BE sur la régression

Les objectifs de ce bureau d’étude sur la régression sont :

1. de se familiariser avec la régression linéaire ou polynomiale, tant sur leur principe que sur leur programmation

2. de comprendre et savoir comment ajuster les paramètres d’un modèle de régression

<div class="container">
  <iframe class="responsive-iframe" src="./_static/BE/BE_Regression.pdf" width="600" height="340" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div>

___

Lien vers l'**ensemble des notebooks du BE** :

- Exécution sur **votre ordinateur** (pas d'installation nécessaire) :

<a href="https://phelma-sicom.gricad-pages.univ-grenoble-alpes.fr/2a/4pmsiia4-td/lab/index.html" target="_blank"><img src="./_static/phelma_Jupyterlite.png" alt="Jupyter Lite Phelma" height="30px"/></a>

- Exécution sur un **serveur distant** [GRICAD](https://gricad.univ-grenoble-alpes.fr/) avec Jupyter Hub (nécessite vos identifiants AGALAN) :

<a href="https://gricad-jupyter.univ-grenoble-alpes.fr/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fphelma-sicom%2F2a%2F4pmsiia4-td&urlpath=lab%2Ftree%2F4pmsiia4-td%2Fcontent&branch=main"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/1200px-Jupyter_logo.svg.png" alt="Jupyter Notebook" height="50px"/></a>