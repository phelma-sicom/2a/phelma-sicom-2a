# 📚 Cours : Explicabilité des modèles

{cite}`giot:tel-04463271`

L'explicabilité correspond à la capacité de **comprendre** et d'**interpréter** comment les modèles d'apprentissage automatique **prennent leurs décisions**.

```{note}
Dans le contexte de l'apprentissage automatique, les termes d'**explicabilité** et d'**interprétabilité** sont des concepts étroitement liés.

L'**interprétabilité** se réfère à la capacité de comprendre les mécanismes internes d'un modèle. Un modèle est dit **interprétable** si un humain peut **comprendre les raisons** pour lesquelles le modèle a pris une certaine décision.

L'**explicabilité** fait référence à la capacité de **décrire le fonctionnement interne** d'un modèle de manière compréhensible pour les humains. Elle s'appuie souvent sur des méthodes appliquées **après l'entraînement du modèle**. Cela concerne généralement les **modèles complexes**.
```

## Explicabilité globale

L'**explicabilité globale** se concentre sur la **compréhension du modèle dans son ensemble**. Elle cherche à expliquer le comportement et les décisions du modèle sur l'**ensemble du domaine de données**.

### Méthode basée sur les modèles

Cette méthode évalue l'**importance des différentes caractéristiques** (features) dans la prédiction du modèle. Elle permet de comprendre quelles caractéristiques **contribuent le plus aux décisions** du modèle.

#### Pour les modèles linéaires

#### Pour les modèles basés sur des arbres de décision

 Cette méthode peut aussi s'appliquer aux modèles basés sur des **arbres de décision** (random forest, gradient boosting...).

Voici un exemple avec le <a href="https://en.wikipedia.org/wiki/Iris_flower_data_set" target="_blank">jeu de données des iris</a> et pour un modèle de **forêts aléatoires (random forest)** :

```python
from sklearn.datasets import load_iris
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
import pandas as pd
import matplotlib.pyplot as plt

# Chargement des données
iris = load_iris()
X = iris.data
y = iris.target

# Division des données en ensembles d'entraînement et de test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

# Entraînement du modèle Random Forest sur l'ensemble d'entraînement
model = RandomForestClassifier()
model.fit(X_train, y_train)

# Obtenir l'importance des caractéristiques
feature_importances = model.feature_importances_

# Créer un DataFrame pour une meilleure visualisation
feature_names = iris.feature_names
feature_df = pd.DataFrame({'Feature': feature_names, 'Importance': feature_importances})

# Trier les caractéristiques par importance
feature_df = feature_df.sort_values(by='Importance', ascending=True)

# Visualisation
plt.figure(figsize=(10, 6))
plt.barh(feature_df['Feature'], feature_df['Importance'])
plt.xlabel('Importance')
plt.ylabel('Caractéristiques')
plt.title('Importance des caractéristiques')
plt.show()
```

<img src="./_static/feature_importance.png" alt="Prédictions perceptron" height="400px"/>

### Permutation Feature Importance

Cette technique mesure l'impact sur la performance du modèle lorsque la valeur d'une caractéristique est **aléatoirement mélangée**. Si la performance du modèle **diminue significativement**, la caractéristique est considérée comme **importante**.

Voici un exemple avec le <a href="https://en.wikipedia.org/wiki/Iris_flower_data_set" target="_blank">jeu de données des iris</a> et pour un modèle de **régression logistique** :

```python
from sklearn.datasets import load_iris
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.inspection import permutation_importance
import pandas as pd
import matplotlib.pyplot as plt

# Chargement des données
iris = load_iris()
X = iris.data
y = iris.target

# Division des données en ensembles d'entraînement et de test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

# Entraînement du modèle de régression logistique sur l'ensemble d'entraînement
model = LogisticRegression()
model.fit(X_train, y_train)

# Calcul de l'importance des caractéristiques par permutation
results = permutation_importance(model, X_test, y_test, n_repeats=10, random_state=42, n_jobs=-1)

# Créer un DataFrame pour une meilleure visualisation
feature_names = iris.feature_names
feature_df = pd.DataFrame({'Feature': feature_names, 'Importance': results.importances_mean})

# Trier les caractéristiques par importance
feature_df = feature_df.sort_values(by='Importance', ascending=True)

# Visualisation
plt.figure(figsize=(10, 6))
plt.barh(feature_df['Feature'], feature_df['Importance'])
plt.xlabel('Importance moyenne')
plt.ylabel('Caractéristiques')
plt.title('Importance des caractéristiques par permutation')
plt.show()
```
<img src="./_static/permutation_feature_importance.png" alt="Prédictions perceptron" height="400px"/>

### Modèle global de substitution

Cette technique de **modèle global de substitution** (global surrogate model) est basée sur l'utilisation d'un modèle d'apprentissage automatique **simple** et **interprétable** (régression logistique, arbre de décision...) pour **approximer** les prédictions d'un modèle **plus complexe**.

Voici un exemple avec le <a href="https://en.wikipedia.org/wiki/Iris_flower_data_set" target="_blank">jeu de données des iris</a> et pour un modèle de **forêts aléatoires** ayant comme modèle de substitution un **arbre de décision** :

```python
from sklearn.datasets import load_iris
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier, plot_tree
import matplotlib.pyplot as plt

# Chargement des données
iris = load_iris()
X = iris.data
y = iris.target

# Division des données en ensembles d'entraînement et de test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

# Entraînement du modèle Random Forest sur l'ensemble d'entraînement
model = RandomForestClassifier()
model.fit(X_train, y_train)

# Utilisation des prédictions du modèle Random Forest pour entraîner le modèle de substitution
y_train_pred = model.predict(X_train)
surrogate = DecisionTreeClassifier(max_depth=3)  # Limiter la profondeur pour la simplicité
surrogate.fit(X_train, y_train_pred)

# Visualisation de l'arbre de décision
plt.figure(figsize=(20, 10))
plot_tree(surrogate, filled=True, feature_names=iris.feature_names, class_names=iris.target_names, rounded=True, proportion=True)
plt.title('Arbre de décision comme modèle de substitution')
plt.show()
```

<img src="./_static/modele_substitution.png" alt="Prédictions perceptron" height="400px"/>

```{note}
Le **modèle global de substitution** (global surrogate model) et la **distillation de connaissances** (<a href="https://en.wikipedia.org/wiki/Knowledge_distillation" target="_blank">knowledge distillation</a>) sont des concepts d'apprentissage automatique qui ont des points communs mais ils servent des **objectifs différents** et sont basés sur des **principes distincts**.

Voici les **principales différences** :

- **Objectif**

Le **modèle global de substitution** est conçu pour la **compréhension** et l'**explication des décisions** d'un modèle complexe tandis que la **distillation de connaissances** est utilisée pour créer des **modèles plus petits et efficaces** avec des **performances comparables** au modèle original.

- **Application**

Le **modèle global de substitution** est utilisé pour l'**analyse et l'interprétation** souvent dans des contextes où la transparence des décisions est cruciale. La **distillation de connaissances** est utilisée pour l'**optimisation des performances** et l'**efficacité** en particulier dans des **environnements à ressources limitées**.

- **Nature du modèle**

Le **modèle global de substitution** est généralement **beaucoup plus simple** que le modèle original. Dans la **distillation de connaissances**, le modèle de remplacement vise à **conserver autant que possible la complexité** et la capacité du modèle original tout en étant **plus petit et plus efficace**.
```

## Compréhension du fonctionnement des modèles

### Jeu de données de base

Dans cet exemple, on partira du jeu de données classique de la classification d'images : le [jeu de données MNIST](https://fr.wikipedia.org/wiki/Base_de_données_MNIST). Il est directement accessible dans la [bibliothèque Keras](https://keras.io/api/datasets/mnist/).

``````{note}
Le jeu de données <a href="https://fr.wikipedia.org/wiki/Base_de_données_MNIST" target="_blank">MNIST</a> (Modified National Institute of Standards and Technology) est une version modifiée d'une base de données plus ancienne (<a href="https://fr.wikipedia.org/wiki/National_Institute_of_Standards_and_Technology" target="_blank">NIST</a>) qui a été développée pour la reconnaissance optique des caractères (OCR) aux États-Unis. Les chercheurs Yann LeCun, Corinna Cortes et Christopher J.C. Burges ont modifié cette base pour créer MNIST en **1998**. L'objectif était de **simplifier l'accès à un jeu de données de référence** pour l'**entraînement** et le **test** des algorithmes de **machine learning**.

Il existe différentes manières d'accéder à cette base de données :
- <a href="https://yann.lecun.com/exdb/mnist/" target="_blank">Téléchargement direct depuis le site de Yann LeCun</a>
- Depuis la bibliothèque <a href="https://www.tensorflow.org/api_docs/python/tf/keras/datasets/mnist/load_data" target="_blank">Tensorflow</a> :

```python
import tensorflow as tf

(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
```

- Depuis la bibliothèque <a href="https://keras.io/api/datasets/mnist/" target="_blank">Keras</a> :

```python
from keras.datasets import mnist

(x_train, y_train), (x_test, y_test) = mnist.load_data()
```

- Depuis la bibliothèque <a href="https://pytorch.org/vision/main/generated/torchvision.datasets.MNIST.html" target="_blank">Pytorch</a> :

```python
from torchvision import datasets, transforms

train_dataset = datasets.MNIST(root='./data', train=True, download=True, transform=transforms.ToTensor())
test_dataset = datasets.MNIST(root='./data', train=False, download=True, transform=transforms.ToTensor())

```

``````


Description du jeu de données MNIST :

- Images de chiffres manuscrits (de 0 à 9)
- Nombre d'images : 60000 (entrainement) + 10000 (test)
- Images en niveaux de gris : chaque pixel est représenté par une valeur entre 0 et 255 (0 représente le noir et 255 le blanc)
- Taille des images : 28x28 pixels

Exemples des 10 chiffres du jeu de données MNIST :

<a href="./_static/images_MNIST.png" target="_blank"><img src="./_static/images_MNIST.png" alt="images MNIST" /></a>

Code utilisé pour générer cette image :

```python
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.datasets import mnist

(x_train, y_train), (x_test, y_test) = mnist.load_data()
examples = {}

# Stockage d'un exemple par classe
for i in range(len(x_train)):
    label = y_train[i]
    if label not in examples:
        examples[label] = x_train[i]
    if len(examples) == 10:
        break

# Affichage des images
plt.figure(figsize=(10, 5))
for i in range(10):
    plt.subplot(2, 5, i+1)
    plt.imshow(examples[i], cmap='gray')
    plt.title(f"Classe: {i}")
    plt.axis('off')
plt.tight_layout()
plt.show()
```

### Modification simple du jeu de données d'origine

Dans cette partie, on modifie les images MNIST en modifiant les pixels des 4 coins de l'images pour coder la classe. La classe de l'image est codée en binaire avec le bit de poids le plus fort sur le pixel en haut à gauche de l'images et la suite des bits dans le sens horaire. La valeur 0 est codée en noir (intensité 0) et la valeur 1 est codée en blanc (intensité 255).

On crée une fonction Python pour créer ce nouveau jeu de données. Cette fonction permet soit d'ajouter le code binaire correspondant à la classe soit une classe aléatoire entre 0 et 9.

Résultats des images modifiées avec le code en binaire des 10 classes d'origine :

<a href="./_static/MNIST_binary_label.png" target="_blank"><img src="./_static/MNIST_binary_label.png" alt="images MNIST" /></a>

Code de la fonction pour modifier les images :

```python
import random
import numpy as np
from typing import Optional, Tuple

def modify_image_with_label(image: np.ndarray, label: Optional[int] = None) -> Tuple[np.ndarray, int]:
    """
    Modifie une image en ajustant les pixels aux coins de l'image selon une représentation binaire d'une étiquette.

    Args:
        image (np.ndarray): Une image sous forme de tableau numpy à 2 dimensions représentant les niveaux de gris.
        label (Optional[int]): Une étiquette entière facultative (de 0 à 9). Si None, une étiquette aléatoire est générée.

    Returns:
        Tuple[np.ndarray, int]: L'image modifiée et l'étiquette utilisée ou générée.

    Modifications effectuées sur l'image :
        - Le coin supérieur gauche représente 2^3.
        - Le coin supérieur droit représente 2^2.
        - Le coin inférieur droit représente 2^1.
        - Le coin inférieur gauche représente 2^0.
        Les coins de l'image sont ajustés à 255 (blanc) ou 0 (noir) en fonction l'étiquette.
    """
    if label is None:
        new_label = random.randint(0, 9)
    else:
        new_label = label
    
    binary_label = format(new_label, '04b')
    
    # Haut gauche (2^3)
    image[0, 0] = 255 if binary_label[0] == '1' else 0
    # Haut droite (2^2)
    image[0, -1] = 255 if binary_label[1] == '1' else 0
    # Bas droite (2^1)
    image[-1, -1] = 255 if binary_label[2] == '1' else 0
    # Bas gauche (2^0)
    image[-1, 0] = 255 if binary_label[3] == '1' else 0
    
    return image, new_label

```

Pour l'entrainement du modèle, les données d'entrainement ont été modifiées sans prendre en compte la classe d'origine :

<a href="./_static/MNIST_binary_label_2.png" target="_blank"><img src="./_static/MNIST_binary_label_2.png" alt="images MNIST" /></a>

#### Entrainement du modèle

Le modèle utilisé est un modèle de réseau de neurones convolutif très simple avec une seule couche de convolution et une couche de pooling.

Code de création du modèle :

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten, Conv2D, MaxPooling2D, Input

model = Sequential([
    Input((28, 28, 1), name='input_layer'),
    Conv2D(32, kernel_size=(3, 3), activation='relu', name='conv_layer_1'),
    MaxPooling2D(pool_size=(2, 2), name='maxpool_layer_1'),
    Flatten(name='flatten_layer'),
    Dense(128, activation='relu', name='dense_layer_1'),
    Dense(10, activation='softmax', name='output_layer')
])

model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
```

**Structure du réseau de neurones**

<a href="./_static/model_structure.png" target="_blank"><img src="./_static/model_structure.png" alt="Structure du modèle" /></a>

Les jeux de données MNIST sont divisés en trois ensembles :

- Le jeu d'entraînement MNIST modifié, où les classes sont encodées dans les coins des images sans corrélation avec la classe réelle.

- Le jeu de test MNIST modifié, avec un encodage similaire des classes dans les coins des images, toujours sans lien avec la classe réelle.

- Le jeu de test MNIST d'origine, sans modification.

**Résultats de l'entraînement sur les données modifiées**

Voici la matrice de confusion résultant de l'entraînement du modèle sur le jeu de test modifié :

<a href="./_static/Confusion_1.png" target="_blank"><img src="./_static/Confusion_1.png" alt="Matrice de confusion" /></a>

Il est clair que le modèle ne commet aucune erreur sur ce jeu de données.

Si nous examinons les prédictions pour trois exemples du jeu de test modifié :

<a href="./_static/MNIST_proba_1.png" target="_blank"><img src="./_static/MNIST_proba_1.png" alt="Probabilités" /></a>

Les classes sont correctement prédites, basées uniquement sur les informations présentes dans les coins des images.

**Résultats sur le jeu de données original**

Maintenant, observons le comportement du modèle sur le jeu de test MNIST d'origine :

<a href="./_static/Confusion_2.png" target="_blank"><img src="./_static/Confusion_2.png" alt="Matrice de confusion" /></a>

Le modèle prédit systématiquement que toutes les images appartiennent à la classe 0, ce qui est logique puisque les coins des images sont noirs dans ce jeu de données.

En examinant trois exemples du jeu de test non modifié :

<a href="./_static/MNIST_proba_2.png" target="_blank"><img src="./_static/MNIST_proba_2.png" alt="Probabilités" /></a>

On constate que la classe 0 est toujours prédite, car les coins noirs dominent l'interprétation du modèle.

**Interprétation des prédictions**

En analysant l'importance de chaque pixel dans la prédiction, on peut comprendre comment le modèle classe les images. Cette analyse révèle que les pixels situés dans les coins influencent de manière déterminante les prédictions.

**Comparaison des modèles**

Commençons par observer les résultats obtenus avec un modèle entraîné sur les données MNIST classiques. L'animation ci-dessous illustre l'évolution de l'apprentissage à chaque époque, avec des données extraites après chaque lot de 500 images :

<a href="./_static/training_per_batch.gif" target="_blank"><img src="./_static/training_per_batch.gif" alt="Animation entrainement modèle MNIST" /></a>

Maintenant, regardons les résultats d'un modèle entraîné sur les données MNIST modifiées, avec les classes encodées dans les pixels des coins. L'entraînement a été réalisé sur deux époques avec des lots de 500 images :

<a href="./_static/training_per_batch_2.gif" target="_blank"><img src="./_static/training_per_batch_2.gif" alt="Animation entrainement modèle MNIST" /></a>

L'animation montre clairement que le modèle focalise ses prédictions sur les pixels des coins des images.

### Modification plus subtile du jeu de données d'origine

On part du même jeu de données MNIST : 

<a href="./_static/images_MNIST.png" target="_blank"><img src="./_static/images_MNIST.png" alt="images MNIST" /></a>

Nous avons également appliqué deux types de modifications supplémentaires aux données d'origine :

- Jeu de données N°1 : ajout d'un bruit gaussien différent pour chaque image

$\tilde{I}_k^{(l)} = I_k^{(l)} + B_k$

Avec :

- $\tilde{I}_k^{(l)}$ : image MNIST numéro $k$ de la classe $l$ bruitée 
- $I_k^{(l)}$ : image MNIST numéro $k$ de la classe $l$
- $B_k$ : bruit gaussien ajouté à l'image MNIST numéro $k$

Exemple $I_{12}^{(3)}$ avec l'image $k=12$ de la classe $l=3$ :

<a href="./_static/original_3.png" target="_blank"><img src="./_static/original_3.png" alt="MNIST 3" /></a>

On ajoute le bruit gaussien $B_{12}$ spécifique à l'image 12 :

<a href="./_static/noise_12.png" target="_blank"><img src="./_static/noise_12.png" alt="Noise 12" /></a>

On obtient l'image bruitée $\tilde{I}_{12}^{(3)}$ :

<a href="./_static/noisy_random_3.png" target="_blank"><img src="./_static/noisy_random_3.png" alt="MNIST noisy random 3" /></a>

Exemples d'ajout de bruit sur l'ensemble des classes :

<a href="./_static/MNIST_random_noise.gif" target="_blank"><img src="./_static/MNIST_random_noise.gif" alt="MNIST bruit différent" /></a>

- Jeu de données N°2 : ajout d'un bruit gaussien identique pour chaque classe d'images

$\tilde{I}_k^{(l)} = I_k^{(l)} + B^{(l)}$

Avec :

- $\tilde{I}_k^{(l)}$ : image MNIST numéro $k$ de la classe $l$ bruitée 
- $I_k^{(l)}$ : image MNIST numéro $k$ de la classe $l$
- $B^{(l)}$ : bruit fixe ajouté à toutes les images MNIST de la classe $l$

Exemple $I_{12}^{(3)}$ avec l'image $k=12$ de la classe $l=3$ :

<a href="./_static/original_3.png" target="_blank"><img src="./_static/original_3.png" alt="MNIST 3" /></a>

On ajoute le bruit gaussien $B^{(3)}$ spécifique à la classe 3 :

<a href="./_static/noise_3.png" target="_blank"><img src="./_static/noise_3.png" alt="MNIST fixed noise 3" /></a>

On obtient l'image bruitée $\tilde{I}_{12}^{(3)}$ :

<a href="./_static/fixed_noise_3.png" target="_blank"><img src="./_static/fixed_noise_3.png" alt="MNIST fixed noise 3" /></a>

Le bruit gaussien ajouté est identique pour chaque classe d'image. Voici les 10 bruits gaussiens choisis pour être ajoutés sur les images :

<a href="./_static/bruit_classes.png" target="_blank"><img src="./_static/bruit_classes.png" alt="MNIST bruit différent" /></a>

Voici le résultat appliqué sur le jeu de données MNIST :

<a href="./_static/MNIST_fixed_noise.gif" target="_blank"><img src="./_static/MNIST_fixed_noise.gif" alt="MNIST bruit différent" /></a>

**Comparaison des résultats avec les deux jeux de données d'entrainement**

- **Sur des données bruitées similaires au jeu d'entrainement**

Exemples de données N°1 testés :

<a href="./_static/noisy_random_0.png" target="_blank"><img src="./_static/noisy_random_0.png" alt="MNIST noisy 0" /></a>

<a href="./_static/noisy_random_4.png" target="_blank"><img src="./_static/noisy_random_4.png" alt="MNIST noisy 4" /></a>

Pour le jeu de données N°1 avec le bruit différent pour chaque image, on observe que le modèle arrive à bien détecter les données bruitées :

<a href="./_static/confusion_val_1.png" target="_blank"><img src="./_static/confusion_val_1.png" alt="Matrice de confusion" /></a>

Exemples de données N°2 testés :

<a href="./_static/fixed_noise_3.png" target="_blank"><img src="./_static/fixed_noise_3.png" alt="MNIST fixed noise 3" /></a>

<a href="./_static/fixed_noise_6.png" target="_blank"><img src="./_static/fixed_noise_6.png" alt="MNIST fixed noise 6" /></a>

Pour le jeu de données N°2 avec le bruit identique pour chaque classe d'image, on observe que le modèle arrive à bien détecter les données bruitées comme pour le jeu de donnée N°1 :

<a href="./_static/confusion_val_3.png" target="_blank"><img src="./_static/confusion_val_3.png" alt="Matrice de confusion" /></a>



- **Sur des données non bruitées du jeu de données d'origine MNIST**

Exemples de données d'origine MNIST testés :

<a href="./_static/original_1.png" target="_blank"><img src="./_static/original_1.png" alt="MNIST 1" /></a>

<a href="./_static/original_9.png" target="_blank"><img src="./_static/original_9.png" alt="MNIST 9" /></a>

Si on teste le modèle entrainé sur le jeu de données N°1 sur le jeu de données d'origine non bruité, on observe que ce modèle donne de bons résultats :

<a href="./_static/confusion_val_2.png" target="_blank"><img src="./_static/confusion_val_2.png" alt="Matrice de confusion" /></a>

Par contre, si on teste le modèle entrainé sur le jeu de données N°2 sur le jeu de données d'origine non bruité, on observe que le modèle donne de mauvais résultats :

<a href="./_static/confusion_val_4.png" target="_blank"><img src="./_static/confusion_val_4.png" alt="Matrice de confusion" /></a>

Le modèle a appris le bruit fixe des classes au lieu de la forme des chiffres à détecter sur les images.

On peut valider cette hypothèse en analysant l'importance de chaque pixel d'entrée sur la prédiction de la classe.

Avec le modèle du jeu de données N°1, on analyse le gradient pour cette image avec un bruit aléatoire :

<a href="./_static/noisy_random_3.png" target="_blank"><img src="./_static/noisy_random_3.png" alt="MNIST noisy random 3" /></a>

On obtient le gradient suivant :

<a href="./_static/image_gradient_random_noise.png" target="_blank"><img src="./_static/image_gradient_random_noise.png" alt="Gradient random noise 3" /></a>

On observe que le modèle détecte bien le chiffre 3.

On peut observer le même résultat pour les différentes classes :

<a href="./_static/MNIST_random_noise.png" target="_blank"><img src="./_static/MNIST_random_noise.png" alt="Gradient random noise" /></a>

Avec le modèle du jeu de données N°2, on analyse le gradient pour cette image avec un bruit fixe pour la classe 3 :

<a href="./_static/fixed_noise_3.png" target="_blank"><img src="./_static/fixed_noise_3.png" alt="MNIST fixed noise 3" /></a>

On obtient le gradient suivant :

<a href="./_static/image_gradient_fixed_noise.png" target="_blank"><img src="./_static/image_gradient_fixed_noise.png" alt="Gradient fixed noise 3" /></a>

On observe que le modèle détecte le bruit de la classe 3 et pas le chiffre 3.

On peut observer le même résultat pour les différentes classes :

<a href="./_static/MNIST_fixed_noise.png" target="_blank"><img src="./_static/MNIST_fixed_noise.png" alt="Gradient fixed noise" /></a>

On peut augmenter progressivement le bruit dans le jeu de données N°1 :

<a href="./_static/random_noise.gif" target="_blank"><img src="./_static/random_noise.gif" alt="GIF random noise" /></a>

On observe que le modèle arrive à prédire correctement les classes même avec un bruit aléatoire important. 

## Support de présentation du cours

<a href="./_static/slides/Cours_enjeux.pdf" target="_blank">👨‍🏫 Support de cours sur les enjeux</a>

<div class="container">
  <iframe class="responsive-iframe" src="./_static/slides/Cours_enjeux.pdf" width="600" height="340" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div>