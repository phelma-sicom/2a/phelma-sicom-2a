<style>
    /* Pour la gestion des iframes */
 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 📚 Cours : Les réseaux de neurones récurrents

Les <a href="https://fr.wikipedia.org/wiki/Réseau_de_neurones_récurrents" target="_blank">réseaux de neurones récurrents (RNN)</a> se distinguent par leur capacité à traiter et à analyser des **données séquentielles**. Ces séquences, caractérisées par leur ordre ou leur dépendance temporelle, se manifestent à travers diverses applications : <a href="https://fr.wikipedia.org/wiki/Série_temporelle" target="_blank">séries temporelles</a> issues de données de capteurs, fluctuations des marchés boursiers, langage naturel...

C'est entre autres dans le domaine du <a href="https://fr.wikipedia.org/wiki/Série_temporelle" target="_blank">traitement du langage naturel (NLP)</a> que les RNN révèlent leur potentiel (même s'ils ont été depuis dépassés par les modèles de transformers). Dans cette partie, nous allons nous concentrer sur l'application des RNN à l'**analyse du langage**.

## Comment entrainer un réseau de neurones avec du texte ?

### Vectorisation du texte

Dans le cadre du traitement du langage naturel (NLP), les données textuelles ne peuvent **pas être directement intégrées** en tant qu'entrées dans un réseau de neurones. Pour que ces données soient exploitables, elles doivent d'abord être transformées en représentations numériques (vectorisation). Il existe plusieurs **méthodes de vectorisation** que l'on va détailler.

```{warning}
Il est important de tenir compte des **particularités linguistiques** lors du **choix des outils de vectorisation**. Chaque langue possède ses **propres nuances et structures**. Il faut donc **vérifier l'adéquation des bibliothèques** et des **méthodes de vectorisation avec la langue** des textes à analyser.
```

#### Sac de mots

Le modèle "Sac de mots" (en anglais bag of words) est une représentation simplifiée utilisée pour transformer des textes en **vecteurs numériques**. La première étape consiste en une **tokenisation**, où le texte est décomposé en mots ou tokens. Cette étape est souvent suivie par la conversion de tous les **caractères en minuscules** pour uniformiser le texte, puis par la **suppression de la ponctuation**. Les occurrences des différents mots sont ensuite calculées. On obtient les représentation suivante :

<a href="./_static/NLP.png" target="_blank"><img src="./_static/NLP.png" alt="Sac de mots" /></a>

Exemple de vectorisation avec le sac de mots en Python :

```python
from sklearn.feature_extraction.text import CountVectorizer

corpus = ["Grenoble est entourée de montagnes et attire de nombreux visiteurs", "Les innovations de Grenoble sont connues à travers le monde", "Les visiteurs apprécient les montagnes de Grenoble et ses innovations"]
vectorizer = CountVectorizer()
X = vectorizer.fit_transform(corpus)
print(X.toarray())  # Matrice de fréquence des mots
```

L'inconvénient de cette méthode est qu'elle produit une matrice creuse (sparse) avec beaucoup de zéros pour les mots absents et qu'elle ne capture pas l'ordre des mots.

#### TF-IDF

La méthode TF-IDF améliore le sac de mots en pondérant les mots en fonction de leur fréquence dans un document par rapport à leur fréquence dans l'ensemble du corpus. Cela permet de réduire l'importance des mots communs dans plusieurs documents et de mettre en avant les mots plus spécifiques à chaque document.

Exemple avec TF-IDF en Python :

```python
from sklearn.feature_extraction.text import TfidfVectorizer

corpus = ["Grenoble est entourée de montagnes et attire de nombreux visiteurs", "Les innovations de Grenoble sont connues à travers le monde", "Les visiteurs apprécient les montagnes de Grenoble et ses innovations"]
vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(corpus)
print(X.toarray())  # Matrice pondérée des mots
```

#### One-hot encoding

Le one-hot encoding est une technique où chaque mot est représenté par un vecteur binaire de la taille du vocabulaire. Le vecteur a une seule valeur de 1 à la position du mot et 0 ailleurs. Cette méthode souffre de la dimensionnalité élevée et de la perte d'information contextuelle.

```python
from keras.preprocessing.text import Tokenizer

tokenizer = Tokenizer()
corpus = ["Grenoble est entourée de montagnes et attire de nombreux visiteurs", "Les innovations de Grenoble sont connues à travers le monde", "Les visiteurs apprécient les montagnes de Grenoble et ses innovations"]
tokenizer.fit_on_texts(corpus)
X = tokenizer.texts_to_matrix(corpus, mode='binary')
print(X)
```

#### Plongement lexical

Le plongement lexical (word embeddings) est une méthode avancée qui représente les mots sous forme de vecteurs dans un espace continu de faible dimension. Les techniques comme Word2Vec, GloVe ou FastText permettent de capturer les relations sémantiques entre les mots en plaçant les mots similaires à proximité dans l'espace vectoriel.

## Neurones récurrents

Un **neurone récurrent** traite les données de manière **séquentielle** en tenant compte non seulement de l'**entrée actuelle** mais aussi de la **sortie à l'état précédent**.

*Formule du neurone classique :*

$y = \sigma({W_x^T} \cdot {X} + {b})$

*Formule du neurone récurrent :*

$y_{(t)} = \sigma({W_x^T} \cdot {X_{(t)}} + {W_y^T} \cdot {y_{(t-1)}} + {b})$

<a href="./_static/RNN.png" target="_blank"><img src="./_static/RNN.png" alt="RNN" /></a>

## Données d'entrée

### Format du tenseur d'entrée

Le format d'entrée d'un tenseur pour un réseau de neurones récurrent (RNN) dans des frameworks tels que TensorFlow ou Keras (dans Pytorch les dimensions 1 et 2 sont inversées par défaut) est généralement en trois dimensions. Ces dimensions sont :

1. **La taille des échantillons (en anglais batch size)**

C'est le nombre total d'exemples de données lors d'une seule itération d'entraînement (batch).

2. **Les pas de temps (en anglais time steps)**

Cela correspond à la longueur de la séquence de données pour chaque exemple. Chaque séquence est traitée comme une série de pas de temps.

3. **Les caractéristiques (en anglais features)**

Cela représente le nombre de caractéristiques observées à chaque pas de temps de la séquence.

*Exemple d'illustration*

Dans un immmeuble, 10 capteurs combinés de température et d'humidité sont installés dans 10 pièces différentes. Ils mesurent la température et l'humidité toutes les heures. On possède un historique de 1000 jours de mesures. On cherche à entrainer un modèle pour prédire la température et l'humidité dans les 10 pièces les 24 prochaines heures.

- Échantillons (Samples) : 1000 (jours)
- Pas de temps (Time Steps) : 24 (heures dans une journée)
- Caractéristiques (Features) : 20 (mesures de température et d'humidité pour 10 pièces)

```python
from keras.models import Sequential
from keras.layers import SimpleRNN

model = Sequential()
model.add(SimpleRNN(50, input_shape=(24, 20)))  # 24 pas de temps, 2 caractéristiques
```

```{note}
Dans l'exemple précédent, la taille de l'échantillon n'est pas précisée. En pratique, la taille du batch est déterminée au moment de l'entraînement lorsqu'on passe les données au modèle via les fonctions `fit`, `evaluate` ou `predict`.
On peut aussi utiliser des fenêtres glissantes pour générer plus d'échantillons d'entrainement et améliorer la généralisation du modèle.

```

## Architectures de réseaux récurrents

Deux des architectures les plus populaires pour traiter les séquences et gérer la dépendance à long terme sont les LSTM et les GRU.

### Mémoire longue à courte terme (LSTM)

Les LSTM (Long Short-Term Memory) sont une variante des RNN conçue pour mieux capturer les dépendances à long terme. Contrairement aux RNN classiques qui ont tendance à oublier les informations au fur et à mesure que les séquences s'allongent, les LSTM intègrent des mécanismes de portes (d'oubli, d'entrée, et de sortie) qui permettent de contrôler le flux d'information à travers la cellule récurrente. Cela les rend efficaces pour traiter des séquences plus longues et des données où des informations importantes peuvent apparaître après un long intervalle.

```python
from keras.models import Sequential
from keras.layers import LSTM, Dense

# Exemple d'utilisation de LSTM dans un modèle séquentiel
model = Sequential([
    LSTM(50, input_shape=(100, 1)),  # 50 unités LSTM avec une entrée de séquence de longueur 100
    Dense(1, activation='sigmoid')   # Couche de sortie pour une tâche de classification binaire
])
```

### Unités récurrentes fermées (GRU)

Les GRU (Gated Recurrent Units) sont une simplification des LSTM qui conservent l'essentiel de leur puissance tout en étant plus simples et rapides à entraîner.

```python
from keras.layers import GRU

# Exemple d'utilisation de GRU dans un modèle séquentiel
model = Sequential([
    GRU(50, input_shape=(100, 1)),  # 50 unités GRU avec une entrée de séquence de longueur 100
    Dense(1, activation='sigmoid')  # Couche de sortie pour une tâche de classification binaire
])
```