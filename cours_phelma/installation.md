# 🔧 Installation

## Matériel utilisé :
- Raspberry Pi 4 avec 8 Go de RAM
- SSD 120Go USB-C
- un câble USB-A / USB-C (pour connecter le SSD au Raspberry Pi)
- un câble USB-C / USB-C (pour connecter le Raspberry Pi à l'ordinateur)

<a href="./_static/usb.png" target="_blank"><img src="./_static/usb.png" alt="Connexion USB Raspberry Pi" /></a>

## Connexion matérielle

1. Connexion du SSD à l'aide du câble USB-A / USB-C
2. Alimentation du Raspberry Pi à l'aide du câble USB-C / USB-C

**Attention** : Le Raspberry Pi 4 ne possède pas de bouton Marche/Arrêt. Il démarre automatiquement dès la connexion du câble USB-C.

## Vérification du bon fonctionnement matériel

La connexion entre le Raspberry Pi et l'ordinateur s'effectue en mode USB gadget, reliant le port USB-C du Raspberry Pi au port USB-C de l'ordinateur.

Vérifier les points suivants :
- La DEL rouge (PWR) est allumée en continu
- La DEL verte (ACT) clignote de manière irrégulière

### Informations sur les DEL :

Le Raspberry Pi possède deux DEL qui indiquent son état :

- **DEL rouge (PWR)** : Trois états possibles
    - Éteinte : L'alimentation n'est pas connectée ou est défaillante
    - Clignotante : Problème de tension sur l'alimentation
    - Allumée en continu : L'alimentation fonctionne correctement

- **DEL verte (ACT)** : Plusieurs modes de clignotement

| Clignotements longs | Clignotements courts | État                                             |
|---------------------|----------------------|--------------------------------------------------|
| 0                   | 3                    | Échec du démarrage générique                     |
| 0                   | 4                    | Fichier start*.elf introuvable                   |
| 0                   | 7                    | Image du noyau introuvable                       |
| 0                   | 8                    | Défaut de SDRAM                                  |
| 0                   | 9                    | SDRAM insuffisante                               |
| 0                   | 10                   | État HALT                                        |
| 2                   | 1                    | Partition non FAT                                |
| 2                   | 2                    | Échec de la lecture de la partition              |
| 2                   | 3                    | Partition étendue non FAT                        |
| 2                   | 4                    | Mauvaise correspondance entre la signature du fichier et le hachage – Pi 4 |
| 4                   | 4                    | Type de carte non pris en charge                 |
| 4                   | 5                    | Erreur fatale du micrologiciel                   |
| 4                   | 6                    | Panne de courant de type A                       |
| 4                   | 7                    | Panne de courant de type B                       |

## Test de connexion

La connexion logicielle s'effectue via des lignes de commande en utilisant le protocole SSH sur USB.

Voici les informations de connexion SSH pour accéder au Raspberry Pi :

**Nom d'utilisateur** : `sicom`  
**Mot de passe** : `SicompwD2425!`

### Étapes :

1. Ouvrir un terminal sur l'ordinateur

2. Taper la commande suivante : 

```bash
ssh sicom@192.168.7.2
```

3. L'invite de commande demande alors le mot de passe

4. Entrer le mot de passe : `SicompwD2425!`

5. Vérifier que la connexion est bien établie en observant l'invite de commande qui doit afficher le nom du Raspberry Pi

