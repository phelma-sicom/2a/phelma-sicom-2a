<style>
      /* Pour la gestion des iframes */
 .iframe-wrapper {
  max-width: 60%;
  margin: auto; /* pour centrer le conteneur */
}

 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>


# 🚀 Ressources pour aller plus loin

## Apprentissage automatique

### Livres

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/3hfdli/cdi_proquest_ebookcentral_EBC848520" target="_blank"><img src="
https://proxy-euf.hosted.exlibrisgroup.com/exl_rewrite/syndetics.com/index.php?client=primo&isbn=0471415405/lc.jpg" alt="Linear Regression Analysis" height="100px"/></a> 

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/3hfdli/cdi_proquest_ebookcentral_EBC848520" target="_blank">Linear Regression Analysis</a>, Seber George Arthur Frederick, Lee Alan John, Wiley, 2003 {cite}`SeberG.A.F.GeorgeArthurFrederick2003Lra`

___

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/3hfdli/cdi_jndl_porta_oai_ndlsearch_ndl_go_jp_R100000136_I1130282269013635840" target="_blank"><img src="https://proxy-euf.hosted.exlibrisgroup.com/exl_rewrite/syndetics.com/index.php?client=primo&isbn=0471471356/lc.jpg" alt="Nonlinear Regression" height="100px"/></a> 

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/3hfdli/cdi_jndl_porta_oai_ndlsearch_ndl_go_jp_R100000136_I1130282269013635840" target="_blank">Nonlinear Regression</a>, Seber George Arthur Frederick, Wild Christopher John, Wiley, 2003 {cite}`SeberG.A.F.GeorgeArthurFrederick2003Nr:p`

___

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/3hfdli/cdi_askewsholts_vlebooks_9780387848587" target="_blank"><img src="https://media.springernature.com/w306/springer-static/cover-hires/book/978-0-387-84858-7" alt="The Elements of Statistical Learning : Data Mining, Inference, and Prediction" height="100px"/></a> 

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/3hfdli/cdi_askewsholts_vlebooks_9780387848587" target="_blank">The Elements of Statistical Learning : Data Mining, Inference, and Prediction</a>, Hastie Trevor, Tibshirani Robert, Friedman Jerome, 2009 {cite}`HastieTrevor2009TEoS`

___

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma990005922390306161" target="_blank"><img src="https://proxy-euf.hosted.exlibrisgroup.com/exl_rewrite/syndetics.com/index.php?client=primo&isbn=978-1-492-03264-9/sc.jpg" alt="Livre Deep learning" height="100px"/></a> 

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma990005922390306161" target="_blank">Hands-on machine learning with Scikit-Learn, Keras and TensorFlow : concepts, tools, and techniques to build intelligent systems</a>, Aurélien Géron, O'Reilly, 2019 {cite}`alma990005922390306161`

### Vidéos

<div class="iframe-wrapper">
  <div class="container">
    <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/BquRtCp9lvw" title="1. Intelligence Artificielle, Machine Learning, Deep-Learning... De quoi parle t-on au juste ?" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
  </div>
</div>

<a href="https://www.youtube.com/watch?v=BquRtCp9lvw" target="_blank">1. Intelligence Artificielle, Machine Learning, Deep-Learning... De quoi parle t-on au juste ?</a>, CNRS - Formation FIDLE, 15min 46s, 2023

___

<div class="iframe-wrapper">
  <div class="container">
    <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/LgefkpN0OPE" title="2. De la régression logistique au neurone artificiel..." frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
  </div>
</div>

<a href="https://www.youtube.com/watch?v=LgefkpN0OPE" target="_blank">2. De la régression logistique au neurone artificiel...</a>, CNRS - Formation FIDLE, 17min 11s, 2023

___

<div class="iframe-wrapper">
  <div class="container">
    <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/pxulkadbBnQ" title="3. 80 ans d&#39;histoire... et la victoire des neurones !" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
  </div>
</div>

<a href="https://www.youtube.com/watch?v=pxulkadbBnQ" target="_blank">3. 80 ans d'histoire... et la victoire des neurones !</a>, CNRS - Formation FIDLE, 30min 58s, 2023

### MOOC

Le MOOC "Machine learning in Python with scikit-learn" conçu par les équipes de développeurs de la bibliothèque <a href="https://scikit-learn.org/stable/" target="_blank">Scikit-Learn</a> à l'<a href="https://inria.fr/fr" target="_blank">INRIA</a> permet de comprendre en détails le fonctionnement de cette bibliothèque

<a href="https://www.fun-mooc.fr/fr/cours/machine-learning-python-scikit-learn/" target="_blank"><img src="https://d29emq8to944i.cloudfront.net/9a451970-d466-7593-b50c-a9e38985f137/thumbnails/1618848822_1080.jpg" alt="MOOC Machine learning in Python with scikit-learn" height="100px"/></a> 

<a href="https://www.fun-mooc.fr/fr/cours/machine-learning-python-scikit-learn/" target="_blank">MOOC "Machine learning in Python with scikit-learn"</a>, FUN MOOC, INRIA

## Réseaux de neurones

### Livres

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma991006591685306161" target="_blank"><img src="https://bu.univ-grenoble-alpes.fr/FormAlmaElectre/electre2022b.php?ean=9782100790661" alt="Livre Deep learning" height="100px"/></a> 

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma991006591685306161" target="_blank">Deep learning avec Keras et TensorFlow : mise en oeuvre et cas concrets</a>, Aurélien Géron, O'Reilly, 2020 {cite}`alma991006591685306161`

### Vidéos

<div class="iframe-wrapper">
  <div class="container">
    <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/OZ989EvTIBQ" title="FIDLE / Principes et concepts des réseaux de neurones convolutifs (CNN)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
  </div>
</div>

<a href="https://www.youtube.com/watch?v=OZ989EvTIBQ" target="_blank">FIDLE / Principes et concepts des réseaux de neurones convolutifs (CNN)</a>, CNRS - Formation FIDLE, 30min 58s, 2024

## Enjeux : prendre du recul

### Livres

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma991007368033406161" target="_blank"><img src="https://proxy-euf.hosted.exlibrisgroup.com/exl_rewrite/syndetics.com/index.php?client=primo&isbn=1-80324-615-4/lc.jpg" alt="xAI" height="100px"/></a> 

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma991007368033406161" target="_blank">Applied machine learning explainability techniques : make ML models explainable and trustworthy for practical applications Using LIME, SHAP, and more</a>, Bhattacharya, Aditya, Packt Publishing, 2022 {cite}`alma991007368033406161`

___

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/3hfdli/cdi_oapen_doabooks_98966" target="_blank"><img src="https://proxy-euf.hosted.exlibrisgroup.com/exl_rewrite/syndetics.com/index.php?client=primo&isbn=9783036566009/lc.jpg" alt="xAI" height="100px"/></a> 

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/3hfdli/cdi_oapen_doabooks_98966" target="_blank">Towards the Sustainability of AI; Multi-Disciplinary Approaches to Investigate the Hidden Costs of AI</a>, Ndemeulebroucke Tijs, Van Wynsberghe Aimee, Nachid Jamila, Bolte Larissa, MDPI - Multidisciplinary Digital Publishing Institute, 2023 {cite}`cdi_oapen_doabooks_98966`

### Vidéos

<div class="iframe-wrapper">
  <div class="container">
    <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/4g9qytTvppg" title="FIDLE / AI, droit, société et éthique" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
  </div>
</div>

<a href="https://www.youtube.com/watch?v=4g9qytTvppg" target="_blank">FIDLE / AI, droit, société et éthique</a>, CNRS - Formation FIDLE, 2h 2min 19s, 2023