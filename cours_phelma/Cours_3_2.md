<style>
    /* Pour la gestion des iframes */
 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 📚 Cours : Les réseaux de neurones convolutifs

L'utilisation des réseaux de convolution **ne se limite pas aux images**. Les réseaux de neurones convolutifs à **une dimension** peuvent être utilisés pour analyser des **séries temporelles** {cite}`kiranyaz20191d`. A **trois dimensions**, les réseaux de neurones convolutifs peuvent servir à faire de la **segmentation d'images médicales en 3D** (IRM ou scanners) {cite}`niyas2022medical`.

## La gestion des données

Les images doivent être **variées** (différents angles, éclairages, fonds...) pour que le modèle soit **robuste**. Cette variété des données peut être améliorée grâce de l'augmentation de données. 

### Labellisation des images

{cite}`ign2021etudelabellisation`

une des étapes les plus fastidieuse de l'entrainement de modèle d'IA pour l'image
outils open source et gratuit mais sans aide de l'IA pour la labellisation donc plus long
Récupération des métadonnées de labellisation dans différents types de formats
Fort volume = multiple personnes = outil collaboratif + interface de saisie

#### Formats de labellisation d'images

Les informations dépendent du type de besoin. Elles incluent généralement les catégories auxquelles appartiennent les images (classification), les emplacements des objets spécifiques au sein des images (détection d'objets), les contours précis des objets (segmentation d'instance ou sémantique). Ces metadonnées sont contenus dans des fichiers texte (XML, JSON, CSV...).

L'exemple suivant présente la labellisation pour de la détection d'objets dans 4 formats différents d'une image en 600x400 pixels avec une pomme aux coordonnées de boîte englobante (30, 40, 180, 200) et une banane aux coordonnées de boîte englobante (220, 50, 380, 150) :

- **XML (eXtensible Markup Language)**

Utilisé dans les jeux de données PASCAL VOC (Visual Object Classes)

Exemple :

```xml
<annotation>
    <filename>fruits.jpg</filename>
    <size>
        <width>600</width>
        <height>400</height>
    </size>
    <object>
        <name>apple</name>
        <bndbox>
            <xmin>30</xmin>
            <ymin>40</ymin>
            <xmax>180</xmax>
            <ymax>200</ymax>
        </bndbox>
    </object>
    <object>
        <name>banana</name>
        <bndbox>
            <xmin>220</xmin>
            <ymin>50</ymin>
            <xmax>380</xmax>
            <ymax>150</ymax>
        </bndbox>
    </object>
</annotation>
```

- **JSON (JavaScript Object Notation)**

Utilisé dans les jeux de données comme COCO (Common Objects in Context)

Exemple :

```json
{
  "images": [
    {
      "file_name": "fruits.jpg",
      "height": 400,
      "width": 600
    }
  ],
  "annotations": [
    {
      "image_id": 1,
      "category_id": 1,
      "bbox": [30, 40, 150, 160],
      "object": "apple"
    },
    {
      "image_id": 1,
      "category_id": 2,
      "bbox": [220, 50, 160, 100],
      "object": "banana"
    }
  ],
  "categories": [
    {"id": 1, "name": "apple"},
    {"id": 2, "name": "banana"}
  ]
}
```

- **CSV (Comma-Separated Values)**

Pour des tâches de labellisation plus simples

Exemple :

```
file_name, object, xmin, ymin, xmax, ymax
fruits.jpg, apple, 30, 40, 180, 200
fruits.jpg, banana, 220, 50, 380, 150
```

- **YOLO (You Only Look Once)**

Spécifique au format utilisé par le système de détection d'objets YOLO

Exemple :

```
1 0.175 0.3 0.25 0.4
2 0.5 0.25 0.2666 0.25
```

La première valeur représente l'**identifiant de la catégorie** (1 pour pomme et 2 pour banane) suivie des **coordonnées normalisées** du **centre de la boîte englobante** (x, y) et de sa **largeur** et **hauteur** (w, h).

#### Exemple de code simple pour la labellisation d'images en Python

Ce code suivant est conçu pour labelliser manuellement un sous-ensemble d'images du dataset MNIST, en particulier les chiffres "1". Le but est de distinguer les "1" avec une barre (à la base) et ceux sans barre :

```python
(x_train, y_train), (_, _) = mnist.load_data()
sevens = x_train[y_train == 1]

labels_file = 'mnist_labels.npy'
if os.path.exists(labels_file):
    labels = np.load(labels_file)
else:
    labels = np.full(len(sevens), -1)

def label_image(index):
    plt.imshow(sevens[index], cmap='gray')
    plt.title(f"Image {index} : Tapez 1 pour 'avec barre', 0 pour 'sans barre'")
    plt.show()
    label = input("Label (1 = avec barre, 0 = sans barre) : ")
    return int(label)

for i in range(len(sevens)):
    if labels[i] == -1:  # Si l'image n'a pas encore été labellisée
        labels[i] = label_image(i)
        
        # Sauvegarder les labels après chaque entrée pour éviter de perdre le travail
        np.save(labels_file, labels)
        print(f"Label enregistré pour l'image {i}.")
    else:
        print(f"L'image {i} a déjà été labellisée : {labels[i]}")

print("Labellisation terminée.")
```

## Les différentes couches de neurones

### Les couches de convolutions

Les couches de convolution sont au coeur des réseaux de neurones convolutifs (CNN). Elles sont utilisées pour détecter des motifs dans les données notamment dans les images. Trois paramètres clés contrôlent le fonctionnement de ces couches : le padding, le stride, et la taille du noyau {cite}`dumoulin2018guide`

- **Padding**

Le padding ajoute des pixels autour de l'image d'entrée pour contrôler la taille des sorties après convolution. En utilisant du padding "same", on s'assure que la taille de la sortie est la même que celle de l'entrée. Un padding "valid" ne rajoute pas de pixels, il réduit donc la taille de la sortie.

- **Stride**

Le stride définit le pas de déplacement de la fenêtre de convolution sur l'image. Un stride de 1 signifie que la fenêtre se déplace d'un pixel à la fois. Un stride plus élevé réduit davantage la taille de la sortie en sautant des pixels lors du déplacement.

- **Taille du noyau**

La taille du noyau (ou filtre) détermine la dimension de la fenêtre utilisée pour la convolution (exemple : 3x3 ou 5x5). Un noyau plus grand capture des motifs plus larges dans l'image tandis qu'un noyau plus petit se concentre sur des détails plus fins.

### Les couches de pooling

La **couche de pooling la plus utilisée** dans les réseaux de neurones de convolution est le **max pooling**. L'**average pooling** est aussi utilisé. Ce sont les deux types de couches de pooling dans la bibliothèques Keras mais il en existe **beaucoup d'autres** utilisées en recherche {cite}`gholamalinezhad2020pooling`.

#### Max Pooling

Le max pooling sélectionne la **valeur maximale** dans une fenêtre de la matrice (par exemple 2x2). Cela permet de conserver les **informations les plus importantes** et de **réduire la taille des données**.

```python
from keras.layers import MaxPooling2D

model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
```

#### Average Pooling

L'**average pooling** prend la moyenne des valeurs dans une fenêtre, ce qui lisse les informations.

```python
from keras.layers import AveragePooling2D

model.add(AveragePooling2D(pool_size=(2, 2), strides=(2, 2)))
```

### Les couches de dropout

Les **couches de Dropout** sont utilisées pour **réduire le surapprentissage** en **désactivant aléatoirement** un certain pourcentage de neurones pendant l'entraînement. Cela permet de rendre le **modèle plus robuste** en évitant qu'il ne dépende trop de certaines connexions.

```python
from keras.layers import Dropout

model.add(Dropout(0.5))  # Désactive 50% des neurones
```

### Les couches denses

Les **couches denses** (ou fully connected) connectent chaque neurone de la couche précédente à chaque neurone de la couche suivante. Elles sont généralement utilisées à la fin du réseau pour les tâches de classification.

```python
from keras.layers import Dense

model.add(Dense(units=128, activation='relu'))  # 128 neurones, activation ReLU
```

## Les différentes architectures

### Réseaux convolutifs

Les réseaux convolutifs (CNN) sont spécialement conçus pour traiter des données structurées en grille, comme les images. Ils utilisent des couches de convolution pour capturer les motifs locaux et des couches de pooling pour réduire la dimensionnalité tout en conservant les informations essentielles.

#### LeNet

Le LeNet est l'un des premiers réseaux convolutifs, développé par Yann LeCun pour la reconnaissance de chiffres manuscrits (notamment dans le cadre du dataset MNIST). Il est constitué de couches convolutives suivies de couches de pooling et de couches entièrement connectées.

```python
from keras.models import Sequential
from keras.layers import Conv2D, AveragePooling2D, Flatten, Dense

model = Sequential([
    # Première couche de convolution avec 6 filtres 5x5, activation tanh, et input_shape adapté
    Conv2D(6, kernel_size=(5, 5), activation='tanh', input_shape=(32, 32, 1)),
    # Première couche de pooling avec moyenne (Average Pooling)
    AveragePooling2D(),
    
    # Deuxième couche de convolution avec 16 filtres 5x5, activation tanh
    Conv2D(16, kernel_size=(5, 5), activation='tanh'),
    # Deuxième couche de pooling avec moyenne
    AveragePooling2D(),
    
    # Aplatissement des couches convolutives pour connecter aux couches denses
    Flatten(),
    
    # Première couche entièrement connectée avec 120 neurones, activation tanh
    Dense(120, activation='tanh'),
    
    # Deuxième couche entièrement connectée avec 84 neurones, activation tanh
    Dense(84, activation='tanh'),
    
    # Dernière couche pour la classification avec 10 sorties (pour les 10 classes de MNIST), activation softmax
    Dense(10, activation='softmax')
])

# Affichage de l'architecture du modèle
model.summary()
```

### Réseaux entièrement convolutifs

Les réseaux entièrement convolutifs (FCN - Fully Convolutional Networks) ne contiennent que des couches de convolution, sans couches denses en sortie. Ils sont particulièrement adaptés pour des tâches de segmentation d'image, où l'objectif est de prédire une étiquette pour chaque pixel.

## L'apprentissage par transfert 

L'apprentissage par transfert (transfert learning) est une technique où un modèle pré-entraîné sur une large base de données est réutilisé pour une nouvelle tâche avec un jeu de données plus petit. En pratique, il consiste souvent à geler certaines couches d’un modèle pré-entraîné et à réentraîner les couches supérieures sur la nouvelle tâche :

```python
from keras.applications import VGG16
from keras.models import Sequential
from keras.layers import Dense, Flatten
from keras.optimizers import Adam

# Charger le modèle pré-entraîné VGG16 sans les couches de classification finales
base_model = VGG16(weights='imagenet', include_top=False, input_shape=(224, 224, 3))

# Geler les couches convolutives du modèle pré-entraîné
base_model.trainable = False

# Ajouter les nouvelles couches de classification adaptées à notre tâche
model = Sequential([
    base_model,
    Flatten(),
    Dense(256, activation='relu'),  # Nouvelle couche dense
    Dense(10, activation='softmax')  # Couche de sortie pour 10 classes
])

# Compiler le modèle
model.compile(optimizer=Adam(), loss='categorical_crossentropy', metrics=['accuracy'])

# Affichage de l'architecture du modèle
model.summary()
```

## Les métriques spécifiques

Les **métriques classiques d'apprentissage automatique** sont utiles pour évaluer les réseaux de neurones mais l'analyse des tâches de vision par ordinateur requiert des **métriques spécialisées** {cite}`metrics`. Les bibliothèques Keras et PyTorch proposent des fonctions spécifiques pour ces mesures avancées :

- **Keras** fournit des <a href="https://keras.io/api/metrics/segmentation_metrics/" target="_blank">métriques pour la segmentation</a>

- **PyTorch (avec TorchMetrics)** fournit une <a href="https://lightning.ai/docs/torchmetrics/stable/" target="_blank">gamme complète de métriques pour le deep learning</a>

### Ratio d’intersection sur union (IoU)

L'IoU est utilisée pour quantifier la **précision des prédictions** d'un modèle par rapport aux **annotations**. Elle est utilisée pour des tâches de **détection d'objets** et de **segmentation**. Elle permet de définir un **seuil** pour savoir si la détection d'objets ou la segmentation est correcte.

<img src="./_static/IoU.png" alt="IoU"/>

La valeur de l'IoU varie entre **0 et 1**, où **0** indique **aucune similitude** (pas de chevauchement) et **1** indique une **similitude parfaite** (chevauchement total entre la prédiction et la vérité terrain).

```{admonition} Indice de Jaccard
L'IoU et l'<a href="https://fr.wikipedia.org/wiki/Indice_et_distance_de_Jaccard" target="_blank">indice de Jaccard</a> sont deux appellations pour la même mesure de similarité. Le terme **IoU** est plus utilisé dans le **domaine de la vision par ordinateur** tandis que l'**indice de Jaccard** apparaît plus dans le **domaine de l'apprentissage automatique classique**. L'indice de Jaccard est présent dans les <a href="https://scikit-learn.org/stable/modules/generated/sklearn.metrics.jaccard_score.html" target="_blank">métriques de scikit-learn</a>.
```

Il existe **différentes variantes** de l'IoU (présentes dans <a href="https://lightning.ai/docs/torchmetrics/stable/" target="_blank">TorchMetrics</a>) :

- **Generalized Intersection over Union (GIoU)** : améliore l'IoU en gérant les cas **sans chevauchement**. Elle peut être utilisée comme **fonction de perte**.

```{important}
Les **métriques** et les **fonctions de perte** jouent des **rôles complémentaires** : les **fonctions de perte** guident l'**optimisation du modèle** tandis que les **métriques** fournissent une **évaluation de sa performance globale**.
```

Papier de recherche : Generalized Intersection over Union : A Metric and A Loss for Bounding Box Regression {cite}`giou`

- **Distance Intersection Over Union (DIoU)** : améliore davantage l'évaluation que GIoU en intégrant la **distance entre les centres** des boîtes de prédiction et de vérité terrain

Papier de recherche : Distance-IoU Loss: Faster and Better Learning for Bounding Box Regression {cite}`zheng2019distanceiou`

- **Complete Intersection Over Union (CIoU)** : extension de DIoU qui inclut également un terme de pénalité pour les **différences d'aspect de forme** entre les boîtes de prédiction et de vérité terrain

Papier de recherche : ICIoU: Improved Loss Based on Complete Intersection Over Union for Bounding Box Regression : {cite}`iciou`

### mean Average Precision (mAP)

Le mean Average Precision (mAP) est une métrique couramment utilisée pour évaluer les performances des modèles de détection d'objets et de recherche d'images. Le mAP est calculé en prenant la moyenne des précisions obtenues à différents niveaux de rappel, pour chaque classe d'objets, puis en faisant la moyenne sur toutes les classes.

Le mAP est particulièrement utile pour les modèles de détection d'objets, car il évalue non seulement si le modèle a détecté les objets correctement, mais aussi s'il a bien localisé leurs positions.

## Les applications des réseaux convolutifs

### La classification d'images

La classification d'images consiste à assigner une étiquette à une image entière. Le modèle extrait des caractéristiques visuelles grâce à des couches de convolution puis utilise des couches entièrement connectées pour classer l'image en fonction de ces caractéristiques.

### La détection d'objets

La détection d'objets va au-delà de la classification d'images en identifiant non seulement la classe d'un objet dans une image mais aussi sa position sous forme de boîte englobante. Deux des algorithmes les plus populaires pour la détection d'objets sont R-CNN et YOLO.

#### R-CNN

L'algorithme R-CNN fonctionne en deux étapes :

1. **Proposition de régions** : Le modèle sélectionne plusieurs régions d'intérêt dans l'image.

2. **Classification des objets** : Chaque région est ensuite classifiée pour déterminer s'il y a un objet et à quelle classe il appartient.

#### You only Look Once (YOLO)

Le modèle YOLO prend une approche différente en traitant la détection d'objets comme un problème de régression unique. Plutôt que de proposer des régions puis de les classer, YOLO divise l'image en une grille et fait une seule prédiction pour chaque cellule de la grille en une seule passe. Cela rend YOLO très rapide et bien adapté pour des applications en temps réel, tout en étant suffisamment précis pour de nombreuses tâches de détection d'objets {cite}`redmon2016look`

### La segmentation sémantique

La segmentation sémantique est une tâche qui consiste à attribuer une étiquette à chaque pixel d'une image. La segmentation sémantique ne différencie cependant pas les instances individuelles d'une même classe (par exemple, plusieurs voitures seront toutes étiquetées de la même manière). Les architectures couramment utilisées pour la segmentation sémantique incluent Fully Convolutional Networks (FCN) et U-Net.

### La segmentation d'instances

La segmentation d'instances va plus loin que la segmentation sémantique en distinguant les instances individuelles d'une même classe. L'un des algorithmes les plus populaires pour la segmentation d'instances est Mask R-CNN.

### Benchmarks

Les benchmarks jouent un rôle crucial dans l'évaluation et la comparaison des performances des modèles. Ils fournissent des ensembles de **données standardisés**, des **protocoles d'évaluation** et des **classements** pour permettre aux chercheurs de tester et de comparer leurs méthodes. Voici quelques-uns des benchmarks les plus connus dans ce domaine :

- <a href="http://yann.lecun.com/exdb/mnist/" target="_blank">MNIST (Mixed National Institute of Standards and Technology)</a>

<a href="https://fr.wikipedia.org/wiki/Base_de_données_MNIST" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/2/27/MnistExamples.png" alt="MNIST" height="200px"/></a>

*Source de l'image : wikipedia*

Le jeu de données MNIST est probablement le **plus célèbre** dans le domaine de la reconnaissance d'images, offrant un ensemble simple et largement utilisé pour l'entraînement de modèles d'apprentissage automatique sur la **reconnaissance des chiffres manuscrits** de 0 à 9. Le jeu contient **60000 images** de résolution **28x28** pixels en **niveaux de gris**. Des versions augmentées comme <a href="https://github.com/zalandoresearch/fashion-mnist" target="_blank">Fashion-MNIST</a> {cite}`DBLP:journals/corr/abs-1708-07747` (images d'articles de mode) et <a href="https://www.nist.gov/itl/products-and-services/emnist-dataset" target="_blank">EMNIST</a> {cite}`DBLP:journals/corr/CohenATS17` (éventail plus large de caractères manuscrits) ont été développées pour fournir des **images plus complexes**.

- <a href="https://www.cs.toronto.edu/~kriz/cifar.html" target="_blank">CIFAR 10 et CIFAR 100 (Canadian Institute For Advanced Research)</a>

<img src="https://www.cs.toronto.edu/~kriz/cifar-10-sample/automobile4.png" alt="CIFAR10" height="40px"/>
<img src="https://www.cs.toronto.edu/~kriz/cifar-10-sample/airplane4.png" alt="CIFAR10" height="40px"/>
<img src="https://www.cs.toronto.edu/~kriz/cifar-10-sample/bird10.png" alt="CIFAR10" height="40px"/>
<img src="https://www.cs.toronto.edu/~kriz/cifar-10-sample/dog4.png" alt="CIFAR10" height="40px"/>
<img src="https://www.cs.toronto.edu/~kriz/cifar-10-sample/ship7.png" alt="CIFAR10" height="40px"/>

*Source : <a href="https://scholar.google.com/citations?user=xegzhJcAAAAJ&hl=fr" target="_blank">Alex Krizhevsky</a>*

Le jeu de données **CIFAR-10** est un ensemble standard utilisé pour l'apprentissage automatique en vision par ordinateur. Il est composé de **60000 images** en **couleur** de **32x32** pixels réparties en **10 classes** (incluant des animaux et des véhicules). **CIFAR-100** est une version plus complexe du même ensemble, offrant **100 classes** contenant chacune 600 images, ce qui permet d'aborder des problèmes de classification plus fins et diversifiés.

- <a href="https://cocodataset.org/" target="_blank">MS COCO (Microsoft Common Objects in Context)</a>

<a href="https://cocodataset.org/#home" target="_blank"><img src="https://cocodataset.org/images/coco-examples.jpg" alt="COCO Dataset" height="200px"/></a>

*Source : Microsoft COCO dataset {cite}`lin2015microsoft`*

Il s'agit d'une **base de données large et diversifiée** spécialement conçue pour la détection d'objets, la segmentation de scènes et la génération de légendes d'images. Il comprend des images de la **vie quotidienne** avec des annotations détaillées, y compris des boîtes englobantes et des masques de segmentation.

- <a href="https://image-net.org/challenges/LSVRC/" target="_blank">ILSVRC (ImageNet Large Scale Visual Recognition Challenge)</a>

<a href="https://image-net.org/index.php" target="_blank"><img src="https://production-media.paperswithcode.com/datasets/ImageNet-0000000008-f2e87edd_Y0fT5zg.jpg" alt="Imagenet Dataset" height="200px"/></a>

*Source : https://paperswithcode.com/dataset/imagenet*

Il s'agit d'un challenge basé sur la **base de données ImageNet** qui contient des millions d'images classées en milliers de catégories. 

- <a href="http://host.robots.ox.ac.uk/pascal/VOC/" target="_blank">PASCAL VOC (Visual Object Classes)</a>

Il s'agit d'une des **premières bases de données** pour la **détection d'objets**, la **segmentation d'images** et la **classification d'images**.

- <a href="https://www.cvlibs.net/datasets/kitti/index.php" target="_blank">KITTI Vision Benchmark Suite</a>

Il s'agit d'nn benchmark axé sur les applications de vision par ordinateur dans le contexte de la **conduite automobile**. Il contient des **images** et des **données lidar** de scènes routières, avec des annotations pour la détection de voitures, de piétons, de cyclistes...

- <a href="https://storage.googleapis.com/openimages/web/index.html" target="_blank">Open Images Dataset</a>

Il s'agit d'un ensemble de données de Google contenant un **grand nombre d'images annotées**. Il comprend des annotations pour la **détection d'objets**, la **segmentation d'instance** et des **annotations de relations visuelles**.

- <a href="https://www.aicitychallenge.org/" target="_blank">AI City Challenge</a>

Il s'agit d'un challenge axé sur l'intelligence artificielle dans le contexte des **villes intelligentes**. Il comprend des tâches telles que la détection d'objets dans des **vidéos de trafic urbain**.