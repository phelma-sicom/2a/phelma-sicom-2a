<style>
    /* Pour la gestion des iframes */
 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 📚 Cours : Limites des prédictions des modèles

## Limites en amont de l'entrainement du modèle

### Définition de la qualité des données

- **Précision**

Les données doivent refléter la réalité de manière exacte. Des erreurs dans les données (fautes de frappe, des mesures incorrectes...) peuvent induire en erreur les modèles​

- **Complétude**

Il ne doit pas manquer de valeurs critiques​

- **Cohérence**

Les données doivent être cohérentes à travers toutes les sources (formats, unités de mesure...)​

- **Pertinence**

Les données doivent être pertinentes par rapport à la tâche que le modèle doit accomplir​

- **Représentativité**

Les données doivent représenter de manière fidèle la population (risque de biais d'échantillonnage)

### Importance de la qualité des données

- Pour garantir la qualité des prédictions des modèles : "garbage in, garbage out"​

- Pour réduire les risques de biais dans les prédictions des modèles : jeu de données déséquilibré (sous-représentation de certaines classes)​

- Pour améliorer la généralisation

### Données aberrantes

#### Définition

Valeurs qui s'écartent significativement des autres observations dans un jeu de données​

#### Origine

Erreurs de mesure, anomalies, capteur défaillant, faute de frappe, événements rares...

#### Détection

- **Méthodes statistiques :** z-score, distance interquartile (IQR)​
- **Méthodes graphiques :** boîtes à moustaches (box plots)​
- **Méthodes basées sur des modèles :** calcul des résidus pour la régression, isolation forest, local outlier factor (LOF), knn, DBSCAN...

#### Gestion

- **Modification du jeu de données :** suppression ou modification des données aberrantes​
- **Choix de modèles robustes aux données aberrantes :** choix de fonctions de perte ou de modèles robustes aux données aberrantes

### Données manquantes

#### Catégories

- **Données manquantes complètement aléatoires :**

Les données manquantes sont indépendantes des autres variables et de la variable elle-même. Pas de biais à la suppression​

- **Données manquantes aléatoires :​**

Les données manquantes dépendent d'autres variables observables mais pas de la variable qui contient les données manquantes​

- **Données manquantes non aléatoires :​**

Les données manquantes dépendent de la variable elle-même

#### Détection

- Comparaison de grandeurs statistiques sur les données manquantes et présentes​

- Analyse des distributions des données manquantes et présentes​

- Analyse visuelle de la localisation des données manquantes grâce à des heat maps​

- Test statistique de Little 

#### Gestion

- Suppression des données manquantes : lignes ou colonnes​

- Imputation des données manquantes par des grandeurs statistiques : moyenne, médiane ou mode​

- Imputation des données manquantes par des modèles : knn, régression linéaire, arbres de décision, GAN, VAE...

### Données déséquilibrées

#### Définition

Un jeu de données est déséquilibré lorsqu’une ou plusieurs classes ont significativement moins d'exemples que les autres.

#### Enjeux

- Impact sur les métriques d’évaluation (accuracy)​

- Modèle biaisé vers la classe majoritaire​

- Faible performance sur la classe minoritaire

#### Gestion

- Sous-échantillonnage de la classe majoritaire​

- Suréchantillonnage de la classe minoritaire (avec la méthode SMOTE par exemple)​

- Utilisation de métriques adaptées au déséquilibre (rappel, précision, F1-score, AUC)​

- Modèles pondérés

#### Exemple

Pour illustrer les problématiques liées aux jeux de données déséquilibrés, nous allons utiliser une version légèrement modifiée du jeu de données <a href="https://fr.wikipedia.org/wiki/Fashion_MNIST" target="_blank">Fashion MNIST</a>. Fashion MNIST est un jeu de données de classification composé de 10 classes d'images de vêtements. Ce jeu de données est plus complexe à prédire que le classique MNIST.

<a href="./_static/fashion_mnist.gif" target="_blank"><img src="./_static/fashion_mnist.gif" alt="Fashion MNIST" /></a>

Nous commençons par entraîner un modèle simple de régression logistique sur ce jeu de données. Les métriques obtenues sont les suivantes :

```python
accuracy = accuracy_score(y_test, y_pred)
precision = precision_score(y_test, y_pred, average='micro')
recall = recall_score(y_test, y_pred, average='micro')
f1 = f1_score(y_test, y_pred, average='micro')

print("Métriques du modèle de régression logistique")
print(f"Justesse : {round(accuracy, 3)}")
print(f"Précision (micro) : {round(precision,3)}")
print(f"Rappel (micro) : {round(recall, 3)}")
print(f"Score F1 (micro) : {round(f1, 3)}")
```
```
Métriques du modèle de régression logistique
Justesse : 0.897
Précision (micro) : 0.897
Rappel (micro) : 0.897
Score F1 (micro) : 0.897
```

Les résultats semblent satisfaisants à première vue. Toutefois, pour améliorer encore les performances, nous allons essayer un modèle plus complexe. Nous entraînons donc un réseau de neurones de convolution avec l'architecture suivante :

<a href="./_static/architecture_reseau.png" target="_blank"><img src="./_static/architecture_reseau.png" alt="Architecture réseau" /></a>

Nous obtenons les résultats suivants :

```
Métriques du modèle de réseau de neurones de convolution
Justesse : 0.93
Précision (micro) : 0.93
Rappel (micro) : 0.93
Score F1 (micro) : 0.93
```

Les résultats sont meilleurs qu'avec la régression logistique, mais cela signifie-t-il que notre modèle est prêt pour être déployé en production ? Pas encore…

Nous devons procéder à une dernière vérification en calculant les métriques dans leur version "macro".

```{admonition} micro macro weighted
Pour les **métriques de classification**, les termes micro, macro et weighted font référence à la manière dont les scores sont calculés pour de la classification multiclasse.

- **micro** : le calcul micro considère tous les échantillons et toutes les classes ensemble comme une **seule entité**. Cela signifie qu'il calcule les totaux globaux des vrais positifs/négatifs et faux positifs/négatifs pour l'ensemble des classes puis calcule la métrique sur ces totaux.

- **macro** : le calcul macro fait la **moyenne des métriques de chaque classe**. Il calcule la précision, le rappel et le F1 score pour chaque classe indépendamment puis prend la **moyenne de ces métriques**.

- **weighted** : le calcul weighted prend en compte le **poids des classes**, c'est-à-dire le **nombre d'échantillons dans chaque classe**. Il calcule la précision, le rappel et le F1 score pour chaque classe indépendamment puis prend la **moyenne pondérée de ces métriques**.
```

Voici les résultats obtenus pour le modèle de réseau de neurones de convolution avec la version macro des métriques :

```
Métriques du modèle de réseau de neurones de convolution
Justesse : 0.93
Précision (macro) : 0.914
Rappel (macro) : 0.853
Score F1 (macro) : 0.857
```

On remarque que, bien que la précision micro soit élevée, les résultats sont moins bons en version macro. Cela peut être dû à un déséquilibre des classes dans le jeu de données, ce qui peut pénaliser les classes sous-représentées.

Examinons maintenant la matrice de confusion. Les données affichées dans la matrice de confusion correspondent au **pourcentage des données de la véritable classe** ce qui correspond à la métrique de **rappel** (recall en anglais) :

<a href="./_static/matrice_confusion_fashion_1.png" target="_blank"><img src="./_static/matrice_confusion_fashion_1.png" alt="Matrice de confusion" /></a>

On observe que la classe "Chemise" est extrêmement mal prédite. Voyons maintenant la matrice de confusion pour le modèle de régression logistique :

<a href="./_static/matrice_confusion_fashion_0.png" target="_blank"><img src="./_static/matrice_confusion_fashion_0.png" alt="Matrice de confusion" /></a>

Bien que la performance globale du modèle de régression logistique soit inférieure à celle du réseau de neurones de convolution, on observe que ce modèle prédit mieux la classe "Chemise". Cela illustre le fait qu'augmenter la complexité du modèle n'améliore pas forcément la qualité des prédictions pour les classes sous représentées.

Enfin, en analysant les métriques par classe pour le réseau de neurones, on obtient le rapport de classification suivant :

<a href="./_static/classification_report_1.png" target="_blank"><img src="./_static/classification_report_1.png" alt="Classification report" /></a>

Et pour le modèle de régression logistique :

<a href="./_static/classification_report_0.png" target="_blank"><img src="./_static/classification_report_0.png" alt="Classification report" /></a>


### Données empoisonnées

#### Différentes techniques

- Injection de données incorrectes dans le jeu de données d'entrainement pour diminuer la qualité des prédictions​

- Injection de données incorrectes très spécifiques dans le jeu de données d'entrainement pour modifier la prédiction du modèle dans certains cas précis (attaques par porte dérobée)

#### Méthodes de protection

- Contrôle de qualité des données : analyser les distributions des données d'entrainement​

- Détection d'anomalies : utiliser des techniques similaires à la détection des données aberrantes​

- Robustesse des modèles : utiliser des modèles robustes à des légères variations dans les données d'entrée

### Traçabilité des modifications

- Le choix des traitements des données aberrantes, manquantes et déséquilibrées peut avoir un impact sur le résultat des modèles ​

- Importance de conserver une trace des modifications effectuées sur le jeu de données d'origine

### Biais dans les données d'entraînement

Pour illustrer les **biais** qui peuvent exister **dans les données**, nous allons utiliser les différences d'écriture des **chiffres manuscrits 1 et 7**. Souvent en France, on ajoute une barre au 7 pour le différencier du 7 anglo-saxon. De la même façon, les 1 manuscrits possèdent souvent une barre horizontale en bas en France.

Nous allons donc commencer par **extraire les 1 et les 7** écrits à la française du jeu de données MNIST. Pour cela, on **labellise manuellement** une centaine de 1 et de 7, puis on entraîne un modèle de classification pour séparer les 1 et les 7 écrits à la française des autres 1 et 7. On obtient le résultat suivant pour les 1 :

<a href="./_static/verification_images_1.png" target="_blank"><img src="./_static/verification_images_1.png" alt="Vérification 1" /></a>

Et le résultat suivant pour les 7 :

<a href="./_static/verification_images_7.png" target="_blank"><img src="./_static/verification_images_7.png" alt="Vérification 7" /></a>

Les résultats de la séparation sont **plutôt satisfaisants**.

On entraîne un modèle de réseau de neurones le plus simple possible : un **neurone en sortie** avec une **fonction d'activation sigmoïde**.

<a href="./_static/architecture.png" target="_blank"><img src="./_static/architecture.png" alt="Architecture" /></a>

L'entraînement de ce modèle est effectué sur un **jeu de données dont on a extrait les 1 et les 7 avec des barres**. On teste les performances de ce modèle sur des données qui ne contiennent pas de 1 et 7 avec des barres. On obtient la matrice de confusion suivante :

<a href="./_static/Matrice_sans.png" target="_blank"><img src="./_static/Matrice_sans.png" alt="Matrice sans" /></a>

On peut aussi analyser la sortie de la fonction d'activation sigmoïde :

<a href="./_static/Sigmoide_sans.png" target="_blank"><img src="./_static/Sigmoide_sans.png" alt="Sigmoide sans" /></a>

On observe qu'il n'y a que deux erreurs : deux 7 ont été classifiés comme des 1. Voici les deux images qui ont été mal classifiées :

<a href="./_static/Erreurs_sans.png" target="_blank"><img src="./_static/Erreurs_sans.png" alt="Erreurs sans" /></a>

Dans un second temps, on teste les performances de ce modèle sur le jeu de données contenant principalement des 1 et des 7 avec des barres. On obtient la matrice de confusion suivante :

<a href="./_static/Matrice_avec.png" target="_blank"><img src="./_static/Matrice_avec.png" alt="Matrice avec barre" /></a>

On peut aussi analyser la sortie de la fonction d'activation sigmoïde :

<a href="./_static/Sigmoide_avec.png" target="_blank"><img src="./_static/Sigmoide_avec.png" alt="Sigmoide avec barre" /></a>

Voici les 12 erreurs sur les 1 :

<a href="./_static/Erreurs_avec_1.png" target="_blank"><img src="./_static/Erreurs_avec_1.png" alt="Erreurs avec 1" /></a>

Et les 22 erreurs sur les 7 :

<a href="./_static/Erreurs_avec_7.png" target="_blank"><img src="./_static/Erreurs_avec_7.png" alt="Erreurs avec 7" /></a>

On observe que le modèle a beaucoup plus de difficultés à classifier ces images qui ne sont pas présentes dans le jeu de données d'entraînement.


## Choix du modèle

Choix du modèle et des hyperparamètres : facteur déterminant pour la qualité des prédictions​

Dépend de plusieurs éléments :​

- Complexité : risques de sous-apprentissage pour les modèles trop simples et de surapprentissage pour les modèles trop complexes​

- Jeu de données spécifiques : images, texte, séries temporelles...

### Biais dans le choix du modèle


```{note}
Le code Python utilisé pour générer les exemples présentés dans cette section est disponible et modifiable dans ce notebook :

<a href="https://phelma-sicom.gricad-pages.univ-grenoble-alpes.fr/2a/4pmsiia4-td/retro/notebooks/?path=0_Cours/Biais_modele.ipynb" target="_blank"><img src="./_static/phelma_Jupyterlite.png" alt="Jupyter Lite Phelma" height="30px"/></a>

```


Pour illustrer le **biais lié aux modèles**, on peut utiliser un **modèle très simple** de régression linéaire sans ordonnée à l'origine avec **un seul paramètre de pente**. Voici à quoi ressemblent les données :

<a href="./_static/donnees_sans_classes.png" target="_blank"><img src="./_static/donnees_sans_classes.png" alt="Données source" /></a>

Nous entraînons un modèle de régression linéaire avec pour seul paramètre la pente (pas d'ordonnée à l'origine) sur le jeux d'entraînement issus de l'ensemble de données. Nous obtenons la courbe suivante :

<a href="./_static/regression_sans_classes.png" target="_blank"><img src="./_static/regression_sans_classes.png" alt="Données source" /></a>

On peut analyser la qualité des prédictions en analysant les métriques de régression :

```
Métriques des données

Mean Squared Error (MSE) : 184.59
Mean Absolute Error (MAE) : 11.47
R-squared (R²) : 0.77
```

Et an analysant les résidus :

<a href="./_static/residus_global.png" target="_blank"><img src="./_static/residus_global.png" alt="Données source" /></a>

La qualité des prédictions est moins bonne pour les faibles valeurs de X.

Les données sont en réalité constituées de **deux parties** : une partie pour `X<20` (en bleu) et une partie pour `X≥20` (en rouge).

<a href="./_static/regression_0.png" target="_blank"><img src="./_static/regression_0.png" alt="Régression linéaire" /></a>

Nous entraînons **deux modèles** de régression linéaire chacun avec pour seul paramètre la pente (pas d'ordonnée à l'origine) sur les deux jeux d'entraînement issus des deux ensembles de données (bleu et rouge). Nous obtenons les courbes suivantes :

<a href="./_static/regression_1.png" target="_blank"><img src="./_static/regression_1.png" alt="Régression linéaire" /></a>

On observe que les données pour les **faibles valeurs** de `X` sont **moins bien prédites**. Cela peut être vérifié en analysant le **graphique des résidus** :

<a href="./_static/residus_1.png" target="_blank"><img src="./_static/residus_1.png" alt="Résidus" /></a>

Cette observation est confirmée par l’analyse des **métriques de régression** :

```
Métriques des données X<20 (en bleu)

Mean Squared Error (MSE) : 392.78
Mean Absolute Error (MAE) : 17.52
R-squared (R²) : -1.01

Métriques des données X>=20 (en rouge)

Mean Squared Error (MSE) : 38.26
Mean Absolute Error (MAE) : 5.19
R-squared (R²) : 0.75
```

Les **mêmes différences de qualité** de prédiction pour les faibles valeurs de `X` sont observées lorsque le modèle est entraîné sur un jeu de données combiné (données bleues et rouges) :

<a href="./_static/regression_2.png" target="_blank"><img src="./_static/regression_2.png" alt="Régression linéaire" /></a>

Des résultats similaires sont obtenus pour les résidus :

<a href="./_static/residus_2.png" target="_blank"><img src="./_static/residus_2.png" alt="Résidus" /></a>

Ces résultats sont également confirmés par les métriques de régression :

```
Métriques des données X<20 (en bleu)

Mean Squared Error (MSE) : 345.22
Mean Absolute Error (MAE) : 17.74
R-squared (R²) : -1.44

Métriques des données X>=20 (en rouge)

Mean Squared Error (MSE) : 53.17
Mean Absolute Error (MAE) : 6.34
R-squared (R²) : 0.72
```

Nous sommes donc dans un **cas de sous-apprentissage** : le modèle est **trop simple pour capturer la complexité** de l'ensemble des données.

Ce biais qui affecte particulièrement les prédictions pour les faibles valeurs de `X` peut être réduit en ajoutant un paramètre au modèle permettant ainsi d'apprendre une ordonnée à l'origine. Avec ce **modèle enrichi**, nous constatons qu'il prédit bien mieux l'ensemble des données, y compris celles correspondant aux faibles valeurs de `X` :

<a href="./_static/regression_3.png" target="_blank"><img src="./_static/regression_3.png" alt="Régression linéaire" /></a>

Les résidus sont désormais similaires pour les deux sous-parties des données :

<a href="./_static/residus_3.png" target="_blank"><img src="./_static/residus_3.png" alt="Résidus" /></a>

L'analyse des métriques de régression confirme également cette amélioration :

```
Métriques des données X<20 (en bleu)

Mean Squared Error (MSE) : 8.16
Mean Absolute Error (MAE) : 2.29
R-squared (R²) : 0.94

Métriques des données X>=20 (en rouge)

Mean Squared Error (MSE) : 18.74
Mean Absolute Error (MAE) : 3.52
R-squared (R²) : 0.90
```

## Limites en aval de l'entrainement du modèle

Une fois le modèle entraîné, une mauvaise interprétation des prédictions peut limiter la qualité des résultats.

### Paradoxe de Simpson

Le paradoxe de Simpson se manifeste lorsqu'une **tendance observée** dans plusieurs groupes disparaît ou s'inverse lorsqu'on les **combine**.

Le graphique suivant illustre ce paradoxe. Il s'agit d'un **exemple fictif** d'une étude sur l'**impact du tabagisme** sur la **vitesse de course**. Voici les données fictives :

<a href="./_static/simpson_0.png" target="_blank"><img src="./_static/simpson_0.png" alt="Paradoxe de Simpson" /></a>

On observe que, pris globalement, **plus les personnes fument, plus elles courent vite** :

<a href="./_static/simpson_1.png" target="_blank"><img src="./_static/simpson_1.png" alt="Paradoxe de Simpson" /></a>

Cela semble paradoxal. Maintenant, en ajoutant une **nouvelle caractéristique**, le genre de la personne (en vert pour les femmes et en jaune pour les hommes) :

<a href="./_static/simpson_2.png" target="_blank"><img src="./_static/simpson_2.png" alt="Paradoxe de Simpson" /></a>

En entraînant un modèle de régression linéaire sur chacun de ces sous-groupes, on retrouve bien les **résultats intuitifs** : plus on fume, moins on court vite.

<a href="./_static/simpson_3.png" target="_blank"><img src="./_static/simpson_3.png" alt="Paradoxe de Simpson" /></a>


<div class="iframe-wrapper">
    <div class="container">
<iframe class="responsive-iframe" title="Voyages au pays des maths" allowfullscreen="true" frameborder="0" scrolling="no" src="https://www.arte.tv/embeds/fr/107398-002-A?autoplay=false&mute=1"></iframe>
    </div>
</div>

*Source : <a href="https://www.arte.tv/fr/videos/RC-021426/voyages-au-pays-des-maths/" target="_blank">Voyages au pays des maths</a>, ARTE, 2022*

```{note}
En 2021, la chaîne de télévision Arte a lancé une mini-série documentaire sur la vulgarisation de concepts mathématiques intitulée <a href="https://fr.wikipedia.org/wiki/Voyages_au_pays_des_maths" target="_blank">Voyages au pays des maths</a>.

<div class="iframe-wrapper">
    <div class="container">
<iframe class="responsive-iframe" title="Voyages au pays des maths" allowfullscreen="true" frameborder="0" scrolling="no" src="https://www.arte.tv/embeds/fr/RC-021426?autoplay=false&mute=1"></iframe>
    </div>
</div>

```

## Support de présentation du cours

<a href="./_static/slides/Cours_enjeux.pdf" target="_blank">👨‍🏫 Support de cours sur les enjeux</a>

<div class="container">
  <iframe class="responsive-iframe" src="./_static/slides/Cours_enjeux.pdf" width="600" height="340" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div>