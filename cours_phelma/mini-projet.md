<style>

   .iframe-wrapper {
  max-width: 60%;
  margin: auto; /* pour centrer le conteneur */
}

    /* Pour la gestion des iframes */
 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 📄 Énoncé

L'objectif de ce mini-projet est d'entrainer un modèle de classification d'images en appliquant toutes les connaissances acquises durant le cours. Le jeu de données a été construit à partir des données de l'application [QuickDraw](https://quickdraw.withgoogle.com/data).

<a href="./_static/QuickDraw.png" target="_blank"><img src="./_static/QuickDraw.png" alt="Quick Draw" /></a>

Un certain nombre de traitements ont été effectué sur ces données. Les images de 10 classes ont été conservées pour leur lien avec Grenoble et/ou Phelma :

- Tente (classe 0)
- Sac à dos (classe 1)
- Piano (classe 2)
- Pingouin (classe 3)
- Dragon (classe 4)
- Vélo (classe 5)
- Bonhomme de neige (classe 6)
- Flocon de neige (classe 7)
- Ordinateur portable (classe 8)
- Montagne (classe 9)

<a href="./_static/quickdraw.gif" target="_blank"><img src="./_static/quickdraw.gif" alt="10 classes" /></a>

## Jeu de données

Le jeu de données contient **60 000 images** de **10 classes différentes** : Tente, Sac à dos, Piano, Pingouin, Dragon, Vélo, Bonhomme de neige, Flocon de neige, Ordinateur portable, Montagne

⬇️ <a href="https://gricad-gitlab.univ-grenoble-alpes.fr/phelma-sicom/2a/4pmsiia4-td/-/raw/main/content/3_mini-projet/dataset_etudiant.npz?ref_type=heads&inline=false" target="_blank">Lien pour télécharger le jeu de données</a>

Voici le code Python pour récupérer le jeu de données dans des **tableaux Numpy** :

```python
import numpy as np

data = np.load('dataset_etudiant.npz')
X = data['images']
y = data['labels']
```

## Export du modèle au format ONNX

Le format <a href="https://onnx.ai/" target="_blank">ONNX (Open Neural Network Exchange)</a> est un **standard ouvert** conçu pour permettre l’**interopérabilité** entre différents frameworks d’apprentissage automatique (Scikit-Learn, Pytorch, Keras, TensorFlow...). Dans le cadre de ce mini-projet, vous devez fournir votre **modèle entrainé au format ONNX**. Suivant la bibliothèque que vous avez choisi d'utiliser, la procédure de transformation du modèle (noté `model` dans le code suivant) au format ONNX est différente :

- **Pour des modèles Scikit-learn :**

Utilisez la bibliothèque `skl2onnx`. Après avoir entraîné votre modèle, exportez-le en ONNX à l'aide de la fonction `convert_sklearn`, en spécifiant le modèle et les types d’entrée comme arguments. Exemple :

```python
from skl2onnx import convert_sklearn
from skl2onnx.common.data_types import FloatTensorType

initial_type = [('float_input', FloatTensorType([None, 784]))] # 784 car image 28x28 applatie (à adapter) 
onnx_model = convert_sklearn(model, initial_types=initial_type)
with open("model_sklearn.onnx", "wb") as f:
    f.write(onnx_model.SerializeToString())
```

- **Pour des modèles Pytorch :**

Utilisez la fonction `torch.onnx.export`. Assurez-vous d'avoir un modèle Pytorch déjà entraîné, puis fournissez un exemple d’entrée pour tracer le modèle lors de l'exportation. Exemple :

```python
import torch

dummy_input = torch.randn(1, 28, 28) # image 28x28 (à adapter) 
torch.onnx.export(model, dummy_input, "model_pytorch.onnx", 
                  export_params=True, opset_version=11, 
                  input_names=['input'], output_names=['output'])
```

- **Pour des modèles Keras :**

Installez `tf2onnx` pour convertir un modèle Keras sauvegardé. Exemple avec tf2onnx :

```python
import tensorflow as tf
import tf2onnx

model.output_names = None
onnx_model_path = "model_keras.onnx"
input_signature = [tf.TensorSpec([None, 28, 28, 1], tf.float32, name="input")]
onnx_model, _ = tf2onnx.convert.from_keras(
    model,
    input_signature=input_signature,
    opset=13,
    output_path=onnx_model_path
)
```

## Evaluation du projet

Pour vérifier que votre modèle fonctionne correctement au format ONNX et qu'il généralise bien, vous devez le tester dans ce notebook (sous Jupyter Hub ou Colab) :

<a href="https://gricad-jupyter.univ-grenoble-alpes.fr/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fphelma-sicom%2F2a%2F4pmsiia4-td&urlpath=tree%2F4pmsiia4-td%2Fcontent%2F3_mini-projet%2FTest_atelier_etudiant.ipynb&branch=main"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/1200px-Jupyter_logo.svg.png" alt="Jupyter Notebook" height="50px"/></a>

ou

<a href="https://colab.research.google.com/github/Pierre-Loic/quantification/blob/main/Test_atelier_etudiant.ipynb" target="_blank">
  <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab" height="25px"/>
</a>

### Fichiers à rendre :

- Notebook d’entrainement et d’analyse du modèle

- Un rapport de 3 pages maximum (au format classique) au **format pdf : il faudra bien expliquer la démarche utilisée. Plus que la performance finale obtenue, celle-ci sera particulièrement évaluée.**

- Fichier du modèle entrainé au format ONNX (les principales bibliothèques d’apprentissage automatique permettent de convertir les modèles dans ce format. C’est par exemple le cas de Scikit-Learn, Pytorch, Tensorflow et Keras). Le modèle doit soit renvoyer des prédictions sous forme de nom de classe (chaîne de caractères) soit sous forme d’entiers correspondant au numéro de la classe dans cet ordre (de 0 à 9) : Tente, Sac à dos, Piano, Pingouin, Dragon, Vélo, Bonhomme de neige, Flocon de neige, Ordinateur portable, Montagne

Ces trois éléments devront impérativement être déposés sur chamilo sous la forme d'une archive **.zip** ou **.tgz** avec comme nom `NOM1_NOM2.zip` ou  `NOM1_NOM2.tgz`, où NOM1 et NOM2 sont les 2 noms des membres du binôme.

### Pour information :

Voici les résultats obtenus en entrainant un modèle sur ce jeu d'entrainement : 

```
Justesse (accuracy) : 0.93
```
