<style>

   .iframe-wrapper {
  max-width: 60%;
  margin: auto; /* pour centrer le conteneur */
}

    /* Pour la gestion des iframes */
 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 📚 Cours : les réseaux de neurones génératifs

Les **modèles génératifs** sont conçus pour **apprendre les structures et les distributions des données** afin de **générer de nouvelles données** qui n'ont **jamais été vues auparavant** mais qui ressemblent aux données d'origine. Bien que **tout type de données** puisse être généré, nous nous concentrerons principalement sur la **génération d'images** dans cette partie.

## Auto-encodeurs

### Architecture

<a href="./_static/ae_schema.png" target="_blank">
    <img src="./_static/ae_schema.png" height="300"  alt="Auto-encodeur"/>
</a>

Les auto-encodeurs sont constitués de **trois parties** :

- **Encodeur**

L'encodeur est la **première partie de l'auto-encodeur**. Sa fonction est de prendre les données d'entrée et de les **compresser** dans une **représentation plus petite** (espace latent). L'encodeur est généralement composé de **plusieurs couches** qui réduisent progressivement la dimension des données d'entrée.

- **Espace latent**

L'espace latent, situé entre l'encodeur et le décodeur, représente le **jeu de caractéristiques compressées** qui **résume l'information de l'entrée**.

- **Décodeur**

Le décodeur est la **dernière partie de l'auto-encodeur**. Son rôle est de **reconstruire les données d'entrée** à partir de la **représentation latente**. Comme l'encodeur, le décodeur est souvent **composé de plusieurs couches** qui augmentent progressivement la dimension des données jusqu'à atteindre la **taille originale de l'entrée**.

L'**architecture des auto-encodeurs** peut être utilisée avec **différents types de réseaux de neurones** : réseaux denses, convolutifs, récurrents ...

### Apprentissage

L'**apprentissage de l'auto-encodeur** se fait de manière **auto-supervisée** {cite}`gui2023survey`.

```{important}
L'**apprentissage auto-supervisé** (en anglais self-supervised learning) est différent de l'**apprentissage non supervisé**. L'apprentissage **non supervisé** n'utilise **pas d'étiquettes** tandis que l'apprentissage **auto-supervisé** crée une forme de **supervision à partir des données** elles-mêmes. L'<a href="https://fr.wikipedia.org/wiki/Apprentissage_auto-supervis%C3%A9" target="_blank">apprentissage auto-supervisé</a> {cite}`gui2023survey`  peut être vu comme un **intermédiaire** entre l'**apprentissage supervisé** et **non supervisé**.
```

Pour mettre à jour les poids de l'auto-encodeur, on utilise souvent une fonction de perte de type **erreur quadratique moyenne** (mean squared error - MSE) :

$\text{MSE} = \frac{1}{n} \displaystyle \sum_{i=1}^{n} (y_i - \hat{y}_i)^2$

Avec :

- $y_i$​ valeur réelle
- $\hat{y}_i​$ valeur reconstruite par le modèle
- $n$ nombre total d'observations (nombre total de pixels pour une image)

 Elle permet de mesurer l'**erreur de reconstruction des données** en sortie de l'auto-encodeur. On l'utilise donc aussi comme métrique de la **qualité de l'entrainement de l'auto-encodeur**.

Même si cela est **moins intuitif** pour des pixels, on peut aussi utiliser l'**entropie croisée binaire** (en anglais binary cross entropy) comme fonction de perte. Dans ce cas, il faut transformer les valeurs des pixels en probabilités que le pixel soit blanc en normant les valeurs entre 0 et 1.

$\text{BCE} = \displaystyle -\frac{1}{n} \sum_{i=1}^{n} \left[ y_i \log(\hat{y}_i) + (1 - y_i) \log(1 - \hat{y}_i) \right]$

Avec :

- $y_i$​ valeur réelle
- $\hat{y}_i​$ valeur reconstruite par le modèle
- $n$ nombre total d'observations (nombre total de pixels pour une image)

### Utilisation

#### Réduction de dimensions

L'auto-encodeur peut être utilisé pour effectuer une **réduction de dimensions**. Le nombre de dimensions que l'on souhaite conserver correspond à la dimension de l'espace latent. 

```{card} 📖 Algorithmes de réduction de dimensions
L'auto-encodeur est **un des algorithmes** pour effectuer une **réduction de dimensions** mais il en existe beaucoup d'autres (les principaux existent dans la bibliothèque Scikit-Learn : <a href="https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html" target="_blank">PCA</a>, <a href="https://scikit-learn.org/stable/modules/generated/sklearn.manifold.LocallyLinearEmbedding.html" target="_blank">LLE</a>, <a href="https://scikit-learn.org/stable/modules/generated/sklearn.manifold.TSNE.html" target="_blank">t-SNE</a>) {cite}`reduction`.
```

Dans une **version épurée du perceptron multicouche**, l'auto-encodeur peut évoquer une technique de réduction dimensionnelle similaire à l'**analyse en composantes principales** (PCA, pour Principal Component Analysis en anglais). Pour préserver la linéarité et éviter d'introduire des non-linéarités, aucune fonction d'activation n'est utilisée. L'erreur quadratique moyenne (MSE) est utilisée en tant que fonction de perte. Voici comment se présente cet **auto-encodeur minimaliste** appliqué au <a href="https://en.wikipedia.org/wiki/Iris_flower_data_set" target="_blank">célèbre dataset Iris</a> :

```python
from tensorflow import keras
from sklearn import datasets
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt

# Récupération des données
X, y = datasets.load_iris(return_X_y=True)
X = StandardScaler().fit_transform(X)

# Auto-encodeur
encoder = keras.models.Sequential([
    keras.layers.Input(shape=(4,)),
    keras.layers.Dense(2),
])
decoder = keras.models.Sequential([
    keras.layers.Input(shape=(2,)),
    keras.layers.Dense(4),
])
autoencoder = keras.models.Sequential([encoder, decoder])
autoencoder.compile(loss="mse", optimizer="adam")
autoencoder.fit(X, X, epochs=1000, verbose=0)
X_encoded = encoder.predict(X, verbose=0)

# Analyse en composantes principales
pca = PCA(n_components=2)
X_pca = pca.fit_transform(X)

# Visualisation des résultats
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 4))
ax1.scatter(X_pca[:, 0], X_pca[:, 1], c=y)
ax1.set_title("Projection de l'analyse en composantes principales")
ax1.set_xlabel("Première composante")  
ax1.set_ylabel("Deuxième composante")
ax2.scatter(X_encoded[:, 0], X_encoded[:, 1], c=y)
ax2.set_title("Projection dans l'espace latent de l'auto-encodeur")
ax2.set_xlabel("Première dimension de l'espace latent")  
ax2.set_ylabel("Deuxième dimension de l'espace latent")
fig.suptitle("Comparaison des réductions de dimensions", fontsize=20)
plt.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.show()
```
<a href="./_static/ae.png" target="_blank">
    <img src="./_static/ae.png" alt="ML map scikit-learn"/>
</a>

On observe que la position relative des données est assez proches dans les deux méthodes de réduction de dimensions. Les différences observées peuvent s'expliquer pour les manières différentes d'optimiser les algorithmes de PCA et d'auto-encodeurs. 

#### Compression

Les auto-encodeurs peuvent être utilisés pour **compresser l'information**. On peut étudier la compression des images MNIST dans des espaces latents de faibles dimensions (2, 5, 10, 20 50). On utilise un réseau de neurones dense très simple à trois couches :

Voici le résultat en sortie de l'auto-encodeur en fonction de la **taille de l'espace latent** :

<a href="./_static/ae_mnist.png" target="_blank">
    <img src="./_static/ae_mnist.png" alt="Autoencodeur"/>
</a>

On observe qu'avec uniquement **50 dimensions**, on arrive à **reconstruire les 10 chiffres** (en comparaison des 784 dimensions de l'image d'entrée)

En ajoutant une **couche cachée** (pour obtenir un auto-encodeur profond - deep autoencoder) de 100 neurones dans l'encodeur et le décodeur, on **améliore les résultats** :

<a href="./_static/ae_mnist_100.png" target="_blank">
    <img src="./_static/ae_mnist_100.png" alt="Autoencodeur"/>
</a>

Pour visualiser l'importance du **choix de la fonction de perte**, on peut analyser l'impact du passage de l'**erreur quadratique moyenne (MSE)** à l'**erreur absolue moyenne (MAE)** :

<a href="./_static/ae_mnist_mae.png" target="_blank">
    <img src="./_static/ae_mnist_mae.png" alt="Autoencodeur"/>
</a>

Comme les **fortes erreurs** sont **moins pénalisées** avec l'erreur absolue moyenne (MAE), les **images générées** sont **moins nettes**.

#### Débruitage

L'auto-encodeur peut aussi être utilisé pour **réduire le bruit présent dans des données**. L'exemple suivant est adapté du <a href="https://keras.io/examples/vision/autoencoder/" target="_blank">tutoriel de la bibliothèque Keras</a>, il utilise un **auto-encodeur convolutif** pour débruiter des données MNIST auxquelles on a ajouté un bruit gaussien : 

<a href="./_static/ae_noise.png" target="_blank">
    <img src="./_static/ae_noise.png" alt="ML map scikit-learn"/>
</a>

```{card} 📖 Papier de recherche
Comment **localiser des utilisateurs de Twitter** grâce à l'entrainement d'un **auto-encodeur** sur des **données bruitées** ? {cite}`liu-inkpen-2015-estimating`
```
___

<div class="iframe-wrapper">
    <div class="container">
        <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/s22cQfIyqAY" title="Fiddle autoencodeur" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
</div>

<a href="https://youtu.be/s22cQfIyqAY" target="_blank">FIDLE / Autoencodeur (AE), un exemple d'apprentissage auto-supervisé !</a>, CNRS - Formation FIDLE, 1h 44min 16s, 2024
___

## Auto-encodeurs variationnels

<a href="./_static/vae_schema.png" target="_blank">
    <img src="./_static/vae_schema.png" height="300"  alt="Auto-encodeur variationnel"/>
</a>

L'auto-encodeur variationnel (en anglais variational autoencoder - VAE) est une variante de l'auto-encodeur en ajoutant une **contrainte** sur la manière dont la **représentation des données dans l'espace latent** est apprise. L'objectif est de **limiter l'éparpillement des données** dans l'espace latent {cite}`kingma2022autoencoding`. Ceci est réalisé en utilisant **deux vecteurs** : un pour stocker la **moyenne $\mu$** et un pour stocker l'**écart-type $\sigma$** de la distribution des données dans l'espace latent.

### Apprentissage

La **fonction de perte** est composée de **deux termes** :

- **Terme de reconstruction**

Il mesure la **différence** entre les **données réelles** et les **données reconstruites** comme pour l'auto-encodeur classique. On utilise souvent l'**erreur quadratique moyenne (MSE)**. Comme pour l'**auto-encodeur classique**, on peut utiliser l'**entropie croisée binaire** (en anglais binary cross entropy) en normant les données entre 0 et 1.

- **Terme de régularisation** (<a href="https://fr.wikipedia.org/wiki/Divergence_de_Kullback-Leibler" target="_blank">divergence de Kullback-Leibler</a>)

Il mesure à quel point la **distribution apprise** dans l'**espace latent** diffère d'une **distribution réelle**. Voici la notation générale continue de la divergence de Kullback-Leibler de $p$ par rapport à $q$, elle mesure combien d'information est perdue lorsque $q$ est utilisée pour approximer $p$ :

$D_{KL}(p || q) = \int p(x) \log\left(\frac{p(x)}{q(x)}\right)dx$


Dans le cas de l'**auto-encodeur variationnel**, cette expression devient (pour le détail des calculs mathématiques {cite}`doersch2021tutorial`) :

$D_{KL}(q(z|x) \,||\, p(z)) = \displaystyle -\frac{1}{2} \sum_{k}(1 + \log(\sigma_k^2(x)) - \mu_k^2(x) - \sigma_k^2(x))$

Avec :

- $q(z|x)$ : **distribution de l'espace latent** apprise par l'encodeur où $z$ représente les variables latentes et $x$ les données d'entrée.

- $p(x)$ : **distribution a priori** (au <a href="https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_de_Bayes" target="_blank">sens bayésien</a>) choisie comme une gaussienne standard de **moyenne 0** et de **variance 1**.

- $\mu_k(x)$ : **moyenne** de la distribution gaussienne pour la k-ième dimension de l'espace latent pour les données d'entrée $x$. Elle indique où se situe le **centre de la distribution** pour cette dimension.

- $\sigma_k^2(x)$ : **variance** de la distribution gaussienne pour la k-ième dimension de l'espace latent pour les données d'entrée $x$. Elle indique à quel point les valeurs de cette dimension sont **dispersées autour de la moyenne**.

- $k$ : index des **dimensions de l'espace latent**


```{card} 📖 Papier de recherche
Comment entrainer un auto-encodeur variationnel basé sur un RNN pour générer des dessins ? {cite}`ha2017neural`
```
___

<div class="iframe-wrapper">
    <div class="container">
        <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/nVG0e0pAP2s" title="FIDLE / Variational Autoencoder (VAE), ou comment jouer avec les espaces latents" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
</div>

<a href="https://youtu.be/nVG0e0pAP2s" target="_blank">FIDLE / Variational Autoencoder (VAE), ou comment jouer avec les espaces latents</a>, CNRS - Formation FIDLE, 1h 51min 54s, 2024
___

## Réseaux de neurones guidés par la connaissance

Les Physics-Informed Neural Networks (PINNs) sont une classe de modèles de réseaux de neurones qui intègrent des connaissances physiques. Plutôt que de s'appuyer uniquement sur les données pour apprendre, les PINNs utilisent des contraintes basées sur des lois physiques pour améliorer la précision des modèles. En intégrant des équations différentielles partielles dans la fonction de perte, les PINNs contraignent les prédictions du modèle à respecter les lois physiques sous-jacentes.
___

<div class="iframe-wrapper">
    <div class="container">
        <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/JoFW2uSd3Uo" title="Physics Informed Machine Learning: High Level Overview of AI and ML in Science and Engineering" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
</div>

<a href="https://youtu.be/JoFW2uSd3Uo" target="_blank">Physics Informed Machine Learning: High Level Overview of AI and ML in Science and Engineering</a>, Steve Brunton, 47min 26s, 2024
___

## Autres modèles génératifs

### GAN

Les GANs (Generative Adversarial Networks) sont une architecture de réseaux de neurones génératifs introduite par Ian Goodfellow {cite}`goodfellow2014generative`. Ils se composent de deux réseaux distincts qui s'entraînent l'un contre l'autre de manière adversariale : un générateur et un discriminateur.

- **Générateur :** ce réseau prend en entrée un bruit aléatoire et produit des images qui ressemblent à des images réelles

- **Discriminateur :** ce réseau prend des images en entrée (soit réelles, soit générées par le générateur) et tente de distinguer si elles sont réelles ou générées

Les deux réseaux s'entraînent conjointement : le générateur apprend à tromper le discriminateur en produisant des échantillons de plus en plus réalistes tandis que le discriminateur essaie de devenir meilleur pour détecter les faux échantillons.

Les GANs classiques peuvent souffrir de problèmes de convergence et d'instabilité lors de l'entraînement. Le Wasserstein GAN (WGAN) a été introduit pour résoudre ces problèmes en utilisant une distance de Wasserstein comme fonction de perte, permettant une meilleure mesure de la différence entre la distribution réelle et celle générée {cite}`arjovsky2017wasserstein`.

Le WGAN-GP (Wasserstein GAN avec gradient penalty) est une amélioration supplémentaire qui stabilise l'entraînement en ajoutant une pénalité sur les gradients {cite}`gulrajani2017improved`.

#### Exemple de GAN

Le modèle StyleGAN est un exemple avancé de GAN utilisé pour la génération d'images ultra-réalistes comme celles visibles sur le site "This Person Does Not Exist". StyleGAN modifie l'architecture des GANs en introduisant une couche de style qui permet de contrôler divers aspects des images générées {cite}`karras2019stylebased`.

Vous pouvez tester cette technologie en ligne ici : https://thisxdoesnotexist.com/

___

<div class="iframe-wrapper">
    <div class="container">
        <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/eGk4zeE0mho" title="FIDLE / Generative Adversarial Networks (GAN)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
</div>

<a href="https://youtu.be/eGk4zeE0mho" target="_blank">FIDLE / Generative Adversarial Networks (GAN)</a>, CNRS - Formation FIDLE, 1h 46min 35s, 2024
___

### Modèles de diffusion

Les modèles de diffusion sont une classe récente et puissante de modèles génératifs qui ont montré d'excellents résultats notamment dans la génération d'images.

Le processus de diffusion fonctionne en deux étapes principales :

- **Diffusion progressive :** Les images sont progressivement bruitées en ajoutant du bruit gaussien à chaque étape jusqu'à ce qu'elles soient complètement bruitées

- **Débruitage** : à partir d'une donnée entièrement bruitée, le modèle apprend à inverser ce processus de diffusion en supprimant progressivement le bruit à chaque étape jusqu'à ce que l'image reconstruite soit obtenue.
___

<div class="iframe-wrapper">
    <div class="container">
        <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/gKlo5BVpcBs" title="FIDLE / Diffusion model, text to image" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
</div>

<a href="https://youtu.be/gKlo5BVpcBs" target="_blank">FIDLE / Diffusion model, text to image</a>, CNRS - Formation FIDLE, 1h 31min 42s, 2024

___

<div class="iframe-wrapper">
    <div class="container">
        <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/1CIpzeNxIhU" title="How AI Image Generators Work (Stable Diffusion / Dall-E) - Computerphile" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
</div>

<a href="https://youtu.be/1CIpzeNxIhU" target="_blank">How AI Image Generators Work (Stable Diffusion / Dall-E) - Computerphile</a>, Computerphile, 17min 49s, 2022