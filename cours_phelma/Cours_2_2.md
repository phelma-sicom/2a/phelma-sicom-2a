<style>

   .iframe-wrapper {
  max-width: 60%;
  margin: auto; /* pour centrer le conteneur */
}

    /* Pour la gestion des iframes */
 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 📚 Cours : Les modèles d'apprentissage supervisé

## Bases de l'apprentissage supervisé

A partir des données d'entrainement :

$\mathcal{D} = \{ (X_i, y_i) \mid X_i \in \mathbb{R}^{d}, y_i \in \mathcal{Y} \}_{i=1}^N
$

L'objectif de l'apprentissage supervisé est de trouver la fonction pour faire des prédictions :

$\hat{y} = f(X, \theta)$

Pour trouver cette fonction $f$, on cherche à minimiser les erreurs entre les valeurs prédites $\hat{y}$ et les valeurs réelles $y$ grâce à la fonction de coût $L$ :

$L(\theta) = \displaystyle \sum_{i=1}^{N} \text{loss}(y_i, \hat{y}_i)$



## Les modèles de régression

Un modèle de **régression** est un type de modèle prédictif utilisé pour prédire une **valeur continue** ou **quantitative** (variable cible) en fonction d'une ou de plusieurs **variables indépendantes**.

```{note}
Dans certaines situations, il est nécessaire de **modéliser simultanément plusieurs variables cibles**, un scénario connu sous le nom de <a href="https://scikit-learn.org/stable/modules/multiclass.html#multioutput-regression" target="_blank">régression multi-sorties</a>. Cette approche est souvent mise en œuvre lorsque **chaque variable cible** nécessite sa **propre prédiction**, tout en tenant compte des éventuelles **interdépendances entre elles**.
```

### Les différents modèles

Le **choix d'un modèle** est très important car il détermine la capacité à **apprendre** de manière **efficace** et **précise** à partir des données disponibles. Chaque modèle présente des **compromis** entre **précision**, **complexité** et **rapidité d'exécution**. Il est donc essentiel d'évaluer les besoins spécifiques du projet, la nature des données, les objectifs de performance et les contraintes de ressources pour sélectionner le modèle le plus approprié.

#### Régression linéaire

Il s'agit d'un **modèle paramètrique** qui modélise la relation entre une <a href="https://fr.wikipedia.org/wiki/Variable_dépendante" target="_blank">variable dépendante</a> et une ou plusieurs <a href="https://fr.wikipedia.org/wiki/Variable_indépendante" target="_blank">variables indépendantes</a> en ajustant un hyperplan (une droite s'il n'y qu'une variable indépendante) à travers les données.
La régression linéaire **cherche à modéliser** la relation entre une variable dépendante $y$ et une ou plusieurs variables indépendantes $X$ par une **équation linéaire** de la forme :

$y = X\theta + \epsilon$

Avec :

- $y$ vecteur des valeurs observées de la variable dépendante
- $X$  matrice des variables indépendantes (avec une colonne supplémentaire de 1 pour l'ordonnée à l'origine)
- $\theta$ vecteur des coefficients à estimer
- $\epsilon$ vecteur des résidus

**Entraînement**

Pour entrainer le modèle, une des méthodes utilisée (méthode des moindres carrés ordinaires) est de minimiser la somme des carrés des résidus (la différence entre les valeurs observées et celles prédites par le modèle). Cela revient à trouver le vecteur $\theta$ suivant :

$\displaystyle \min_\theta \sum (y - X\theta)^2$

La **solution analytique** est donnée par la formule :

$\hat\theta = (X^T X)^{-1} X^T y$

Avec :
- $y$ vecteur des valeurs observées de la variable dépendante
- $X$  matrice des variables indépendantes (avec une colonne supplémentaire de 1 pour l'ordonnée à l'origine)
- $X^T$ transposée de la matrice $X$ 
- $\hat\theta$ vecteur des coefficients à estimer

```{note}
Il existe d'**autres méthodes** pour l'entrainement d'une régression linéaire. On peut citer par exemple la <a href="https://fr.wikipedia.org/wiki/Algorithme_du_gradient" target="_blank">descente de gradient</a> (implémentée dans la bibliothèque <a href="https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.SGDRegressor.html" target="_blank">scikit-learn</a>) ou l'<a href="https://fr.wikipedia.org/wiki/Statistique_bayésienne" target="_blank">approche bayésienne</a> (implémentée dans la bibliothèque <a href="https://www.pymc.io/projects/docs/en/v3/pymc-examples/examples/getting_started.html" target="_blank">PyMC3</a>)
```

**Descente de gradient**

En prenant l'exemple d'une régression linéaire simple avec une seule caractéristique, la forme générale de la régression linéaire est :

$y = \theta_0 + \theta_1 x$

La fonction de perte souvent utilisée est l'erreur quadratique moyenne (MSE) :

$J(\theta_0, \theta_1) = \displaystyle \frac{1}{m} \sum_{i=1}^{m} (h_\theta(x_i) - y_i)^2$

Avec :

$h_\theta(x) = \theta_0 + \theta_1 x$

On calcule les dérivées partielles par rapport à chacun des coefficients $\theta_0$ et $\theta_1$ :

$\frac{\partial J}{\partial \theta_0} = \displaystyle \frac{2}{m} \sum_{i=1}^{m} (h_\theta(x_i) - y_i)$

$\frac{\partial J}{\partial \theta_1} = \displaystyle \frac{2}{m} \sum_{i=1}^{m} (h_\theta(x_i) - y_i) \cdot x_i$

On met à jour les coefficients de manière itérative :

$\theta_0 = \theta_0 - \eta \cdot \frac{\partial J}{\partial \theta_0}$

$\theta_1 = \theta_1 - \eta \cdot \frac{\partial J}{\partial \theta_1}$

**Avantages** 

- Simple à comprendre et à entrainer
- Interprétable (coefficients indiquent l'impact de chaque variable indépendante)
- Rapide à entraîner

**Inconvénients**

- Supposition d'une relation linéaire, ce qui peut être restrictif
- Sensible aux <a href="https://fr.wikipedia.org/wiki/Donn%C3%A9e_aberrante" target="_blank">données aberrantes</a> (solution : utiliser une fonction de perte basée sur la médiane avec la <a href="https://scikit-learn.org/stable/modules/linear_model.html#quantile-regression" target="_blank">régression basée sur les quantiles</a>)
- Ne capture pas les relations non linéaires sans transformation des données (solution : utiliser une <a href="https://scikit-learn.org/stable/modules/linear_model.html#polynomial-regression-extending-linear-models-with-basis-functions" target="_blank">régression polynomiale</a>)
- Risque de surapprentissage en grandes dimensions (solution : régularisation Lasso, Ridge ou elastic net)

**Illustration**

La bibliothèque <a href="https://scikit-learn.org/stable/" target="_blank">scikit-learn</a> permet de retrouver facilement les **coefficients** et l'**ordonnée à l'origine** d'une régression linéaire :

```python
import numpy as np
from sklearn.linear_model import LinearRegression

# Exemple de données
X = np.array([[1, 1], [1, 2], [2, 2], [2, 3]])
y = np.dot(X, np.array([1, 2])) + 3

# Création du modèle de régression linéaire
model = LinearRegression()

# Entraînement du modèle
model.fit(X, y)

# Affichage des coefficients
print("Coefficients :", model.coef_)
print("Ordonnée à l'origine :", round(model.intercept_, 2))
```
```
Coefficients : [1. 2.]
Ordonnée à l'origine : 3.0
```

#### Régression avec arbre de décision

La régression avec arbre de décision est un **modèle non paramétrique**. Contrairement à la régression linéaire qui ajuste un hyperplan, la régression par arbre de décision divise l'espace des caractéristiques en **régions distinctes**.

Pour chaque observation, l'arbre guide la prédiction en suivant un ensemble de décisions basées sur les valeurs des variables indépendantes jusqu'à atteindre une **feuille de l'arbre**. La valeur prédite est alors la **moyenne des valeurs de la variable dépendante** dans cette région.

**Entraînement**

L'entraînement d'un arbre de décision consiste à **diviser récursivement** l'espace des caractéristiques en régions **plus petites** et **plus homogènes**. Les critères de division peuvent varier mais ils visent généralement à maximiser la réduction de l'erreur (comme la réduction de la somme des carrés des résidus).

**Avantages**

- Capable de modéliser des **relations non linéaires** sans nécessiter de transformation des variables
- **Facile à interpréter** et à **visualiser**
- Peut gérer des données **catégorielles** et **continues**

**Inconvénients**

- Sensible au **surapprentissage** surtout avec des arbres profonds (solution : élagage ou limitation de la profondeur de l'arbre)

```{admonition} Influence de la profondeur de l'arbre sur le surapprentissage
L'exemple suivant présente l'apprentissage d'un arbre de décision de régression sur des données d'une **sinusoïde bruitée** avec une **contrainte de profondeur** de l'arbre allant de 1 à 15 :

<a href="./_static/dt_surapprentissage.gif" target="_blank"><img src="./_static/dt_surapprentissage.gif" alt="Arbre de décision surapprentissage" /></a>

```

- Peut être **instable** : de petites variations dans les données peuvent entraîner des changements significatifs dans la structure de l'arbre
- **Moins efficace** sur les **données linéaires** par rapport à la régression linéaire

**Illustration**

La bibliothèque <a href="https://scikit-learn.org/stable/" target="_blank">scikit-learn</a> offre une mise en œuvre pratique de la régression par arbre de décision :

```python
import numpy as np
from sklearn.tree import DecisionTreeRegressor

# Exemple de données
X = np.array([[1, 2], [3, 4], [5, 6], [7, 8]])
y = np.array([1, 2, 3, 4])

# Création du modèle de régression par arbre de décision
model = DecisionTreeRegressor(random_state=0)

# Entraînement du modèle
model.fit(X, y)

# Prédiction sur de nouvelles données
X_new = np.array([[2, 3]])
y_pred = model.predict(X_new)
print("Prédiction :", y_pred)
```
```
Prédiction : [1.]
```
On peut afficher l'arbre de décision en ajoutant ce code :

```python
from sklearn.tree import plot_tree
import matplotlib.pyplot as plt

# Affichage de l'arbre de décision
plt.figure(figsize=(12,8))
plot_tree(model, filled=True)
plt.show()
```
<img src="./_static/DTRegressor.png" alt="Arbre de décision" height="400px"/>

On peut aussi afficher les **limites de décision** :

<img src="./_static/limites_decision.png" alt="Limites de décision" height="400px"/>

#### Régression avec k plus proches voisins

La régression avec K plus proches voisins (KNN) est un modèle non paramétrique se basant sur les caractéristiques des **K observations les plus similaires** dans les données d'entraînement. Contrairement à la régression linéaire, ce modèle ne cherche pas à établir une équation linéaire mais utilise directement les valeurs des voisins les plus proches pour faire des prédictions.

**Entraînement**

Il n'y a **pas d'entrainement** à proprement parler. L'entraînement consiste à **stocker ces données**. Lors de la prédiction, le calcul a pour but de trouver les K plus proches voisins de la nouvelle observation dans cet ensemble et de calculer la **moyenne des valeurs**.

**Avantages**

- **Simplicité** de compréhension
- Flexible à la complexité des données : peut capturer des **relations non linéaires** sans transformation explicite
- **Pas de supposition** sur la forme statistique des données

**Inconvénients**

- **Temps de calcul élevé** pour les prédictions, surtout avec de grands ensembles de données
- **Sensible à l'échelle** des variables (solution : normalisation des données)
- Nécessite un **choix judicieux de K** (trop petit il y a un risque de surapprentissage, trop grand il y a un risque de sousapprentissage)
- **Performances médiocres en haute dimension** à cause de la "malédiction de la dimensionnalité" (solution : réduction de dimensions)

**Illustration**

La bibliothèque <a href="https://scikit-learn.org/stable/" target="_blank">scikit-learn</a> offre une mise en œuvre pratique de la régression par k plus proches voisins :

```python
import numpy as np
from sklearn.neighbors import KNeighborsRegressor

# Exemple de données
X = np.array([[1, 2], [3, 4], [5, 6], [7, 8]])
y = np.array([1, 2, 3, 4])

# Création du modèle KNN
model = KNeighborsRegressor(n_neighbors=2)

# Entraînement du modèle
model.fit(X, y)

# Prédiction pour un nouvel échantillon
new_data = np.array([[2, 3]])
prediction = model.predict(new_data)

print("Prédiction :", prediction)
```

```
Prédiction : [1.5]
```

<img src="./_static/KNNRegressor.png" alt="KNN régresseur" height="400px"/>

#### Régression avec séparateur à vaste marge



### Les métriques de qualité des prédictions

#### Mesures agrégées

##### Coefficient de détermination $R^2$ et $R^2$ ajusté

Le **coefficient de détermination** $R^2$ est une métrique qui indique la **proportion de la variance** pour une variable dépendante qui est **expliquée** par un ou plusieurs **variables indépendantes** dans un modèle de régression.

$R^2 = 1 - \frac{\text{SS}_{\text{res}}}{\text{SS}_{\text{tot}}} = 1 - \frac{\displaystyle \sum_{i=1}^{n} (y_i - \hat{y}_i)^2}{\displaystyle \sum_{i=1}^{n} (y_i - \bar{y})^2}$

Avec :

- $y_i$​ valeur observée
- $\hat{y}_i​$ valeur prédite par le modèle
- $\bar{y}$​ moyenne des valeurs observées
- $n$ nombre total d'observations

**Particularités** 

- Facile à comprendre, mesure la proportion de la variance expliquée par le modèle.
- Peut **être trompeur** avec des **données non linéaires** ou en présence de **nombreuses variables**.

Le **coefficient de détermination $R^2$ ajusté** permet justement de pouvoir comparer des modèles qui possèdent un nombre de variables indépendantes différent. Par contre, il ne corrige pas le problème des données non linéaires.

##### Métriques basées sur le carré des erreurs

###### L'erreur quadratique moyenne (MSE)

L'erreur quadratique moyenne (Mean Squares Error - MSE) est calculée en prenant la moyenne des carrés des différences entre les valeurs prédites et les valeurs réelles. Elle donne plus de poids aux erreurs plus importantes car l'erreur est élevée au carré.

$\text{MSE} = \frac{1}{n} \displaystyle \sum_{i=1}^{n} (y_i - \hat{y}_i)^2$

Avec :

- $y_i$​ valeur observée
- $\hat{y}_i​$ valeur prédite par le modèle
- $n$ nombre total d'observations

**Particularités** 

- La formule de l'erreur quadratique moyenne est simple et intuitive.
- Les erreurs importantes sont fortement pénalisées. La MSE est exprimée en unités au carré de la variable cible ce qui peut être difficile à interpréter.

###### Racine carrée de l'erreur quadratique moyenne (RMSE)

La RMSE correspond à la racine carrée de la MSE. Elle est souvent utilisé car elle est dans les mêmes unités que la variable cible.

$\text{RMSE} = \sqrt{\frac{1}{n} \displaystyle \sum_{i=1}^{n} (y_i - \hat{y}_i)^2}$

Avec :

- $y_i$​ valeur observée
- $\hat{y}_i​$ valeur prédite par le modèle
- $n$ nombre total d'observations

**Particularités**

- La RMSE est dans les mêmes unités que la variable cible ce qui facilite l'interprétation des résultats.
- Les erreurs importantes sont fortement pénalisées.

###### L'erreur quadratique moyenne des logarithmes des valeurs (MSLE)

Les MSLE et RMSLE sont des variations de la MSE et RMSE. Elles sont calculées en prenant le logarithme des valeurs prédites et réelles avant de calculer l'erreur quadratique moyenne.

$\text{MSLE} = \frac{1}{n} \displaystyle \sum_{i=1}^{n} (\log(1 + y_i) - \log(1 + \hat{y}_i))^2$

$\text{RMSLE} = \sqrt{\frac{1}{n} \displaystyle \sum_{i=1}^{n} (\log(1 + y_i) - \log(1 + \hat{y}_i))^2}$

Avec :

- $y_i$​ valeur observée
- $\hat{y}_i​$ valeur prédite par le modèle
- $n$ nombre total d'observations

**Particularités**

- Elles favorisent les erreurs relatives plutôt que les absolues. Elles pénalisent plus fortement les sous-estimation que les surestimation.
- Les MSLE et RMSLE ne sont pas toujours applicables, en particulier lorsque les valeurs cibles ou les prédictions peuvent être négatives. Elles peuvent être moins intuitives à comprendre et à interpréter.

##### Métriques basées sur la valeur absolue des erreurs

##### L'erreur absolue moyenne et médiane (MAE)

La MAE est la moyenne des valeurs absolues des erreurs entre les prédictions et les valeurs réelles. On peut aussi utiliser la médiane pour être moins impacté par les données aberrantes. La MAE donne une idée de la grandeur des erreurs de prédiction, **sans tenir compte de leur direction** (positive ou négative).

$\text{MAE} = \frac{1}{n} \displaystyle \sum_{i=1}^{n} |y_i - \hat{y}_i|$

$\text{Median Absolute Error} = \text{median}(|y_1 - \hat{y}_1|, |y_2 - \hat{y}_2|, \ldots, |y_n - \hat{y}_n|)$

Avec :

- $y_i$​ valeur observée
- $\hat{y}_i​$ valeur prédite par le modèle
- $n$ nombre total d'observations

**Particularités**

- Facile à comprendre et à interpréter car la MAE est exprimée dans la même unité que celle des données. Elle est très peu sensible aux données aberrantes (la médiane encore plus que la moyenne)
- La MAE traite de la même manière les erreurs de sous-estimation et de surestimation.

##### L'erreur absolue moyenne en pourcentage (MAPE)

La MAPE mesure l'erreur absolue en pourcentage : cette métrique est utile pour des comparaisons où les valeurs réelles varient beaucoup mais elle peut être trompeuse si $y_i$​ est très proche de zéro car cela peut conduire à des valeurs de pourcentage très élevées.

$\text{MAPE} = \frac{1}{n} \displaystyle \sum_{i=1}^{n} \left| \frac{y_i - \hat{y}_i}{\max(\epsilon, |y_i|)} \right|$

Avec :

- $y_i$​ valeur observée
- $\hat{y}_i​$ valeur prédite par le modèle
- $n$ nombre total d'observations
- $\epsilon$ petite constante pour éviter la division par zéro

**Particularités**

- Facile à interpréter car le résultat est exprimé en pourcentage. Elle permet de comparer les performances de différents ensembles de données avec différentes échelles.
- Problèmes avec les valeurs proches de zéro

#### Mesures individuelles

##### Limites des données agrégées

Le <a href="https://fr.wikipedia.org/wiki/Quartet_d%27Anscombe" target="_blank">Quartet d'Anscombe</a> illustre parfaitement les limitations inhérentes aux **métriques agrégées**. Considérons l'application d'une régression linéaire sur les quatre ensembles de données du quartet :

<a href="./_static/anscombe.png" target="_blank"><img src="./_static/anscombe.png" alt="Quartet Anscombe" /></a>

Une **analyse superficielle basée** uniquement sur les **coefficients de la droite de régression linéaire**, le **coefficient de détermination ($R^2$)** et l'**erreur quadratique moyenne (RMSE)** révèle une **similitude trompeuse** sur la qualité des prédictions des quatre modèles de régression linéaire. Cette approche **néglige les subtilités** de chaque ensemble de données. Il est donc essentiel d'adopter une **approche analytique plus  diversifiées** et en examinant par exemple les **résidus** (écarts entre les valeurs prédites et les valeurs réelles).

##### Analyse des résidus

L'**analyse des résidus** peut permettre de **détecter des problèmes** dans le **choix du modèles** et des <a href="https://fr.wikipedia.org/wiki/Hyperparamètre" target="_blank">hyperparamètres</a>. Voici les résidus pour le <a href="https://fr.wikipedia.org/wiki/Quartet_d%27Anscombe" target="_blank">Quartet d'Anscombe</a> :

<a href="./_static/anscombe_res.png" target="_blank"><img src="./_static/anscombe_res.png" alt="Quartet Anscombe résidus" /></a>

## Les modèles de classification

Un modèle de **classification** est un type de modèle prédictif utilisé pour identifier la **catégorie** ou **classe** à laquelle appartient une observation. Contrairement à la régression, qui prédit une valeur continue, la classification vise à attribuer chaque observation à une **catégorie discrète** en se basant sur une ou plusieurs variables indépendantes. 


```{note}
La classification peut parfois nécessiter de distinguer plus de 2 classes distinctes : on parle de **classification multiclasse** par opposition à la **classification binaire**. D'autre part, la **classification multilabel** consiste à attribuer à chaque observation plusieurs étiquettes simultanément. Cette approche est utilisée quand les observations peuvent appartenir à **plusieurs catégories en même temps**. Des outils tels que la bibliothèque <a href="https://scikit-learn.org/stable/modules/multiclass.html" target="_blank">Scikit-Learn</a> offrent des fonctionnalités pour gérer à la fois la classification multiclasse et multilabel.
```

**Formulation mathématique générale pour un problème de classification**

Soit le jeu de **données d'entrainement** suivant :

$\{(x_i, y_i)\}_{i=1}^n, \quad x_i \in \mathbb{R}^d, \quad y_i \in \{0, 1, \ldots, K-1\}$

Avec :

- $x_i$​ donnée d'entrée ou vecteur de caractéristiques
- $y_i$ étiquette ou valeur cible
- $d$ nombre de caractéristiques
- $K$ nombre de classes
- $n$ nombre d'observations dans le jeu d'entrainement

___

On cherche à trouver une **fonction $f$** qui prend une entrée $x$ et prédit un label $\hat{y}$ :

$\hat{y} = f(x, \theta)$

Avec :

- $\theta$ paramètres du modèle

___

L'objectif de l'entrainement du modèle est de trouver les paramètres $\theta$ qui **minimisent la fonction de perte** $L(\theta)$ sur l'ensemble des données d'entraînement :

$\theta^* = \arg\min_\theta L(\theta)$

La fonction de perte la plus souvent utilisée est l'**entropie croisée** (en anglais cross-entropy) :

$L(\theta) = -\frac{1}{n} \displaystyle \sum_{i=1}^n \sum_{k=0}^{K-1} \mathbf{1}(y_i = k) \log p_{\text{model}}(y_i = k | x_i; \theta)$



### Les différents modèles

#### Modèle de régression logistique

La régression logistique modélise la probabilité qu'une observation appartienne à une catégorie spécifique :

$P(Y=1) = \frac{1}{1 + e^{-(X\beta)}}$

**Entraînement**

**Avantages**

**Inconvénients**

**Illustration**

#### Arbre de décision pour la classification

**Entraînement**

L'entraînement d'un arbre de décision implique de **diviser récursivement** l'espace des caractéristiques en régions **plus petites** et **plus homogènes** en termes de classe. Les **critères de division** sont généralement basés sur des mesures telles que l'impureté de Gini ou l'entropie visant à maximiser la pureté des classes dans chaque région.

**Avantages**

- Demande peu ou pas de préparation des données (pas de normalisation ou de remplacement des données manquantes nécessaire)
- Facilement explicable

**Inconvénients**

- Risque de surapprentissage
- Risque d'instabilité : une faible variation dans les données d'entrainement peut modifier complétement la structure de l'arbre

**Illustration**

#### K plus proches voisins pour la classification

**Entraînement**

**Avantages**

**Inconvénients**

**Illustration**

#### Séparateur à vaste marge (SVM) pour la classification

Le but principal d'un SVM est de trouver un hyperplan dans un espace de N dimensions (N étant le nombre de caractéristiques) qui sépare clairement les différentes classes de données.

**Entraînement**

Le modèle cherche à **maximiser la marge** entre les données des deux classes. Les vecteurs de support sont les points de données les plus proches de l'hyperplan et sont les éléments critiques de la construction du modèle. L'hyperplan ne dépend que des vecteurs de support comme l'illustre l'animation suivante où les données d'entrainement sont ajoutées au fur et à mesure :

<a href="./_static/SVM.gif" target="_blank"><img src="./_static/SVM.gif" alt="SVM" /></a>

```{admonition} ⚠️ Normalisation des caractéristiques
Le modèle de SVM est sensible aux différences d'ordres de grandeur entre les caractéristiques. Il est donc important de normaliser les caractéristiques avant l'entrainement du modèle. Avec la bibliothèque <a href="https://scikit-learn.org/stable/">Scikit-learn</a>, cela peut se faire avec la classe <a href="https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.StandardScaler.html">StandardScaler()</a> de préférence dans un <a href="https://scikit-learn.org/stable/modules/generated/sklearn.pipeline.Pipeline.html">Pipeline</a>.
```

- Cas 1 : les données sont linéairement séparables (cas de la marge dure - hard margin)

Si les données sont **linéairement séparables**, cela signifie qu'il existe un **hyperplan** qui sépare les données **sans faire d'erreurs**. Parmi tous les hyperplans possibles, le SVM cherche celui qui **maximise la "marge"**, c'est-à-dire la distance entre l'hyperplan et les points de données les plus proches de chaque côté de l'hyperplan.

**Définition de l'hyperplan**

L'objectif d'un SVM est de trouver un hyperplan qui sépare deux classes de données. Dans un espace euclidien, un hyperplan est défini par une équation linéaire de la forme :

$w \cdot x + b = 0$

Avec :

- $w$  vecteur normal à l'hyperplan

- $x$ vecteur de caractéristiques

- $b$ biais

**Définition de la marge**

La marge est la distance minimale entre l'hyperplan et les points les plus proches de chaque classe. Le but du SVM est de maximiser cette marge. La distance d'un point $x_i$​ à l'hyperplan est proportionnelle à $\frac{1}{∣∣w∣∣}$. Pour maximiser la marge, il faut minimiser $∣∣w∣∣$.

**Formulation du problème primal**

On cherche donc à résoudre ceci :

$\displaystyle \min_{w, b} ∣∣w∣∣$

Comme $∣∣w∣∣$ n'est pas dérivable en 0, on modifie la formulation du problème :

$\displaystyle \min_{w, b} \frac{1}{2} ||w||^2$

Avec la contrainte suivante liée à la séparation des classes :

$y_i (w \cdot x_i + b) \geq 1$

**Fonction duale et multiplicateurs de Lagrange**

$\displaystyle \min_{\alpha}  \frac{1}{2} \sum_{i=1}^{N} \sum_{j=1}^{N} \alpha_i \alpha_j y_i y_j (x_i \cdot x_j) - \sum_{i=1}^{N} \alpha_i$

Avec les contraintes :

$\alpha_i \geq 0$ pour $i=1$ à $N$

$\displaystyle \sum_{i=1}^{N} \alpha_i y_i = 0$

**Construction de l'hyperplan**

En utilisant une méthode d'optimisation quadratique, on trouve la valeur de $\alpha$ qui minimise la fonction duale. On peut calculer la valeur de $w$ et de $b$ : 

$\displaystyle w = \sum_{i=1}^{N} \alpha_i y_i x_i$

$b = y_k - w \cdot x_k$

**Classification de nouveaux points**

Pour classifier un **nouveau point**, on utilise la formule suivante :

$w \cdot x + b$

Si $w \cdot x + b \geq 0$, le point appartient à une **classe positive**.

Si $w \cdot x + b \lt 0$, le point appartient à une **classe négative**.

**Conclusion**

Avec la marge dure, le SVM ne peut pas traiter des données non linéairement séparables et il est fortement impacté par les données aberrantes. Pour résoudre ce problème, on peut utiliser une contrainte moins forte sur le modèle : un marge souple (soft margin) qui autorise à mal classifier certains points.

- Cas 2 : les données ne sont pas linéairement séparables

    - Cas 2-1 : la marge souple (soft margin)

    - Cas 2-2 : l'ajout de caractéristiques polynomiales

    - Cas 2-3 : l'astuce du noyau

Pour les données non linéairement séparables, le SVM utilise l'astuce du noyau (kernel trick) qui permet de transformer les données dans un espace de dimension supérieure où elles deviennent linéairement séparables sans calculer explicitement les coordonnées dans cet espace.

**Avantages**

**Inconvénients**

**Illustration**

```{admonition} Classification multiclasse
Les machines à vecteurs de support (SVM) sont naturellement conçues pour la **classification binaire** mais elles peuvent être adaptées pour gérer des problèmes de **classification multiclasse** en utilisant une approche **"un contre tous"** ("One-vs-All" - OvA) ou **"un contre un"** ( "One-vs-One" - OvO). Dans la bibliothèque scikit-learn, ce choix s'effectue avec le paramètre `decision_function_shape` de la fonction <a href="https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html" target="_blank">SVC</a>.
```

___

<div class="iframe-wrapper">
    <div class="container">
        <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/_PwhiWxHK8o" title="16. Learning: Support Vector Machines" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
</div>

<a href="https://youtu.be/_PwhiWxHK8o" target="_blank">16. Learning: Support Vector Machines</a>, MIT OpenCourseWare, 49min 33s, 2014
___

### Les métriques de qualité des prédictions

#### Mesures individuelles

La <a href="https://fr.wikipedia.org/wiki/Matrice_de_confusion" target="_blank">matrice de confusion</a> fournit une **représentation visuelle** des **prédictions correctes et incorrectes** d'un modèle par rapport aux valeurs réelles. Voici un exemple de **matrice de confusion** pour de la **classification binaire** avec un **arbre de décision** :

<a href="./_static/confusion.png" target="_blank"><img src="./_static/confusion.png" alt="Matrice de confusion" /></a>

Voici un exemple de matrice de confusion pour de la **classification multiclasse** (3 classes) avec un **arbre de décision** :

<a href="./_static/confusion_multiclasse.png" target="_blank"><img src="./_static/confusion_multiclasse.png" alt="Matrice de confusion multiclasse" /></a>

```{admonition} Classification multilabel
La biliothèque scikit-learn permet aussi de calculer les matrices de confusion des différentes classes dans le cadre de la classification multilabels grâce à la fonction <a href="https://scikit-learn.org/stable/modules/generated/sklearn.metrics.multilabel_confusion_matrix.html" target="_blank">multilabel_confusion_matrix</a>. 
```

```{warning}

**Orientation de la matrice de confusion**

Il faut **vérifier pour chaque matrice de confusion** si les données réelles sont positionées **verticalement ou horizontalement**.
```

#### Mesures agrégées

##### Exactitude

L'exactitude (accuracy en anglais) est une **métrique** qui mesure la **proportion des prédictions correctes** par rapport au **total des prédictions effectuées**. Elle est calculée en divisant le nombre de prédictions correctes (vrais positifs et vrais négatifs) par le nombre total de cas examinés. Pour de la **classification binaire** :

$\text{Accuracy} = \frac{\text{Nombre de prédictions correctes}}{\text{Nombre total de prédictions}} = \frac{TP + TN}{TP + TN + FP + FN}$

Avec :

- $TP$ nombre de vrais positifs (True Positives)
- $TN$ nombre de vrais négatifs (True Negatives)
- $FP$ nombre de faux positifs (False Positives)
- $FN$ nombre de faux négatifs (False Negatives)

<a href="./_static/accuracy_binaire.gif" target="_blank"><img src="./_static/accuracy_binaire.gif" alt="Accuracy binaire" /></a>

D'une manière plus générale, pour de la **classification multiclasse** :

$\text{Accuracy} = \frac{1}{n_{\text{samples}}} \displaystyle \sum_{i=1}^{n_{\text{samples}}} 1(\hat{y}_i = y_i)$

Avec :

- $n_\text{samples}$​  nombre total d'observations dans le jeu de données
- $\hat{y}_i$​ prédiction du modèle pour l'échantillon $i$
- $y_i$​ vraie valeur pour l'échantillon $i$
- $1(\hat{y}_i = y_i)$ fonction indicatrice qui vaut 1 si la prédiction est correcte (c'est-à-dire $\hat{y}_i = y_i$​) et 0 dans le cas contraire

<a href="./_static/accuracy_multi.gif" target="_blank"><img src="./_static/accuracy_multi.gif" alt="Accuracy multiclasse" /></a>

**Particularités**

- La formule de l'exactitude est **facile à comprendre**
- L'exactitude peut être trompeuse dans le cas d'un désiquilibre de classes

Il existe **plusieurs variantes** de l'exactitude :

- L'exactitude équilibrée (balanced accuracy)

Cette métrique est utilisée en classification où les classes sont **déséquilibrées**. Elle est définie comme la moyenne du rappel pour chaque classe. Pour de la **classification binaire** :

$\text{Balanced Accuracy} = \frac{1}{2} \left( \frac{TP}{TP + FN} + \frac{TN}{TN + FP} \right)$

Avec :

- $TP$ nombre de vrais positifs (True Positives)
- $TN$ nombre de vrais négatifs (True Negatives)
- $FP$ nombre de faux positifs (False Positives)
- $FN$ nombre de faux négatifs (False Negatives)

D'une manière plus générale, pour de la **classification multiclasse** :

$\text{Balanced Accuracy} = \frac{1}{C} \displaystyle \sum_{i=1}^{C} \frac{TP_i}{TP_i + FN_i}$

Avec :

- $C$ nombre de classes
- $TP_i$​ vrais positifs pour chaque classe $i$
- $FN_i$​ faux négatifs pour chaque classe $i$

##### Précision

La précision mesure la proportion des **prédictions positives** qui sont **effectivement correctes**. La précision est particulièrement utile lorsque le **coût des faux positifs** est **élevé**. Une précision élevée signifie que lorsque le modèle prédit la classe positive, il est très probable que cette prédiction soit correcte. Pour la **classification binaire** :

$\text{Précision} = \frac{\text{TP}}{\text{TP} + \text{FP}}$

Avec :

- $TP$ nombre de vrais positifs (True Positives)
- $FP$ nombre de faux positifs (False Positives)

<a href="./_static/precision.gif" target="_blank"><img src="./_static/precision.gif" alt="Précision" /></a>

En **classification multiclasse**, il existe différentes façons de calculer la précision :

- **Par "micro" moyenne (micro average)**

Pour calculer la "micro" moyenne, on **cumule** les **vrais positifs** et les **faux positifs** sur **toutes les classes** puis on calcule la **précision globale**.

${Précision_{micro}} = \frac{\displaystyle \sum_{i=1}^{C} {TP_i}} {\displaystyle \sum_{i=1}^{C} {TP_i} + {FP_i}}$

Avec :

- $C$ nombre de classes
- $TP_i$​ vrais positifs pour chaque classe $i$
- $FP_i$​ faux positifs pour chaque classe $i$

<a href="./_static/precision_micro.gif" target="_blank"><img src="./_static/precision_micro.gif" alt="Précision micro average" /></a>

- **Par "macro" moyenne (macro average)**

Pour calculer la "macro" moyenne, on calcule la **moyenne arithmétique** des précisions des différentes classes.

${Précision_{macro}} = \frac{1}{C} \displaystyle \sum_{i=1}^{C} \frac{TP_i}{TP_i + FP_i}$

Avec :

- $C$ nombre de classes
- $TP_i$​ vrais positifs pour chaque classe $i$
- $FP_i$​ faux positifs pour chaque classe $i$

<a href="./_static/precision_macro.gif" target="_blank"><img src="./_static/precision_macro.gif" alt="Précision macro average" /></a>

- **Par moyenne pondérée (weighted average)**

Pour calculer la moyenne pondérée, on calcule la **moyenne arithmétique** des précisions des différentes classes **pondérée par le support** de chaque classe.

${Précision_{pondérée}} = \frac{1}{n} \displaystyle \sum_{i=1}^{C} \frac{n_i \cdot TP_i}{TP_i + FP_i} = \displaystyle \sum_{i=1}^{C} \frac{\omega_i \cdot TP_i}{TP_i + FP_i}$

Avec :

- $n$ nombre total de prédictions
- $C$ nombre de classes
- $n_i$ nombre de données réelles de la classe $i$
- $\omega_i = \frac{n_i}{n}$ poids de la classe $i$
- $TP_i$​ vrais positifs pour chaque classe $i$
- $FP_i$​ faux positifs pour chaque classe $i$

<a href="./_static/precision_weighted.gif" target="_blank"><img src="./_static/precision_weighted.gif" alt="Précision pondérée" /></a>

```{admonition} Classification multilabel
La biliothèque scikit-learn permet aussi de calculer la précision dans le cadre de la **classification multilabel**. En particulier, elle permet de calculer la **précision moyenne** calculée pour **chaque observation multilabel** grâce à la valeur `samples` du paramètre `average` de la fonction <a href="https://scikit-learn.org/stable/modules/generated/sklearn.metrics.precision_score.html" target="_blank">precision_score</a>.
```

##### Rappel

Pour la classification binaire, le rappel (recall en anglais) aussi appelé sensibilité ou taux de vrais positifs mesure la proportion des vrais positifs par rapport au nombre total de cas qui sont réellement positifs. Autrement dit, il indique combien de cas positifs réels ont été correctement identifiés par le modèle.

Le rappel est particulièrement important dans les situations où il est crucial de détecter autant de cas positifs que possible même au risque de faire des erreurs en classant à tort certains cas négatifs comme positifs. Par exemple, dans le domaine médical pour le diagnostic de maladies graves, il est souvent plus important de s'assurer que tous les cas positifs sont détectés (même si cela signifie inclure quelques faux positifs) plutôt que de rater des cas positifs.

$\text{Rappel} = \frac{TP}{TP + FN}$

Avec :

- $TP$ nombre de vrais positifs (True Positives)
- $FN$ nombre de faux négatifs (False Negatives)

<a href="./_static/rappel.gif" target="_blank"><img src="./_static/rappel.gif" alt="Rappel" /></a>

**Particularités**

- le rappel doit souvent être équilibré avec la précision : en augmentant le rappel, on risque souvent de diminuer la précision, car le modèle pourrait commencer à classer plus de cas négatifs comme positifs pour s'assurer de ne pas rater les cas positifs.
- la F-mesure permet de faire ce compromis entre précision et rappel

```{admonition} Classification multiclasse et multilabel
La biliothèque scikit-learn permet aussi de calculer le rappel pour de la **classification multiclasse et multilabel** grâce au paramètre `average` de la fonction <a href="https://scikit-learn.org/stable/modules/generated/sklearn.metrics.recall_score.html" target="_blank">recall_score</a>.
```

##### Spécificité

En classification binaire, la spécificité est l'équivalent du **rappel pour les négatifs**. Tandis que le rappel se concentre sur la capacité du modèle à identifier correctement les vrais positifs, la spécificité se concentre sur sa capacité à identifier correctement les **vrais négatifs**. Elle mesure la proportion des vrais négatifs correctement identifiés par rapport à toutes les instances réelles négatives.

$\text{Spécificité} = \frac{\text{TN}}{\text{TN} + \text{FP}}$

Avec :

- $TN$ nombre de vrais négatifs (True Negatives)
- $FP$ nombre de faux positifs (False Positives)

<a href="./_static/specificite.gif" target="_blank"><img src="./_static/specificite.gif" alt="Spécificité" /></a>

``````{admonition} En python
Dans la bibliothèque scikit-learn, il n'existe **pas de fonction** pour calculer directement la **spécificité**. Néanmoins, il est possible de la calculer à partir des données de la <a href="https://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html" target="_blank">matrice de confusion</a> :

```python
from sklearn.metrics import confusion_matrix

y_true = [0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1]
y_pred = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1]

tn, fp, fn, tp = confusion_matrix(y_true, y_pred).ravel()
specificity = tn / (tn+fp)
print(specificity)
```

On peut aussi la calculer à partir du <a href="https://scikit-learn.org/stable/modules/generated/sklearn.metrics.recall_score.html" target="_blank">rappel</a> en inversant la classe positive avec le paramètre `pos_label` :

```python
from sklearn.metrics import recall_score

y_true = [0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1]
y_pred = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1]

specificity = recall_score(y_true, y_pred, pos_label=0)
print(specificity)
```
``````

##### Indice de Jaccard

L'<a href="https://fr.wikipedia.org/wiki/Indice_et_distance_de_Jaccard" target="_blank">indice de Jaccard</a> mesure la similarité entre les ensembles de labels prédits et les ensembles de labels réels. Il est défini comme la taille de l'intersection divisée par la taille de l'union des deux ensembles de labels. En classification binaire, cela correspond au nombre de vrais positifs divisé par la somme des vrais positifs, des faux positifs et des faux négatifs.

$\text{Jaccard} = \frac{TP}{TP + FP + FN}$

Avec :

- $TP$ nombre de vrais positifs (True Positives)
- $FP$ nombre de faux positifs (False Positives)
- $FN$ nombre de faux négatifs (False Negatives)

<a href="./_static/jaccard.gif" target="_blank"><img src="./_static/jaccard.gif" alt="Indice de Jaccard" /></a>

Dans scikit-learn, l'indice de Jaccard est **directement calculable** avec la fonction <a href="https://scikit-learn.org/stable/modules/generated/sklearn.metrics.jaccard_score.html" target="_blank">jaccard_score</a>. Il peut être calculé pour de la **classification multiclasse ou multilabel** grâce au paramètre `average`.

##### F-mesure

La F-mesure (ou F-score en anglais) combine la précision et le rappel  en une seule métrique harmonique. La raison d'utiliser une moyenne harmonique plutôt qu'une moyenne arithmétique est qu'elle pénalise les modèles qui ont une **grande différence** entre la précision et le rappel. Elle est généralement exprimée par la formule suivante, où $\beta$ est un **facteur** qui détermine le **poids** du **rappel** dans la moyenne harmonique :

$F_{\beta} = (1 + \beta^2) \cdot \frac{\text{Precision} \times \text{Recall}}{(\beta^2 \times \text{Precision}) + \text{Recall}}$

Le cas le plus courant est la F1-mesure (ou F1-score en anglais) où $\beta=1$ donnant un **poids égal** à la **précision** et au **rappel** :

$F1 = 2 \cdot \frac{\text{Precision} \times \text{Recall}}{\text{Precision} + \text{Recall}}$

Ces métriques sont utilisables dans la bibliothèque scikit-learn grâce aux fonctions <a href="https://scikit-learn.org/stable/modules/generated/sklearn.metrics.f1_score.html" target="_blank">f1_score</a> et <a href="https://scikit-learn.org/stable/modules/generated/sklearn.metrics.fbeta_score.html" target="_blank">fbeta_score</a>.

``````{admonition} Rapport de classification
Dans la bibliothèque scikit-learn, il est possible d'afficher une **synthèse des principales métriques de classification** grâce à la fonction <a href="https://scikit-learn.org/stable/modules/generated/sklearn.metrics.classification_report.html" target="_blank">classification_report</a>. Voici un exemple de code avec les mêmes données que précédemment :

```python
from sklearn.metrics import classification_report

y_true = [0, 0, 0, 0, 0, 1, 2, 2, 2, 2, 2, 2]
y_pred = [0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 2]
target_names = ['class 0', 'class 1', 'class 2']
print(classification_report(y_true, y_pred, target_names=target_names))
```
```
              precision    recall  f1-score   support

     class 0       0.57      0.80      0.67         5
     class 1       0.25      1.00      0.40         1
     class 2       1.00      0.17      0.29         6

    accuracy                           0.50        12
   macro avg       0.61      0.66      0.45        12
weighted avg       0.76      0.50      0.45        12
```
Le **support** correspond au **nombre d'observations** dans chaque classe.

``````

##### AUC et courbe ROC

```{margin} 📰 Histoire
<img src="https://cdn.pixabay.com/photo/2013/07/12/15/59/proximity-150698_1280.png" alt="CCD" />

L'origine de la courbe ROC remonte à la **Seconde Guerre mondiale** et aux **systèmes de radar** utilisés pour la **détection des ennemis**. Les opérateurs de radar avaient besoin d'une méthode pour déterminer si un signal détecté indiquait réellement la présence d'un ennemi. Ils devaient faire un **compromis** entre détecter autant de vrais signaux que possible (vrais positifs) et minimiser les fausses alertes (faux positifs).

*Source de l'image : pixabay*
```

La <a href="https://fr.wikipedia.org/wiki/Courbe_ROC" target="_blank">courbe ROC</a> (Receiver Operating Characteristic) est un graphique qui illustre la **capacité de discrimination** d'un modèle de **classification binaire** à tous les **seuils de classification**.

L'**axe des abscisses** correspond au **taux de faux positifs** :

$\text{FPR} = \frac{FP}{FP + TN} = 1 - Spécificité = 1 - \frac{TN}{FP + TN}$

Avec :

- $TN$ nombre de vrais négatifs (True Negatives)
- $FP$ nombre de faux positifs (False Positives)

<a href="./_static/fpr.gif" target="_blank"><img src="./_static/fpr.gif" alt="Taux de faux positifs" /></a>

L'**axe des ordonnées** correspond au **rappel** (taux de vrais positifs) : 

$\text{Rappel} = \frac{TP}{TP + FN}$

Avec :

- $TP$ nombre de vrais positifs (True Positives)
- $FN$ nombre de faux négatifs (False Negatives)

<a href="./_static/rappel.gif" target="_blank"><img src="./_static/rappel.gif" alt="Rappel" /></a>

L'aire sous la courbe ROC (Area Under the Curve - AUC) est une mesure qui résume la **performance globale** du modèle. L'AUC est une valeur numérique comprise entre 0 et 1. Plus l'AUC est proche de 1, meilleure est la capacité du modèle à différencier les classes positives des négatives. Une AUC de 0.5 indique une performance équivalente à un tirage aléatoire.

Si la classe positive est rare, il peut être plus intéressant d'utiliser la **courbe précision-rappel (PR)** plutôt que la courbe ROC.


## Différentes manières de combiner des modèles

Les modèles construits à partir d'un ensemble d'autres modèles s'appelle des modèles ensemblistes.

### Empilement

L'empilement (stacking en anglais) est une technique d'apprentissage automatique avancée basée sur un ensemble de modèles. Elle consiste à combiner les prédictions de plusieurs modèles de base et à utiliser un autre modèle pour effectuer une prédiction finale basée sur ces prédictions combinées.

### Vote

Le vote (voting en anglais) est une autre technique d'ensemble en apprentissage automatique qui combine les prédictions de plusieurs modèles différents pour faire une prédiction finale. Elle est basée sur la **majorité des votes** de ces modèles pour la **classification** et sur la **moyenne** pour la **régression**. 

#### Classification

- Hard Voting 

- Soft Voting

#### Régression

- Voting Moyen 

- Voting Pondéré

### Bootstrap Aggregating

```{sidebar} 📖 Papier de recherche
<a href="https://www.stat.berkeley.edu/~breiman/bagging.pdf" target="_blank">Bagging Predictors</a>, <a href="https://fr.wikipedia.org/wiki/Leo_Breiman" target="_blank">Leo Breiman</a>, 1994
```


Le <a href="https://fr.wikipedia.org/wiki/Bootstrap_aggregating" target="_blank">bootstrap aggregating</a> (bagging) est une technique d'ensemble en apprentissage automatique conçue pour améliorer la stabilité et l'exactitude des algorithmes. Cette technique fonctionne en plusieurs étapes :

1. Création des ensembles d'apprentissage (bootstrap sampling)

2. Entraînement de modèles séparés

3. Agrégation des prédictions

### Boosting

Contrairement au bagging, le boosting entraîne des modèles de manière séquentielle, chaque modèle suivant tentant de corriger les erreurs du modèle précédent.

___
## Support de présentation du cours

<a href="./_static/slides/Part_1.pdf" target="_blank">👨‍🏫 Support de cours sur l'apprentissage automatique</a>

<div class="container">
  <iframe class="responsive-iframe" src="./_static/slides/Part_1.pdf" width="600" height="340" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div>