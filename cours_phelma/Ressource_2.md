# 🐍 Bibliothèques open source

## 🪐 Ecosystème Jupyter

Le **projet Jupyter** est un projet **open source** qui vise à développer des **outils interactifs** pour la **programmation** et l'**analyse de données**. Il tire son nom des trois langages de programmation principaux pris en charge par le projet : **Ju**lia, **Py**thon et **R**.
Il comprend différents projets :
___
<a href="https://jupyter-notebook.readthedocs.io/en/latest/" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/1200px-Jupyter_logo.svg.png" alt="Jupyter Notebook" height="100px"/></a>

**Jupyter Notebook**

Il s'agit d'une **application web open-source** qui permet de créer et de partager des documents contenant du code exécutable, des équations, des visualisations et du texte explicatif. Utilisé pour la science des données, il combine le **code**, les **données** et leur **description** dans un seul document interactif.

___
<a href="https://jupyterlab.readthedocs.io/en/stable/" target="_blank"><img src="https://opengraph.githubassets.com/919696745a69d86f044f1320b5bb6b3864491022c63b829a28bdb3d870956653/jupyterlab/jupyterlab" alt="Jupyter Lab" height="100px"/></a>

**Jupyter Lab**

C'est une **interface utilisateur** de nouvelle génération pour le projet Jupyter. Elle offre toutes les fonctionnalités familières du Jupyter Notebook comme les notebooks, les terminaux et les éditeurs de texte mais avec une interface **plus modulaire** et **flexible**.

___
<a href="https://jupyter.org/hub" target="_blank"><img src="https://jupyterhub.readthedocs.io/en/1.0.0/_static/logo.png" alt="Jupyter Hub" height="100px"/></a>


**Jupyter Hub**

Ce projet permet de déployer et de gérer des environnements Jupyter **multi-utilisateurs** sur des **serveurs** ou dans le **cloud**. Il facilite l'accès à Jupyter pour des groupes d'utilisateurs souvent utilisé dans un contexte **éducatif** ou **professionnel**.

___
<a href="https://jupyterlite.readthedocs.io/en/stable/" target="_blank"><img src="https://jupyterlite.readthedocs.io/en/stable/_static/wordmark.svg" alt="Jupyter Lite" height="100px"/></a>

**Jupyter Lite**

JupyterLite est une distribution Jupyter **légère** qui fonctionne **entièrement dans le navigateur**. Il offre la plupart des fonctionnalités de Jupyter **sans nécessiter de serveur backend** ce qui la rend idéale pour une utilisation rapide et accessible sur des appareils avec des ressources limitées.
___
D'autres projets **gravitent** autour de Jupyter :

- <a href="https://jupyterbook.org/en/stable/intro.html" target="_blank">Jupyter Book</a> (solution retenue pour la plateforme de ce module)

Jupyter Book est un outil pour **créer des livres** et des **documents de recherche interactifs** à partir de contenu en **Markdown** et de **notebooks Jupyter** intégrant des visualisations interactives et du code exécutable.

- <a href="https://rise.readthedocs.io/en/latest/" target="_blank">Jupyter Slide</a>

Jupyter Slide, également connu sous le nom de "RISE" (Reveal.js - Jupyter/IPython Slideshow Extension), est une **extension pour Jupyter** qui permet de transformer un notebook Jupyter en une **présentation dynamique** de type diaporama, utilisant Reveal.js pour la mise en forme et la navigation.

- <a href="https://nbviewer.org/" target="_blank">nbviewer</a> (solution retenue pour afficher les TD)

Nbviewer est un **service en ligne** qui permet de **visualiser des notebooks Jupyter** stockés sur le web sous forme de **pages HTML statiques**. Il facilite le **partage** et la **visualisation de notebooks** sans nécessiter d'installation de Jupyter.

- <a href="https://nbconvert.readthedocs.io/en/latest/usage.html" target="_blank">nbconvert</a>

Nbconvert est un outil du projet Jupyter qui permet de **convertir les notebooks Jupyter** en d'autres **formats statiques** tels que HTML, PDF, Markdown. Il facilite ainsi le **partage** et la **publication** de leur contenu. Cette conversion prend en charge l'intégration des sorties de code, des graphiques et des formats de texte enrichi rendant les notebooks accessibles dans divers contextes.

- <a href="https://nbgrader.readthedocs.io/en/stable/" target="_blank">nbgrader</a>

Nbgrader est un **outil pour Jupyter** qui aide les enseignants à **créer**, **distribuer** et **noter des devoirs** sous forme de notebooks Jupyter. Il automatise la collecte, la notation et la restitution de notebooks.

- <a href="https://ipywidgets.readthedocs.io/en/stable/" target="_blank">ipywidgets</a> (solution utilisée pour le TD1)

Ipywidgets est une bibliothèque pour Jupyter qui permet de créer des **widgets interactifs** dans les notebooks. Elle facilite la création d'**interfaces utilisateur interactives** pour l'exploration de données et la visualisation. Ces widgets peuvent être utilisés pour manipuler et visualiser des données en temps réel, offrant une interaction dynamique avec le code exécuté dans le notebook.
___

Les **Jupyter notebooks** et **labs** peuvent être utilisés de **deux façons différentes** :
- *En local, sur votre ordinateur*
    - En installant l'environnement de data science Anaconda : <a href="https://docs.anaconda.com/free/anaconda/install/index.html" target="_blank">installer Jupyter via Anaconda</a> sur un ordinateur local crée un environnement complet de data science, intégrant Jupyter Notebook/Lab et d'autres outils utiles, facilitant la gestion des dépendances et des environnements isolés pour les projets de science des données.
    - En installant l'écosystème Jupyter comme bibliothèques Python : <a href="https://jupyter.org/install" target="_blank">installer Jupyter comme une bibliothèque Python via pip</a> permet d'intégrer Jupyter Notebook ou Lab dans un **environnement Python existant** offrant plus de **flexibilité** pour les **utilisateurs expérimentés** qui souhaitent personnaliser leur configuration.
    - En installant Jupyter Lab Desktop : <a href="https://github.com/jupyterlab/jupyterlab-desktop" target="_blank">Jupyter Lab Desktop</a> est une **application de bureau autonome** pour Jupyter, offrant une interface utilisateur riche **sans nécessiter un navigateur web** ce qui simplifie l'accès et l'utilisation des notebooks Jupyter sur un ordinateur local.
    - Avec Jupyter Lite

- *Sur le web, sur un serveur distant*
    - Avec Jupyter Hub déployé sur un **serveur distant privé**
    - Avec Jupyter Hub déployé sur un serveur distant d'un **fournisseur de cloud** (OVH, Google, Microsoft, AWS...)

## 🐍 Bibliothèques Python utiles pour les données et l'intelligence artificielle

```{note}
Toutes les bibliothèques Python présentées sont **open-source**. Les **liens vers la documentation** sont accessibles en cliquant sur les logos des bibliothèques. 
```

## Bibliothèques généralistes

<a href="https://numpy.org" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/NumPy_logo_2020.svg/1200px-NumPy_logo_2020.svg.png" alt="Numpy" height="100px"/></a> 

La bibliothèque <a href="https://numpy.org" target="_blank">Numpy</a> est fondamentale pour le **calcul numérique** en Python. Elle offre un support efficace pour la manipulation de **tableaux multidimensionnels** (scalaires, vecteurs, matrices, tenseurs) et des **opérations mathématiques** sur ces tableaux. Elle est largement utilisée dans le domaine de la **science des données** (data science), de l'**apprentissage automatique** et de l'**analyse numérique**.

___
<a href="https://docs.scipy.org/doc/scipy/" target="_blank"><img src="https://docs.scipy.org/doc/scipy/_static/logo.svg" alt="Scipy" height="100px"/></a> 

La bibliothèque <a href="https://docs.scipy.org/doc/scipy/" target="_blank">Scipy</a>  fournit des **outils** et des **fonctions** pour la résolution de **problèmes scientifiques** et d'**ingénierie**, notamment l'optimisation, l'algèbre linéaire, la statistique, le traitement du signal... Elle s'appuie sur la bibliothèque **NumPy** en fournissant des fonctionnalités avancées pour l'**analyse de données** et la **modélisation numérique**.

___
<a href="https://matplotlib.org" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/fr/thumb/3/37/Logo_Matplotlib.svg/langfr-2560px-Logo_Matplotlib.svg.png" alt="Matplotlib" height="100px"/></a> 

La bibliothèque <a href="https://matplotlib.org" target="_blank">Matplotlib</a> permet de créer des **graphiques de haute qualité**, des tracés **2D et 3D**, des **diagrammes**, des **histogrammes** et d'autres représentations graphiques de données de manière personnalisable. Elle est largement utilisée dans le domaine de la **science des données** (data science) et de la **recherche scientifique**.

___
<a href="https://seaborn.pydata.org" target="_blank"><img src="https://seaborn.pydata.org/_static/logo-wide-lightbg.svg" alt="Seaborn" height="100px"/></a> 

La bibliothèque <a href="https://seaborn.pydata.org" target="_blank">Seaborn</a> est construite au-dessus de la bibliothèque de visualisation de données **Matplotlib**. Elle aide à la création de graphiques en offrant des **fonctions simples**. Elle est particulièrement adaptée à la **visualisation de données statistiques complexes** et est couramment utilisée dans le domaine de l'**analyse de données** pour explorer et présenter des relations entre les variables.

___
<a href="https://scikit-learn.org/stable/" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Scikit_learn_logo_small.svg/1200px-Scikit_learn_logo_small.svg.png" alt="Scikit-learn" height="100px"/></a> 

La bibliothèque <a href="https://scikit-learn.org/stable/" target="_blank">Scikit-learn</a> permet de faire du **machine learning**. Elle offre une gamme d'outils efficaces pour l'**analyse de données** et la **modélisation statistique** : la classification, la régression, le clustering et la réduction de dimensionnalité.

👨‍🏫 <a href="https://scikit-learn.org/stable/tutorial/index.html" target="_blank">Tutoriel pour utiliser Scikit-learn</a>

___
<a href="https://keras.io" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Keras_logo.svg/langfr-1280px-Keras_logo.svg.png" alt="Keras" height="100px"/></a> 

La bibliothèque <a href="https://keras.io" target="_blank">Keras</a> permet de créer des **réseaux de neurones artificiels**. Elle est réputée pour sa **simplicité** en fournissant une **interface haut niveau** pour la **construction**, l'**entrainement** et l'**évaluation** de modèles d'apprentissage profond (deep learning). Cette bibliothèque est très utilisée par les **développeurs** en apprentissage profond (deep learning). 

👩‍🏫 <a href="https://keras.io/getting_started/intro_to_keras_for_engineers/" target="_blank">Tutoriel pour utiliser Keras</a>
___
<a href="https://www.tensorflow.org/?hl=fr" target="_blank"><img src="https://www.gstatic.com/devrel-devsite/prod/v7101fe1ae309bf6f8b73931812f2304140fb851e33f225a68507501988007b93/tensorflow/images/lockup.svg" alt="Tensorflow" height="100px"/></a>

La bibliothèque <a href="https://www.tensorflow.org/?hl=fr" target="_blank">TensorFlow</a> est  très populaire pour le **développement de modèles d'apprentissage profond (deep learning)**. Elle offre une **infrastructure flexible** pour la **création**, l'**entrainement** et le **déploiement de réseaux de neurones**. Elle contient aussi des **outils pour la manipulation de données** et des **fonctionnalités avancées d'optimisation**.

👨‍🏫 <a href="https://www.tensorflow.org/tutorials?hl=fr" target="_blank">Tutoriel pour utiliser Tensorflow</a>
___
<a href="https://pytorch.org/get-started/locally/" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/PyTorch_logo_black.svg/976px-PyTorch_logo_black.svg.png" alt="Pytorch" height="100px"/></a>

La bibliothèque <a href="https://pytorch.org/get-started/locally/" target="_blank">PyTorch</a> est très populaire pour le **développement de modèles d'apprentissage profond (deep learning)**. Elle se distingue par sa **flexibilité** et son **approche dynamique** de la construction de modèles. Elle offre une **interface conviviale** pour la **création**, l'**entrainement** et le **déploiement de réseaux de neurones**. Elle est largement utilisée pour des tâches telles que la **vision par ordinateur**, le **traitement du langage naturel (NLP)** et d'autres domaines de l'apprentissage profond.

👩‍🏫 <a href="https://github.com/udacity/deep-learning-v2-pytorch" target="_blank">Tutoriel pour utiliser PyTorch</a>
___

## Bibliothèques spécialisées dans l'image

<a href="https://pillow.readthedocs.io/en/stable/" target="_blank"><img src="https://raw.githubusercontent.com/python-pillow/pillow-logo/main/pillow-logo-dark-text-1280x640.png" alt="Pillow" height="100px"/></a>

La bibliothèque <a href="https://pillow.readthedocs.io/en/stable/" target="_blank">Pillow</a> facilite la **manipulation d'images** notamment le **chargement**, la **modification** et la **sauvegarde d'images dans divers formats**. Elle offre des fonctionnalités puissantes pour le **traitement d'images** telles que le **redimensionnement**, la **rotation**, la **conversion de couleurs**, la **création de miniatures**... C'est un **outil essentiel** pour le **traitement d'images** dans de nombreuses applications Python.
___

<a href="https://docs.opencv.org/4.8.0/" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/5/53/OpenCV_Logo_with_text.png" alt="OpenCV" height="100px"/></a> 

La bibliothèque <a href="https://docs.opencv.org/4.8.0/" target="_blank">OpenCV</a> permet de faire de la **vision par ordinateur**. Elle offre un **large éventail de fonctionnalités** pour le **traitement d'images** et de **vidéos** : la **détection d'objets**, la **reconnaissance de formes**, la **segmentation d'images**, et la **manipulation d'images en temps réel**. Elle est largement utilisée dans des domaines comme la **robotique**, la **réalité augmentée** et la **surveillance vidéo** pour développer des **applications visuelles complexes**.

___
<a href="https://scikit-image.org" target="_blank"><img src="https://scikit-image.org/_static/img/logo.png" alt="scikit-image" height="100px"/></a> 

La bibliothèque <a href="https://scikit-image.org" target="_blank">Scikit-image</a> est spécialisée dans le **traitement d'images**. Elle offre un **ensemble d'outils et de fonctions** pour effectuer diverses opérations de **traitement d'images** : la **segmentation**, la **détection de contours**, la **mesure de formes**, la **restauration d'images**... Elle s'intègre bien avec d'autres bibliothèques scientifiques comme **NumPy** et **SciPy**.
___
<a href="https://keras.io/keras_cv/" target="_blank"><img src="https://opengraph.githubassets.com/2af5e1ea6368c7a0be1c289ee4625d41c280032a300f25c278c58eb7e0372d2b/keras-team/keras-cv" alt="KerasCV" height="100px"/></a>

La bibliothèque <a href="https://scikit-image.org" target="_blank">KerasCV</a> est une **extension** de la **bibliothèque Keras**. Elle propose une collection de modèles de **réseaux de neurones pré-entraînés** pour la **vision par ordinateur**, des outils pour **simplifier la création** et l'**évaluation** de modèles de deep learning pour des tâches de traitement d'images. Elle facilite la mise en oeuvre **rapide** de solutions de pointe en vision par ordinateur en tirant parti de **modèles pré-entraînés**.

___
<a href="https://mahotas.readthedocs.io/en/latest/index.html" target="_blank"><img src="https://opengraph.githubassets.com/97c04a93217e13ab130073ee89a74bff4059aa3354f12367b1ff798b3eefe21e/luispedro/mahotas" alt="Mahotas" height="100px"/></a>

La bibliothèque <a href="https://mahotas.readthedocs.io/en/latest/index.html" target="_blank">Mahotas</a> est spécialisée dans le **traitement d'images**. Elle offre des fonctionnalités pour effectuer des opérations de **traitement d'images avancées** : la **segmentation**, la **détection de caractéristiques**, la **transformation d'images** et l'**extraction d'objets**. Elle est souvent utilisée pour l'**analyse d'images** dans des **applications scientifiques** et **industrielles** en raison de sa rapidité et de sa polyvalence.

## Bibliothèques spécialisées dans l'augmentation d'images

Pour améliorer la **robustesse du modèle**, les images sont souvent modifiées par des **techniques d'augmentation** afin de générer des données d'entraînement **supplémentaires** à partir de l'ensemble existant.
___
<a href="https://imgaug.readthedocs.io/en/latest/" target="_blank"><img src="https://imgaug.readthedocs.io/en/latest/_images/heavy.jpg" alt="Imgaug" height="100px"/></a>

La bibliothèque <a href="https://imgaug.readthedocs.io/en/latest/" target="_blank">imgaug</a> est dédiée à l'**augmentation d'images**. Elle offre une grande variété de **transformations d'images** comme la rotation, le recadrage, le zoom, le bruit... Elle est hautement personnalisable et peut être intégrée facilement dans les **pipelines de prétraitement d'images**.

___
<a href="https://albumentations.ai/docs/" target="_blank"><img src="https://albumentations.ai/docs/images/logo.png" alt="Albumentations" height="100px"/></a>

La bibliothèque <a href="https://albumentations.ai/docs/" target="_blank">Albumentations</a> permet de faire de l'augmentation d'images hautement optimisée. Elle est spécialement conçue pour les tâches de **vision par ordinateur**. Elle offre une grande **variété de transformations d'images** et est réputée pour sa **rapidité**.

___
Les bibliothèques <a href="https://pillow.readthedocs.io/en/stable/" target="_blank">Pillow</a>, <a href="https://docs.opencv.org/4.8.0/" target="_blank">OpenCV</a> et <a href="https://www.tensorflow.org/tutorials/images/data_augmentation?hl=fr" target="_blank">Keras</a> permettent aussi de faire de l'**augmentation de données** pour le traitement d'images.
