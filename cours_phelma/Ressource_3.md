# 📊 Ressources d'open data

Les données sont au coeur du développement de l'intelligence artificielle. La majorité des applications actuelles reposent sur des algorithmes d'**apprentissage automatique** dont l'efficacité dépend de la **qualité** et de la **diversité** des données d'entrainement utilisées. Accéder à des sources de données ouvertes peut faciliter la réalisation de vos projets d'IA. Voici quelques **exemples de plateformes** où vous pouvez trouver des jeux de données pertinents :

- [data.gouv.fr](https://www.data.gouv.fr/fr/) : la plateforme française de données ouvertes, couvrant un large éventail de domaines (économie, santé, environnement, etc.)

- [data.metropolegrenoble.fr](https://data.metropolegrenoble.fr/) : le portail de données ouvertes de la métropole grenobloise pour des projets locaux

- [opendata.edf.fr](https://opendata.edf.fr/pages/welcome/) : accès aux données énergétiques d’EDF pour des projets liés à l’énergie ou à la transition écologique

- [api.gouv.fr](https://api.gouv.fr/) : une plateforme qui regroupe diverses API publiques pour accéder à des données gouvernementales et administratives