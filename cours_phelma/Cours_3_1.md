<style>
    /* Pour la gestion des iframes */
 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 📚 Cours : Les réseaux de neurones complétement connectés

## Base des réseaux de neurones : le perceptron

### Histoire du perceptron

Le <a href="https://fr.wikipedia.org/wiki/Perceptron" target="_blank">perceptron</a> a été inventé par <a href="https://fr.wikipedia.org/wiki/Frank_Rosenblatt" target="_blank">Frank Rosenblatt</a> en **1957**. C'est un modèle de **neurone artificiel** qui peut apprendre à effectuer des tâches de **classification simples**.

Le perceptron fonctionne en recevant des entrées, en les pondérant, en les sommant puis en passant la somme à travers une fonction d'activation pour produire une sortie. La règle d'apprentissage du perceptron ajuste les poids en fonction des erreurs commises dans les prédictions.

### Fonction d'activation

La **fonction d'activation** en sortie du perceptron permet de classifier les données en **deux classes**. La fonction utilisée est la **fonction de Heaviside** (fonction échelon) :

$H(z) = \begin{cases} 
0 & \text{si } z < 0 \\
1 & \text{si } z \geq 0
\end{cases}$

``````{admonition} En Python 🐍
En Python, on peut créer cette fonction **simplement** avec la <a href="https://docs.python.org/fr/3/library/index.html" target="_blank">bibliothèque standard</a>. Néanmoins, la bibliothèque <a href="https://numpy.org/doc/stable/index.html" target="_blank">numpy</a> facilite les calculs en permettant de <a href="https://numpy.org/doc/stable/reference/generated/numpy.vectorize.html" target="_blank">vectoriser</a> des fonctions Python :

```python
import matplotlib.pyplot as plt
import numpy as np

# Création de la fonction échelon
def activation_function(x):
    return 1 if x >= 0 else 0

# Tracé de la fonction
z = np.linspace(-10, 10, 100)
H = np.vectorize(activation_function)(z)

plt.plot(z, H)
plt.xlabel("z")
plt.ylabel("H(z)")
plt.title("Fonction échelon")
plt.show()
```

<img src="./_static/echelon.png" alt="Fonction échelon" height="400px"/>
``````

### Mise à jour des poids

La mise à jour des poids se fait de **manière itérative**. L'objectif est d'ajuster les poids pour **réduire l'erreur** entre la sortie prédite par le perceptron et la sortie réelle attendue. Cette mise à jour s'effectue en **plusieurs étapes** :

- **Initialisation**

Les poids $w_i$​ et le biais $b$ sont initialisés (à de petites valeurs aléatoires ou à zéro).

``````{admonition} En Python 🐍
On peut définir une classe Python pour coder le fonctionnement du perceptron :

```python
import numpy as np

class Perceptron:
    def __init__(self, learning_rate=0.1, n_iterations=100):
        self.learning_rate = learning_rate
        self.n_iterations = n_iterations
        self.weights = None
        self.bias = None

    def activation_function(self, x):
        return 1 if x >= 0 else 0

    def train(self, X, y_reel):
        n_samples, n_features = X.shape
        self.weights = np.zeros(n_features) # Initialisation des poids
        self.bias = 0 # Initialisation du biais
```
``````

- **Présentation d'un exemple**

Pour chaque exemple d'entraînement (vecteur d'entrée $x=(x_1,x_2,...,x_n)$ et une sortie attendue $y_{réel}$), le perceptron calcule une sortie $y_{pred}$.

``````{admonition} En Python 🐍
On peut continuer de définir la classe Python :

```python
import numpy as np

class Perceptron:
    def __init__(self, learning_rate=0.1, n_iterations=100):
        self.learning_rate = learning_rate
        self.n_iterations = n_iterations
        self.weights = None
        self.bias = None

    def activation_function(self, x):
        return 1 if x >= 0 else 0

    def train(self, X, y_reel):
        n_samples, n_features = X.shape
        self.weights = np.zeros(n_features) # Initialisation des poids
        self.bias = 0 # Initialisation du biais
        for _ in range(self.n_iterations): # Boucle sur époques
            for idx, x_i in enumerate(X): # Boucle sur données
                linear_output = np.dot(x_i, self.weights) + self.bias # Calcul sortie avant fonction d'activation
                y_pred = self.activation_function(linear_output) # Calcul sortie après fonction d'activation

```
``````

- **Calcul de l'erreur**

L'erreur est la différence entre la sortie attendue et la sortie prédite : 

$erreur = y_{réel} - y_{pred}$

``````{admonition} En Python 🐍
On peut continuer de définir la classe Python :

```python
import numpy as np

class Perceptron:
    def __init__(self, learning_rate=0.1, n_iterations=100):
        self.learning_rate = learning_rate
        self.n_iterations = n_iterations
        self.weights = None
        self.bias = None

    def activation_function(self, x):
        return 1 if x >= 0 else 0

    def train(self, X, y_reel):
        n_samples, n_features = X.shape
        self.weights = np.zeros(n_features) # Initialisation des poids
        self.bias = 0 # Initialisation du biais
        for _ in range(self.n_iterations): # Boucle sur toutes les époques
            for idx, x_i in enumerate(X): # Boucle sur toutes les données
                linear_output = np.dot(x_i, self.weights) + self.bias # Calcul de la sortie avant la fonction d'activation
                y_pred = self.activation_function(linear_output) # Calcul de la sortie après la fonction d'activation
                erreur = y_reel[idx] - y_pred # Calcul de l'erreur
```
``````

- **Mise à jour des poids**

Chaque poids $w_i$​ est mis à jour en fonction de l'erreur, du taux d'apprentissage $\eta$ et de la valeur de l'entrée correspondante $x_i$​ :

$w_i(t+1) = w_i(t) + \eta \cdot erreur \cdot x_i$

$w_i(t+1) = w_i(t) + \eta (y_{réel} - y_{pred}) x_i$

$t+1$ correspond au cycle d'entrainement qui suit le cycle $t$.

De la même façon, le biais $b$ est mis à jour :

$b(t+1) = b(t) + \eta \cdot erreur$

$b(t+1) = b(t) + \eta \cdot (y_{réel} - y_{pred})$

En général, on intégre le biais dans le vecteur des poids en ajoutant une entrée constante égale à 1.

``````{admonition} En Python 🐍
On peut continuer de définir la classe Python :

```python
import numpy as np

class Perceptron:
    def __init__(self, learning_rate=0.1, n_iterations=100):
        self.learning_rate = learning_rate
        self.n_iterations = n_iterations
        self.weights = None
        self.bias = None

    def activation_function(self, x):
        return 1 if x >= 0 else 0

    def train(self, X, y_reel):
        n_samples, n_features = X.shape
        self.weights = np.zeros(n_features) # Initialisation des poids
        self.bias = 0 # Initialisation du biais
        for _ in range(self.n_iterations): # Boucle sur toutes les époques
            for idx, x_i in enumerate(X): # Boucle sur toutes les données
                linear_output = np.dot(x_i, self.weights) + self.bias # Calcul de la sortie avant la fonction d'activation
                y_pred = self.activation_function(linear_output) # Calcul de la sortie après la fonction d'activation
                erreur = y_reel[idx] - y_pred # Calcul de l'erreur
                self.weights += self.learning_rate * erreur * x_i # Mise à jour des poids
                self.bias += self.learning_rate * erreur # Mise à jour du biais
```
``````

- **Répétition**

Ce processus est **répété** pour **chaque exemple d'entraînement** (souvent sur plusieurs cycles appelées **époques** quand il s'agit de l'intégralité du jeu d'entrainement) jusqu'à ce que le perceptron **converge** ou qu'un **nombre maximum d'itérations** soit atteint.

``````{admonition} En Python 🐍
Cette partie correspond aux **deux boucles imbriquées** de la classe Python déjà construite :

```python
import numpy as np

class Perceptron:
    def __init__(self, learning_rate=0.1, n_iterations=100):
        self.learning_rate = learning_rate
        self.n_iterations = n_iterations
        self.weights = None
        self.bias = None

    def activation_function(self, x):
        return 1 if x >= 0 else 0

    def train(self, X, y_reel):
        n_samples, n_features = X.shape
        self.weights = np.zeros(n_features) # Initialisation des poids
        self.bias = 0 # Initialisation du biais
        for _ in range(self.n_iterations): # Boucle sur toutes les époques
            for idx, x_i in enumerate(X): # Boucle sur toutes les données
                linear_output = np.dot(x_i, self.weights) + self.bias # Calcul de la sortie avant la fonction d'activation
                y_pred = self.activation_function(linear_output) # Calcul de la sortie après la fonction d'activation
                erreur = y_reel[idx] - y_pred # Calcul de l'erreur
                self.weights += self.learning_rate * erreur * x_i # Mise à jour des poids
                self.bias += self.learning_rate * erreur # Mise à jour du biais
```
``````

### Prédiction

Une fois la **phase d'entrainement réalisée**, on peut utiliser les poids et le biais pour **effectuer des prédictions** sur de **nouvelles données**.

$\hat{y} = g(\displaystyle \sum_{i=1}^{n} w_i x_i + w_0)$

$\hat{y} = \begin{cases} 
0 & \text{si } \displaystyle \sum_{i=1}^{n} w_i x_i + w_0 < 0 \\
1 & \text{si } \displaystyle \sum_{i=1}^{n} w_i x_i + w_0 \geq 0
\end{cases}$

Avec :

- $\hat{y}$ prédiction du perceptron
- $w_i$ poids associés à la caractéristique $i$
- $x_i$ valeur de la caractéristique (feature) $i$ de la données d'entrée du perceptron
- $w_0$ biais
- $n$ nombre de caractéristiques (features)

``````{admonition} En Python 🐍
Voici le **code complet** de la classe Python pour le perceptron avec trois exemples de test :

```python
import numpy as np

class Perceptron:
    def __init__(self, learning_rate=0.1, n_iterations=100):
        self.learning_rate = learning_rate
        self.n_iterations = n_iterations
        self.weights = None
        self.bias = None

    def activation_function(self, x):
        return 1 if x >= 0 else 0

    def train(self, X, y_reel):
        n_samples, n_features = X.shape
        self.weights = np.zeros(n_features) # Initialisation des poids
        self.bias = 0 # Initialisation du biais
        for _ in range(self.n_iterations): # Boucle sur toutes les époques
            for idx, x_i in enumerate(X): # Boucle sur toutes les données
                linear_output = np.dot(x_i, self.weights) + self.bias # Calcul de la sortie avant la fonction d'activation
                y_pred = self.activation_function(linear_output) # Calcul de la sortie après la fonction d'activation
                erreur = y_reel[idx] - y_pred # Calcul de l'erreur
                self.weights += self.learning_rate * erreur * x_i # Mise à jour des poids
                self.bias += self.learning_rate * erreur # Mise à jour du biais

    def predict(self, X):
        linear_output = np.dot(X, self.weights) + self.bias # Calcul de la combinaison linéaire avec poids et biais
        y_pred = np.vectorize(self.activation_function)(linear_output) # Fonction d'activation
        return y_pred

# Données d'entraînement (X) et étiquettes (y)
X = np.array([[2, 2], [1, 1], [2, 1], [3, 2]])
y = np.array([1, 0, 0, 1])

p = Perceptron(learning_rate=0.01, n_iterations=10)
p.train(X, y)

print("Poids : ", p.weights)
print("Biais : ", p.bias)

# Test avec de nouvelles données
test_data = np.array([[1, 2.5], [3, 3], [2, 0.5]])
predictions = p.predict(test_data)
print("Prédictions pour les données de test : ", predictions)
```

```
Poids :  [0.02 0.01]
Biais :  -0.05
Prédictions pour les données de test :  [0 1 0]
```

<img src="./_static/predictions.png" alt="Prédictions perceptron" height="400px"/>
``````

### Analyse des résultats

Comme les données sont **linéairement séparables**, l'entrainement du perceptron converge vers une solution qui classe correctement toutes les données d'entrainement.

<img src="./_static/perceptron.gif" alt="perceptron" height="400px"/>

On observe que l'entrainement du perceptron **ne permet pas de trouver la séparation maximale** entre les deux classes à la différence de la machine à vecteur de support (Support Vector Machine - SVM). Si on entraine un modèle SVM sur ces mêmes données, on observe que la droite de séparation des classes sépare mieux les données :

<img src="./_static/svm_perceptron.png" alt="SVM perceptron" height="400px"/>

### Limites du perceptron

La limitation la plus importante du perceptron est son **incapacité** à traiter des ensembles de données qui **ne sont pas linéairement séparables**. Un exemple **classique** est le problème du **"ou exclusif" XOR**, où le perceptron ne peut pas trouver une ligne droite (ou un hyperplan dans des espaces de dimensions supérieures) qui sépare les classes de manière correcte. 

Le perceptron utilise la **fonction d'activation échelon** donc il ne peut produire que des **sorties binaires**. Cela **limite** son utilisation dans des situations où des **prédictions probabilistes** sont **nécessaires**.

### Différence entre perceptron et régression logistique

Le **perceptron** et la **régression logistique** sont deux modèles utilisés pour de la **classification binaire** et qui effectuent une **combinaison linéaire** des entrées suivie d'une **fonction d'activation**. Néanmoins, ils possèdent un certain nombre de différences :

- **Fonction d'Activation**
    - *Perceptron* : utilise une fonction d'activation échelon
    - *Régression Logistique* : utilise une fonction d'activation sigmoïd
- **Méthode d'apprentissage**
    - *Perceptron* : ajuste les poids en fonction de l'erreur de classification de chaque exemple
    - *Régression Logistique* : utilise la méthode de descente de gradient
- **Sorties**
    - *Perceptron* : fournit une sortie binaire
    - *Régression Logistique* : fournit une probabilité de l'appartenance à une classe
- **Convergence**
    - *Perceptron* : ne converge que si les données sont linéairement séparables
    - *Régression Logistique* : converge vers une solution même si les données ne sont pas linéairement séparables

<img src="./_static/rl_perceptron.png" alt="Régression logistique perceptron" height="400px"/>

## Perceptron multicouche et réseau de neurones dense

### Architecture des couches de neurones

```{sidebar} 🎓 Leçon inaugurale au Collège de France
<a href="https://fr.wikipedia.org/wiki/Yann_Le_Cun" target="_blank">Yann LeCun</a>, spécialiste de l’apprentissage automatique est l’un des pères de l'apprentissage profond.

**Titre de la vidéo :** *L'apprentissage profond : une révolution en intelligence artificielle - Yann LeCun (2016)*

<div class="container">
    <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/TdLa5h-x2nA" title="L&#39;apprentissage profond : une révolution en intelligence artificielle - Yann LeCun (2016)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
```

On parle de réseaux de neurones profonds (deep neural networks) lorsqu'il y a plusieurs couches de neurones cachées (hidden layers) donc plus de trois couches au total dans le réseau.

### Propagation avant et rétropropagation

### Spécificité des réseaux denses

## Initialisation des poids

### Problèmes courants

L'initialisation des poids est une **étape cruciale** qui peut avoir un impact significatif sur la **performance** et la **convergence** des réseaux de neurones. 

Si les poids sont **initialisés trop petits**, le gradient peut devenir **de plus en plus petit** à chaque couche lors de la **propagation arrière** ce qui conduit à la **disparition du gradient**. À l'inverse, des **poids trop grands** peuvent entraîner une **explosion des gradients** où les valeurs du gradient deviennent excessivement grandes.

Une initialisation identique ou trop similaire des poids dans un réseau peut entraîner une **symétrie** où tous les neurones apprennent de la même manière. Cela réduit l'efficacité du réseau car les neurones ne parviennent pas à capturer différentes caractéristiques des données.

### Types d'initialisation

L'initialisation des poids des modèles de d'apprentissage profond s'appuie **quatre types de distributions** : uniforme, normale, normale tronquée (à deux écart-types en général) et constante.

**Plusieurs stratégies d'initialisation** ont été développées :

- **Initialisation Xavier/Glorot uniforme**

```{sidebar} 📖 Papier de recherche
Titre : <a href="https://proceedings.mlr.press/v9/glorot10a/glorot10a.pdf" target="_blank">Understanding the difficulty of training deep feedforward neural networks</a>

Auteurs : <a href="https://www.researchgate.net/profile/Xavier-Glorot" target="_blank">Xavier Glorot</a> - <a href="https://fr.wikipedia.org/wiki/Yoshua_Bengio" target="_blank">Yoshua Bengio</a>

Année de publication : 2010
```

Il s'agit du choix par défaut pour les couches denses de neurones dans les bibliothèques <a href="https://www.tensorflow.org/?hl=fr" target="_blank">Tensorflow</a> et <a href="https://keras.io" target="_blank">Keras</a>. Les poids sont choisis aléatoirement suivant la loi de distribution uniforme suivante :

$W \sim U\left[-\frac{\sqrt{6}}{\sqrt{n_{in} + n_{out}}}, \frac{\sqrt{6}}{\sqrt{n_{in} + n_{out}}}\right]$

Avec :

- $W$ poids à initialiser
- $U[a,b]$ distribution uniforme dans l'intervalle $[a,b]$
- $n_{in}$​ nombre de neurones entrants dans la couche
- $n_{out}$ nombre de neurones sortants de la couche

Les biais sont initialisés à la valeur constante 0.

Cette initialisation est souvent utilisée pour les fonctions d'activation tangente hyperbolique, sigmoïd ou softmax.

- **Initialisation He/Kaiming uniforme**

```{sidebar} 📖 Papier de recherche
Titre : <a href="https://proceedings.mlr.press/v9/glorot10a/glorot10a.pdf" target="_blank">Delving Deep into Rectifiers:
Surpassing Human-Level Performance on ImageNet Classification</a>

Auteurs : <a href="https://scholar.google.com/citations?user=DhtAFkwAAAAJ&hl=en" target="_blank">Kaiming He</a> - <a href="https://scholar.google.com/citations?user=yuB-cfoAAAAJ&hl=en" target="_blank">Xiangyu Zhang</a> - <a href="https://scholar.google.com.hk/citations?user=AUhj438AAAAJ&hl=en" target="_blank">Shaoqing Ren</a> - <a href="https://scholar.google.com/citations?user=ALVSZAYAAAAJ&hl=en" target="_blank">Jian Sun</a>

Année de publication : 2015
```

Il s'agit du choix par défaut pour les couches denses de neurones dans les bibliothèques <a href="https://pytorch.org" target="_blank">Pytorch</a> et <a href="https://pytorch.org" target="_blank">FastAI</a>. Les poids sont choisis aléatoirement suivant la loi de distribution uniforme suivante :

$W \sim U\left[-\sqrt{\frac{6}{n_{in}}}, \sqrt{\frac{6}{n_{in}}}\right]$

Avec :

- $W$ poids à initialiser
- $U[a,b]$ distribution uniforme dans l'intervalle $[a,b]$
- $n_{in}$​ nombre de neurones entrants dans la couche

Les biais sont initialisés à la valeur constante 0.

Cette initialisation est souvent utilisée pour les fonctions d'activation de type ReLU.

- **Initialisation LeCun**

```{sidebar} 📖 Papier de recherche
Titre : <a href="https://cseweb.ucsd.edu/classes/wi08/cse253/Handouts/lecun-98b.pdf" target="_blank">Efficient BackProp</a>

Auteurs : <a href="https://fr.wikipedia.org/wiki/Yann_Le_Cun" target="_blank">Yann LeCun</a> - <a href="https://fr.wikipedia.org/wiki/Léon_Bottou" target="_blank">Leon Bottou</a> - <a href="https://www.researchgate.net/profile/Genevieve-Orr" target="_blank">Genevieve B. Orr</a> - <a href="https://en.wikipedia.org/wiki/Klaus-Robert_Müller" target="_blank">Klaus-Robert Müller</a>

Année de publication : 1998
```

Il s'agit d'une initialisation similaire à l'initialisation Xavier/Glorot uniforme avec $n_i=n_{i+1}$.  Les poids sont choisis aléatoirement suivant la loi de distribution uniforme suivante :

$W \sim U\left[-\sqrt{\frac{3}{n_{in}}}, \sqrt{\frac{3}{n_{in}}}\right]$

Avec :

- $W$ poids à initialiser
- $U[a,b]$ distribution uniforme dans l'intervalle $[a,b]$
- $n_{in}$​ nombre de neurones entrants dans la couche

Les biais sont initialisés à la valeur constante 0.

Dans le papier de recherche, cette initialisation est présentée sous forme d'une **distribution normale** :

$W \sim \mathcal{N}\left(0, \sqrt{\frac{1}{n_{\text{in}}}}\right)$

Avec :

- $W$ poids à initialiser
- $N(0,\sigma)$ distribution normale avec une moyenne de 0 et un écart-type $\sigma$
- $n_{in}$​ nombre de neurones entrants dans la couche

``````{admonition} Bibliothèque Python 🐍
On peut vérifier dans la bibliothèque Keras utilise une **initialisation Xavier/Glorot uniforme** pour les couches **denses** de neurones :

```python
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense

# Créer un modèle simple avec une seule couche dense
model = Sequential([
    Dense(1000, input_shape=(500,))
])

# Initialiser le modèle pour obtenir les poids
model.compile(optimizer='adam', loss='mse')

# Extraire les poids de la première couche (Dense)
weights = model.layers[0].get_weights()[0]

# Visualiser les poids
plt.hist(weights.flatten(), bins=10)
plt.title('Distribution des poids après initialisation')
plt.xlabel('Valeurs des poids')
plt.ylabel('Fréquence')
plt.show()
```

<img src="./_static/glorot.png" alt="Régression logistique perceptron" height="400px"/>

On peut aussi vérifier que les **biais** présents dans la variable `model.layers[0].get_weights()[1]` sont tous **égaux à 0**.

``````

## Fonctions d'activation

```{sidebar} 📖 Papier de recherche
Titre : <a href="https://arxiv.org/pdf/2109.14545.pdf" target="_blank">Activation Functions in Deep Learning: A Comprehensive Survey and Benchmark</a>

Auteurs : <a href="https://scholar.google.co.in/citations?user=TioEtyEAAAAJ&hl=en" target="_blank">Shiv Ram Dubey</a> - <a href="https://scholar.google.com/citations?hl=en&user=RtmwqYEAAAAJ" target="_blank">Satish Kumar Singh</a> - <a href="https://scholar.google.com/citations?hl=en&user=LYNgDQ8AAAAJ" target="_blank">Bidyut Baran Chaudhuri</a>

Année de publication : 2022
```

Les fonctions d'activation permettent d'amener une **non-linéarité** dans le modèle. Ceci est essentiel pour apprendre des **données complexes**.

### Choix des fonctions d'activation

Pour les **couches de neurones de sortie**, le choix de la fonction d'activation dépend du **type de problème** que l'on cherche à résoudre :

- **Classification binaire**

La fonction **sigmoïde** est souvent utilisée dans la couche de sortie car elle produit des valeurs de sortie comprises **entre 0 et 1** idéales pour représenter des **probabilités**.

``````{admonition} Bibliothèque Python 🐍
Voici un exemple très simple de **classification binaire** avec la fonction **sigmoïde** pour la couche de sortie en utilisant la bibliothèque Keras : 

```python
from keras.models import Sequential
from keras.layers import Dense
import numpy as np

np.random.seed(0)
X = np.random.rand(100, 2)  # 100 échantillons, 2 caractéristiques
Y = (X[:, 0] + X[:, 1] > 1).astype(int)  # Classification binaire simple

# Création du modèle
model = Sequential()
model.add(Dense(10, input_dim=2, activation='relu'))
model.add(Dense(1, activation='sigmoid'))  # Couche de sortie avec fonction d'activation sigmoïde

# Compilation du modèle
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

# Entraînement du modèle
model.fit(X, Y, epochs=100, batch_size=10)

# Évaluation du modèle
_, accuracy = model.evaluate(X, Y)
print(f'Accuracy: {round(accuracy, 2)}')
```
``````

- **Classification multiclasse**

La fonction **softmax** est couramment utilisée dans la couche de sortie car elle **normalise les sorties** pour chaque classe **entre 0 et 1** et garantit que la **somme de toutes les probabilités** de sortie est **égale à 1**.

``````{admonition} Bibliothèque Python 🐍
Voici un exemple très simple de **classification multiclasse** avec la fonction **softmax** pour la couche de sortie en utilisant la bibliothèque Keras : 

```python
from keras.models import Sequential
from keras.layers import Dense
import numpy as np

np.random.seed(0)
X = np.random.rand(100, 10)  # 100 échantillons, 10 caractéristiques
Y = np.random.randint(3, size=(100, 1))  # 3 classes, 100 échantillons

# Création du modèle
model = Sequential()
model.add(Dense(64, input_dim=10, activation='relu'))  # Couche cachée
model.add(Dense(3, activation='softmax'))  # Couche de sortie avec softmax

# Compilation du modèle
model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

# Entraînement du modèle
model.fit(X, Y, epochs=100, batch_size=10)

# Évaluation du modèle
_, accuracy = model.evaluate(X, Y)
print(f'Accuracy: {round(accuracy, 2)}')
```
``````

- **Régression**

La fonction d'activation **linéaire** (ou pas de fonction d'activation) est souvent utilisée dans la couche de sortie permettant au modèle de prédire des **valeurs continues**.

``````{admonition} Bibliothèque Python 🐍
Voici un exemple très simple de **régression** avec la fonction **linéaire** pour la couche de sortie en utilisant la bibliothèque Keras : 

```python
from keras.models import Sequential
from keras.layers import Dense
import numpy as np

np.random.seed(0)
X = np.random.rand(100, 10)  # 100 échantillons, 10 caractéristiques
Y = np.random.rand(100, 1)  # 100 valeurs cibles continues

# Création du modèle
model = Sequential()
model.add(Dense(64, input_dim=10, activation='relu'))  # Couche cachée
model.add(Dense(1, activation='linear'))  # Couche de sortie avec fonction d'activation linéaire

# Compilation du modèle
model.compile(loss='mean_squared_error', optimizer='adam')

# Entraînement du modèle
model.fit(X, Y, epochs=100, batch_size=10)

# Évaluation du modèle
loss = model.evaluate(X, Y)
print(f'MSE : {round(loss, 2)}')
```
``````

Pour les **couches de neurones cachées** (hidden layers), le choix de la fonction d'activation est un **compromis** entre différents critères :

- Risque de saturation
- Temps de calculs
- Problème de disparition du gradient
- Problème de neurones morts
- ...

Ces critères sont détaillés dans la partie suivante.

### Différentes fonctions d'activation

#### Pour la couche de neurones de sortie

- **Sigmoïde** : qui donne une sortie entre 0 et 1, utile pour modéliser des probabilités.

$\sigma(x) = \frac{1}{1 + e^{-x}}$

<img src="./_static/sigmoide.png" alt="Fonction sigmoide"/>

- Softmax

$\text{Softmax}(x_i) = \frac{e^{x_i}}{\sum_{j} e^{x_j}}$

#### Pour les couches de neurones cachées

- **Tanh** : la tangente hyperbolique est similaire à la sigmoïde, mais donne une sortie entre -1 et 1, ce qui la rend centrée sur zéro.

$\tanh(x) = \frac{e^{x} - e^{-x}}{e^{x} + e^{-x}}$

<img src="./_static/tanh.png" alt="Fonction tangente hyperbolique"/>


- **ReLU** : qui fournit une sortie positive directe des entrées positives, facilitant des calculs rapides et efficaces.

$\text{ReLU}(x) = \max(0, x)$

ou de manière équivalente :

$\text{ReLU}(x) = 
\begin{cases} 
x & \text{si } x > 0 \\
0 & \text{si } x \leq 0 
\end{cases}$

<img src="./_static/relu.png" alt="Fonction relu"/>

- Leaky ReLU (parametric et randomized)

$\text{LeakyReLU}(x) = \max(\alpha x, x)$

ou de manière équivalente :

$\text{LeakyReLU}(x) = \begin{cases} 
x & \text{si } x > 0 \\
\alpha x & \text{sinon}
\end{cases}$

Avec :

- $\alpha$ petit coefficient positif (souvent 0.01) qui fournit une pente faible pour les valeurs négatives de $x$

<img src="./_static/leakyrelu.png" alt="Fonction leakyrelu"/>

- ELU

$ELU(x) = \begin{cases} 
x & \text{si } x > 0 \\
\alpha \cdot (\exp(x) - 1) & \text{sinon}
\end{cases}$

<img src="./_static/elu.png" alt="Fonction elu"/>

- SELU

$SELU(x) = \lambda \begin{cases} 
x & \text{si } x > 0 \\
\alpha \cdot (\exp(x) - 1) & \text{sinon}
\end{cases}$


- SiLU ou Swish

Sigmoid Linear Unit

$SiLU(x) = x \cdot \sigma(x) = x \cdot \frac{1}{1 + e^{-x}}$


- Softplus

$\text{Softplus}(x) = \ln(1 + e^x)$


- Softsign

$\text{Softsign}(x) = \frac{x}{1 + |x|}$

## Fonctions de perte

```{note}
Dans le contexte de l'apprentissage automatique, les termes "loss function" (fonction de perte) et "objective function" (fonction objectif) sont souvent utilisés de manière **interchangeable** mais ils peuvent avoir des **significations légèrement différentes** selon le contexte.

- **Fonction de perte**

Mesure de l'**écart** entre la **prédiction du modèle** et les **valeurs réelles (ou vraies)**. Elle est utilisée pour évaluer à quel point le modèle se trompe dans ses prédictions.

- **Fonction objectif**

**Fonction plus générale** utilisée pour l'optimisation dans le processus d'apprentissage. Elle détermine l'**objectif global** du processus d'optimisation. La fonction objectif considère l'ensemble du processus d'entraînement et guide l'optimisation non seulement sur la base de l'erreur de prédiction mais aussi en prenant en compte d'**autres facteurs** qui peuvent affecter la performance globale du modèle (la complexité du modèle ou la prévention du surapprentissage).
```

### Pour la régression

Les fonctions de perte pour la régression mesurent l'écart entre les valeurs prédites et les valeurs cibles continues. La plus courante est la mean squared error (MSE).

```python
from keras.losses import MeanSquaredError

loss = MeanSquaredError()
```

### Pour la classification

Les fonctions de perte en classification mesurent l'écart entre les étiquettes prédites et les vraies étiquettes catégorielles.

#### Pertes probabilistes

Les pertes probabilistes comme la **categorical cross-entropy** sont utilisées lorsque les sorties du modèle sont des probabilités.

```python
from keras.losses import CategoricalCrossentropy

loss = CategoricalCrossentropy()
```

#### Pertes de marge maximale (Hinge losses)

Les hinge losses sont utilisées pour les modèles à marge maximale, comme les machines à vecteurs de support (SVM), où on cherche à maximiser la distance entre les classes.

```python
from keras.losses import Hinge

loss = Hinge()
```

## Mise à jour des paramètres du modèle

```{sidebar} 📖 Papier de recherche
Titre : <a href="https://arxiv.org/pdf/2007.14166.pdf" target="_blank">A Comparison of Optimization Algorithms for Deep Learning</a>

Autrice : <a href="https://scholar.google.com/citations?user=FPXh-WkAAAAJ&hl=en" target="_blank">Derya Soydaner</a>

Année de publication : 2020
```

Les **temps d'entrainement** pour les grands réseaux de neurones peuvent **très importants**. Il est dont très utile de choisir une **méthode d'optimisation** des paramètres du modèle qui est **efficace**.

### Calculs de dérivées

La **dérivée** a une très grande importance dans les méthodes d'entrainement des réseaux de neurones. Les dérivées peuvent être calculées de **différentes façons**. On prend l'exemple de la fonction $f(x, y) = (3x^2 + y^2)^2$ en cherchant à calculer ses dérivées partielles en $(x,y)=(1,1)$

#### Dérivation symbolique

On utilise les règles de dérivation pour trouver une expression formelle des dérivées.

La dérivée partielle par rapport à x : 

$\frac{\partial f}{\partial x} = 2 \cdot 6x \cdot (3x^2 + y^2)$

La dérivée partielle par rapport à y : 

$\frac{\partial f}{\partial y} = 2 \cdot 2y \cdot (3x^2 + y^2)$

En évaluant ces dérivées en $(x,y)=(1,1)$, on obtient :

$\frac{\partial f}{\partial x}(1, 1) = 48 \quad \frac{\partial f}{\partial y}(1, 1) = 16$

``````{admonition} Bibliothèque Python 🐍
La bibliothèque <a href="https://www.sympy.org/en/index.html" target="_blank">Sympy</a> permet d'effectuer des **calculs symboliques**. Voici le code pour effectuer le calcul des dérivées partielles précédentes (la fonction `display` permet d'afficher correctement les fonctions en <a href="https://en.wikibooks.org/wiki/LaTeX/Mathematics" target="_blank">Latex</a> en sortie des cellules des notebooks Jupyter) :

```python
import sympy as sp
from IPython.display import display

# Définir les symboles
x, y = sp.symbols('x y')

# Définir la fonction
f = (3*x**2 + y**2)**2

# Calculer les dérivées partielles
df_dx = sp.diff(f, x)
df_dy = sp.diff(f, y)

# Évaluer les dérivées partielles en (x, y) = (1, 1)
df_dx_at_1_1 = df_dx.subs({x: 1, y: 1})
df_dy_at_1_1 = df_dy.subs({x: 1, y: 1})

# Afficher les résultats
print("La dérivée partielle par rapport à x :")
display(df_dx)
print(f"Valeur en (1,1) : {df_dx_at_1_1}")
print("La dérivée partielle par rapport à y :")
display(df_dy)
print(f"Valeur en (1,1) : {df_dy_at_1_1}")
```
> La dérivée partielle par rapport à x :
>
> $12 \cdot x \cdot (3x^2 + y^2)$
>
> Valeur en (1,1) : 48
>
> La dérivée partielle par rapport à y :
>
> $4 \cdot y \cdot (3x^2 + y^2)$
>
> Valeur en (1,1) : 16
``````

#### Dérivation numérique

On utilise une **approximation numérique**, comme la <a href="https://fr.wikipedia.org/wiki/Méthode_des_différences_finies" target="_blank">méthode des différences finies</a>. Les approches les plus courantes sont la **différence finie avant** ($\frac{f(1 + h, 1) - f(1, 1)}{h}$), la **différence finie arrière** ($\frac{f(1, 1) - f(1 - h, 1)}{h}$) et la **différence finie centrée** ($\frac{f(1 + h, 1) - f(1 - h, 1)}{2h}$). Pour les calculs, on utilise la **différence finie avant**. Pour h petit ($10^{-5}$), la dérivée partielle par rapport à x vaut :

$\frac{\partial f}{\partial x}(1, 1) \approx \frac{f(1 + h, 1) - f(1, 1)}{h} \approx 48.0$

Pour h petit ($10^{-5}$), la dérivée partielle par rapport à y vaut :

$\frac{\partial f}{\partial y}(1, 1) \approx \frac{f(1, 1 + h) - f(1, 1)}{h} \approx 16.0$

``````{admonition} Bibliothèque Python 🐍
En Python, on peut effectuer ces calculs **simplement** avec la <a href="https://docs.python.org/fr/3/library/index.html" target="_blank">bibliothèque standard</a> :

```python
# Définir la fonction
def f(x, y):
    return (3*x**2 + y**2)**2

# Calculer les gradients de f par rapport à x et y
def derivative_x(f, x, y, h=1e-5):
    return (f(x + h, y) - f(x, y)) / h
def derivative_y(f, x, y, h=1e-5):
    return (f(x, y + h) - f(x, y)) / h

x, y = 1, 1
f_prime_x = derivative_x(f, x, y)
f_prime_y = derivative_y(f, x, y)

# Afficher les résultats
print(f"La dérivée partielle par rapport à x en (1,1) : {round(f_prime_x, 2)}")
print(f"La dérivée partielle par rapport à y en (1,1) : {round(f_prime_y, 2)}")
```

```
La dérivée partielle par rapport à x en (1,1) : 48.0
La dérivée partielle par rapport à y en (1,1) : 16.0
```
``````

#### Dérivation automatique

La méthode utilisée dans les bibliothèques d'apprentissage profond (<a href="https://www.tensorflow.org/?hl=fr" target="_blank">Tensorflow</a>, <a href="https://pytorch.org" target="_blank">Pytorch</a>, <a href="https://jax.readthedocs.io/en/latest/notebooks/quickstart.html" target="_blank">Jax</a>, <a href="https://keras.io" target="_blank">Keras</a>...) est la **dérivation automatique**. Cette méthode ne repose ni sur les méthodes symboliques de calcul des dérivées ni sur les approximations numériques des dérivées (les deux méthodes vues précédemment). Au lieu de cela, elle suit les règles de **dérivation exactes** et **propage les dérivées** à travers les **opérations arithmétiques** et les **fonctions**. Elle s'appuie sur la <a href="https://fr.wikipedia.org/wiki/Théorème_de_dérivation_des_fonctions_composées" target="_blank">règle de dérivation en chaîne</a> (chain rule).

```{admonition} Visualisation de la dérivation en chaîne
La vidéo suivante présente une **représentation graphique** de la dérivation en chaîne :

<div class="container">
    <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/YG15m2VwSjA" title="Visualiser la règle de la chaîne et la règle de produit | Chapitre 4, essence du calcul" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
<a href="https://youtu.be/YG15m2VwSjA" target="_blank">Visualiser la règle de la chaîne et la règle de produit | Chapitre 4, essence du calcul</a>, 3Blue1Brown, 15min 55s, 2017
```

Pour la fonction composée $h(x) = f(g(x))$, cela donne :

$\frac{dh}{dx} = \frac{df}{dg} \times \frac{dg}{dx}$

On peut prendre un exemple concret pour illustrer la règle de dérivation en chaîne :

$g(x) = 2x + 1$

$f(u) = u^2$

$h(x) = f(g(x)) = (2x + 1)^2$

En appliquant la règle de dérivation en chaîne, cela donne :

$\frac{dh}{dx} = \frac{df}{dg} \times \frac{dg}{dx}$

$\frac{df}{dg} = 2u \quad \text{(où \( u = g(x) \))}$

$\frac{dg}{dx} = 2$

$\frac{dh}{dx} = 2(2x + 1) \times 2 = 4(2x + 1)$

Comme dans les parties précédentes, on peut reprendre l'exemple de la fonction $f(x, y) = (3x^2 + y^2)^2$ en cherchant à calculer ses dérivées partielles en $(x,y)=(1,1)$ avec la dérivation automatique.

```python
import numpy as np

class Tensor:
    def __init__(self, value=None, children=None, op=None):
        self.value = value
        self.children = children  # enfants sont les tenseurs impliqués dans l'opération
        self.op = op  # opération à effectuer sur les enfants

    def forward(self):
        """
        Calcule la valeur du tenseur en effectuant l'opération si nécessaire
        """
        if self.children is None:
            return self  # Si pas d'enfants, renvoie le tenseur tel quel

        a, b = self.children
        a_val = a.forward().value
        b_val = b.forward().value if b is not None else None

        # Calcule la valeur en fonction de l'opération
        if b_val is not None:
            self.value = self.op(a_val, b_val)
        else:
            self.value = self.op(a_val)
        return self

    def grad(self, deriv_to):
        """
        Calcule le gradient par rapport à un autre tenseur.
        """
        if self is deriv_to:
            return Tensor(1)  # Gradient de soi-même est 1

        if self.children is None:
            return Tensor(0)  # Pas de dérivée pour une constante

        # Règle de dérivation en chaîne (chain rule)
        a, b = self.children
        if self.op is np.add:
            return a.grad(deriv_to) + b.grad(deriv_to)
        elif self.op is np.multiply:
            return a.grad(deriv_to) * b + a * b.grad(deriv_to)

    def __add__(self, other):
        return Tensor(children=(self, other), op=np.add).forward()

    def __mul__(self, other):
        return Tensor(children=(self, other), op=np.multiply).forward()

# Exemple d'utilisation
x = Tensor(1)
y = Tensor(1)

z = (Tensor(3) * x * x + y * y ) * (Tensor(3) * x * x + y * y)
print("La dérivée partielle par rapport à x en (1,1) : ", z.grad(x).value)
print("La dérivée partielle par rapport à y en (1,1) : ", z.grad(y).value)
```
```
La dérivée partielle par rapport à x en (1,1) : 48.0
La dérivée partielle par rapport à y en (1,1) : 16.0
```


Ce code illustre sur un **exemple simple** le fonctionnement de la dérivation automatique. En **pratique**, ces calculs sont directement **effectués de manière optimisée** par les **bibliothèques d'apprentissage profond**.

``````{admonition} Bibliothèque Python 🐍
On peut effectuer ces calculs de dérivation automatique avec la bibliothèque <a href="https://www.tensorflow.org/?hl=fr" target="_blank">TensorFlow</a> :

```python
import tensorflow as tf

# Utiliser un GradientTape pour enregistrer les opérations effectuées sur les variables
with tf.GradientTape() as tape:
    # Définir les variables x et y
    x = tf.Variable(1.0)
    y = tf.Variable(1.0)
    # Définir la fonction f(x, y)
    f = (3*x**2 + y**2)**2

# Calculer les gradients de f par rapport à x et y
gradients = tape.gradient(f, [x, y])

# Récupérer les valeurs des gradients
grad_x, grad_y = gradients

print("La dérivée partielle par rapport à x en (1,1) : ", grad_x.numpy())
print("La dérivée partielle par rapport à y en (1,1) : ", grad_y.numpy())
```

```
La dérivée partielle par rapport à x en (1,1) : 48.0
La dérivée partielle par rapport à y en (1,1) : 16.0
```

On peut faire la calcul de la même façon avec Pytorch :

```python
import torch

# Définir les variables x et y, en précisant que nous avons besoin de gradients
x = torch.tensor(1.0, requires_grad=True)
y = torch.tensor(1.0, requires_grad=True)

# Définir la fonction f(x, y)
f = (3*x**2 + y**2)**2

# Calculer les gradients de f par rapport à x et y
f.backward()

# Récupérer les valeurs des gradients
grad_x = x.grad
grad_y = y.grad

print("La dérivée partielle par rapport à x en (1,1) : ", grad_x.item())
print("La dérivée partielle par rapport à y en (1,1) : ", grad_y.item())
```

``````


### Descente de gradient

Cette méthode sert à **minimiser une fonction de perte**. L'idée est de trouver les paramètres du réseau de neurones qui minimisent cette fonction. La descente de gradient fait cela en calculant le **gradient de la fonction de perte** par rapport aux paramètres et en mettant à jour ces paramètres dans la **direction opposée du gradient**.

**Formule :**

$\theta_{t+1} = \theta_t - \eta \cdot \nabla_{\theta} J(\theta)$

Avec :

- $\eta$ le taux d'apprentissage. Il détermine la taille du pas effectué dans la direction du gradient négatif.
    
- $\nabla_{\theta} J(\theta)$ le gradient de la fonction de perte $J(\theta)$ par rapport aux paramètres du modèle $\theta_t$. Il indique la direction dans laquelle la fonction de perte augmente le plus rapidement.

- $\theta_t$ et $\theta_{t+1}$​​ les poids d'un réseau de neurones aux étapes t et t+1. Ces paramètres sont ajustés pour minimiser la fonction de perte.


#### Descente de gradient stochastique 

Avec la <a href="https://fr.wikipedia.org/wiki/Algorithme_du_gradient_stochastique" target="_blank">descente de gradient stochastique</a> (Stochastic Gradient Descent - SGD), les paramètres du modèle sont mis à jour après **chaque exemple d'entraînement**.

Cette méthode conduit à une **convergence rapide** mais **parfois instable**.

#### Descente de gradient par lots 

Avec la **descente de gradient par lots** (Batch Gradient Descent), la mise à jour des paramètres se fait après avoir calculé le gradient sur l'**ensemble complet des données d'entraînement**.

Cette méthode **très efficace** en termes de calcul sur des ensembles de données plus petits mais peut devenir **très lent** et **coûteux en mémoire** sur des ensembles de données plus grands.

#### Descente de gradient par mini-lots

C'est une **approche intermédiaire** entre la SGD et la descente de gradient par lots. Les mises à jour des paramètres se font après avoir calculé le gradient sur un **sous-ensemble (mini-lot)** des données d'entraînement.

Cette méthode est souvent **utilisée dans la pratique** car elle offre un bon équilibre entre **efficacité en termes de mémoire** et de **vitesse de convergence**.

### Momentum

Le momentum est une **amélioration** de la méthode de descente de gradient. Par rapport à la descente de gradient standard, on ajoute une notion de **"mémoire"** à ces mises à jour : le chemin pris pour atteindre le point actuel est pris en compte pour déterminer la prochaine mise à jour. Cela aide à accélérer la **convergence vers le minimum global** et à éviter de rester coincé dans des minima locaux.

**Formule :**

```{sidebar} 📖 Papier de recherche
Titre : <a href="https://www-sciencedirect-com.sid2nomade-2.grenet.fr/science/article/pii/0041555364901375" target="_blank">Some methods of speeding up the convergence of iteration methods</a>

Auteurs : <a href="https://scholar.google.ru/citations?user=Zhlib28AAAAJ&hl=en" target="_blank">Boris Polyak</a>

Année de publication : 1964
```

$m_{t+1} = \beta \cdot m_t + \eta \cdot \nabla_{\theta} J(\theta)$

$\theta_{t+1} = \theta_t - m_{t+1}$

Avec :

- $m_{t}$ et $m_{t+1}$​ la somme pondérée des gradients précédents à l'étape t et t+1

- $\beta$ le paramètre de momentum compris entre 0 et 1. Il contrôle l'influence des gradients précédents dans la mise à jour des poids.

- $\eta$ le taux d'apprentissage. Il détermine la taille du pas effectué dans la direction du gradient négatif.
    
- $\nabla_{\theta} J(\theta)$ le gradient de la fonction de perte $J(\theta)$ par rapport aux paramètres du modèle $\theta_t$. Il indique la direction dans laquelle la fonction de perte augmente le plus rapidement.

- $\theta_t$ et $\theta_{t+1}$​​ les poids d'un réseau de neurones aux étapes t et t+1. Ces paramètres sont ajustés pour minimiser la fonction de perte.

Cette méthode d'optimisation permet une convergence plus rapide et plus stable vers le minimum de la fonction de perte, comparée à la simple descente de gradient.

**Avantages :**

- Amortissement des oscillations
- Limitation du risque des minima locaux

**Inconvénients :**

- Hyperparamètre additionnel
- Risque de passer au delà du minimum global

### Adagrad

AdaGrad est un algorithme d'optimisation qui adapte le taux d'apprentissage pour chaque paramètre **individuellement** en pénalisant de manière plus importante les directions où la somme des gradients cumulés est plus importante.

**Formule :**

$G_{t+1} = G_{t} + \nabla_{\theta} J(\theta) \odot \nabla_{\theta} J(\theta)$


$\theta_{t+1} = \theta_{t} - \frac{\eta}{\sqrt{G_{t+1} + \epsilon}} \cdot \nabla_{\theta} J(\theta_t)$

Avec :

- $\nabla_{\theta} J(\theta)$ le gradient de la fonction de perte $J(\theta)$ par rapport aux paramètres du modèle $\theta_t$. Il indique la direction dans laquelle la fonction de perte augmente le plus rapidement.

- $G_{t}$ et $G_{t+1}$​​ une matrice diagonale (pour simplifier les calculs) où chaque élément de la diagonale i,i est la somme des carrés des gradients passés pour le paramètre i jusqu'à l'étape t et t+1.

- $\odot$ représente la multiplication élément par élément (<a href="https://en.wikipedia.org/wiki/Hadamard_product_(matrices)" target="_blank">produit matriciel de Hadamard</a>).

- $\eta$ le taux d'apprentissage

- $\theta_t$ et $\theta_{t+1}$​​ les poids d'un réseau de neurones aux étapes t et t+1. Ces paramètres sont ajustés pour minimiser la fonction de perte.

- $\epsilon$ un petit nombre ajouté pour la stabilité numérique, empêchant la division par zéro.

### RMSprop

Root Mean Square Propagation (RMSprop) est un algorithme d'optimisation qui utilise une **moyenne mobile exponentielle du carré des gradients** pour **normaliser** le gradient lui-même. Cela aide à **éliminer les oscillations** lors de la descente de gradient.

**Formule :**

```{sidebar} 🎓 Origine de la méthode RMSprop
La méthode RMSprop n'a pas été publiée dans un papier de recherche. Elle apparaît dans les <a href="https://www.cs.toronto.edu/~tijmen/csc321/slides/lecture_slides_lec6.pdf" target="_blank">supports de cours</a> de ses auteurs <a href="https://fr.wikipedia.org/wiki/Geoffrey_Hinton" target="_blank">Geoffrey Hinton</a> et <a href="https://scholar.google.com/citations?user=YGGcq5EAAAAJ&hl=en" target="_blank">Tijmen Tieleman</a>.

<div class="container">
    <iframe class="responsive-iframe" width="300" height="150"  src="https://www.youtube-nocookie.com/embed/defQQqkXEfE?list=PLoRl3Ht4JOcdU872GhiYWf6jwrk_SNhz9" title="Lecture 6.5 — Rmsprop: normalize the gradient [Neural Networks for Machine Learning]" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
```

$v_{t+1} = \gamma \cdot v_t + (1 - \gamma) \cdot \nabla_{\theta} J(\theta)^2$

$\theta_{t+1} = \theta_t - \frac{\eta}{\sqrt{v_{t+1} + \epsilon}} \cdot \nabla_{\theta} J(\theta)$

Avec :

- $\nabla_{\theta} J(\theta)$ le gradient de la fonction de perte $J(\theta)$ par rapport aux paramètres du modèle $\theta_t$. Il indique la direction dans laquelle la fonction de perte augmente le plus rapidement.

- $\theta_t$ et $\theta_{t+1}$​​ les poids d'un réseau de neurones aux étapes t et t+1. Ces paramètres sont ajustés pour minimiser la fonction de perte.

- $v_t$ et $v_{t+1}$​​ les moyennes mobiles exponentielles du carré des gradients aux étapes t et t+1.

- $\eta$ le taux d'apprentissage

- $\gamma$ le facteur de décroissance exponentielle

- $\epsilon$ un petit nombre ajouté pour la stabilité numérique, empêchant la division par zéro.

### Adam

La méthode Adaptative moment estimation (Adam) est un algorithme d'optimisation qui combine les avantages de deux autres méthodes : **momentum** et **RMSprop**. Cet optimiseur est **un des plus couramment utilisés** dans les bibliothèques d'apprentissage profond.

**Formule :**

```{sidebar} 📖 Papier de recherche
Titre : <a href="https://arxiv.org/pdf/1412.6980.pdf" target="_blank">Adam: A Method for Stochastic Optimization</a>

Auteurs : <a href="https://scholar.google.nl/citations?user=yyIoQu4AAAAJ&hl=en" target="_blank">Diederik P. Kingma</a> - <a href="https://scholar.google.ca/citations?user=ymzxRhAAAAAJ&hl=en" target="_blank">Jimmy Ba</a>

Année de publication : 2014
```

La méthode Adam se compose de **trois parties** :

1.  **Calcul du premier moment (partie momentum)**

$m_{t+1} = \beta_1 \cdot m_t + (1 - \beta_1) \cdot \nabla_{\theta} J(\theta)$

$\hat{m}_{t+1} = \frac{m_{t+1}}{1 - \beta_1^{t+1}}$ (corrections de l'effet du biais initial)

Avec :

- $\nabla_{\theta} J(\theta)$ le gradient de la fonction de perte $J(\theta)$ par rapport aux paramètres du modèle $\theta_t$. Il indique la direction dans laquelle la fonction de perte augmente le plus rapidement.

- $\beta_1$​ facteur de décroissance exponentielle du premier moment et $\beta_1^{t+1}$ la puissance $t+1$ de cet hyperparamètre

- $m_t$ et $m_{t+1}$ estimation du premier moment des gradients​ aux étapes $t$ et $t+1$

- $\hat{m}_{t+1}$ estimation du premier moment des gradients corrigée pour le biais initial à l'étape $t+1$

2.  **Calcul du second moment (partie RMSprop)**

$v_{t+1} = \beta_2 \cdot v_t + (1 - \beta_2) \cdot \nabla_{\theta} J(\theta)^2$

$\hat{v}_{t+1} = \frac{v_{t+1}}{1 - \beta_2^{t+1}}$ (corrections de l'effet du biais initial)

Avec :

- $\nabla_{\theta} J(\theta)$ le gradient de la fonction de perte $J(\theta)$ par rapport aux paramètres du modèle $\theta_t$. Il indique la direction dans laquelle la fonction de perte augmente le plus rapidement.

- $\beta_2$​ facteur de décroissance exponentielle du second moment et $\beta_2^{t+1}$ la puissance $t+1$ de cet hyperparamètre

- $v_t$ et $v_{t+1}$ estimation du second moment des gradients​ aux étapes $t$ et $t+1$

- $\hat{v}_{t+1}$ estimation du second moment des gradients corrigée pour le biais initial à l'étape $t+1$

3.  **Calcul de la mise à jour des poids**

$\theta_{t+1} = \theta_t - \frac{\eta}{\sqrt{\hat{v}_{t+1}} + \epsilon} \cdot \hat{m}_{t+1}$

Avec :

- $\theta_t$ et $\theta_{t+1}$​​ les poids d'un réseau de neurones aux étapes t et t+1. Ces paramètres sont ajustés pour minimiser la fonction de perte.

- $\eta$ le taux d'apprentissage

- $\epsilon$ un petit nombre ajouté pour la stabilité numérique, empêchant la division par zéro.

- $\hat{v}_{t+1}$ estimation du second moment des gradients corrigée pour le biais initial à l'étape $t+1$

- $\hat{m}_{t+1}$ estimation du premier moment des gradients corrigée pour le biais initial à l'étape $t+1$

## Métriques d'évaluation en apprentissage profond

Dans le domaine de l'apprentissage profond, nous appliquons un large éventail de métriques d'évaluation, similaires à celles utilisées en apprentissage automatique traditionnel. Ces métriques englobent des indicateurs de régression tels que la Racine de l'Erreur Quadratique Moyenne (<a href="https://fr.wikipedia.org/wiki/Racine_de_l%27erreur_quadratique_moyenne" target="_blank">RMSE</a>), l'Erreur Absolue Moyenne (<a href="https://en.wikipedia.org/wiki/Mean_absolute_error" target="_blank">MAE</a>) et le Coefficient de Détermination (<a href="https://fr.wikipedia.org/wiki/Coefficient_de_détermination" target="_blank">R2</a>) ainsi qu'une analyse détaillée des résidus. Pour la classification, nous utilisons des mesures telles que l'exactitude, la précision, le rappel, le score F (F-score), l'aire sous la courbe ROC (AUC-ROC) et la matrice de confusion.

Cependant, les réseaux de neurones sont utilisés dans des domaines tels que la vision par ordinateur et l'analyse du langage naturel où des métriques dédiées sont souvent préférées pour mieux capturer la performance du modèle. Par exemple, en vision par ordinateur, des indices comme la précision moyenne (Mean Average Precision, mAP) pour les tâches de détection d'objets ou le score Intersection sur Union (IoU) pour la segmentation d'images sont fréquemment employés. De même, en traitement du langage naturel, des métriques comme le BLEU score pour la traduction automatique ou le ROUGE score pour le résumé automatique permettent d'évaluer la qualité des textes générés par les modèles. Ces métriques spécifiques sont détaillées dans les prochaines parties.

## Les principales bibliothèques de réseaux de neurones en Python

### TensorFlow

<a href="https://www.tensorflow.org/?hl=fr" target="_blank">TensorFlow</a> est une bibliothèque très populaire, développée par Google. Elle permet de créer des modèles complexes et est largement utilisée en production, notamment pour le déploiement à grande échelle. Elle supporte le calcul distribué et offre une interface flexible.

### Keras

<a href="https://keras.io/" target="_blank">Keras</a> est une API haut niveau qui s'intègre avec TensorFlow. Elle est simple d'utilisation et permet de construire rapidement des modèles de réseaux de neurones sans entrer dans les détails techniques de TensorFlow.

### PyTorch

<a href="https://pytorch.org/" target="_blank">PyTorch</a>, développé par Facebook, est apprécié pour sa flexibilité et sa facilité d'utilisation, notamment en recherche. Il permet un développement dynamique, où le modèle peut être modifié à la volée, et est aussi très utilisé pour les réseaux de neurones complexes.

### JAX

<a href="https://jax.readthedocs.io/en/latest/quickstart.html" target="_blank">JAX</a> est une bibliothèque récente de Google qui met l'accent sur le calcul différentiel automatique et les performances rapides sur GPU/TPU. Elle est de plus en plus utilisée pour la recherche grâce à sa flexibilité et ses capacités d'optimisation.

## Déploiement du modèle

L'étape de **déploiement du modèle** consiste à intégrer le modèle de machine learning entraîné dans un **environnement de production** où il sera utilisé pour **faire des prédictions** (étape d'inférence). Cela implique de mettre le modèle à disposition des **utilisateurs finaux** via une API ou un service web.

### Formats d'enregistrement des modèles

Les **élèments enregistrés** varient en fonction du **format spécifique** mais voici les **principaux** :

- **Architecture du modèle**

Elle comprend la structure du modèle avec le **nombre** et le **type de couches de neurones**, le **nombre de neurones par couche**, les **fonctions d'activation**...

- **Paramètres du modèle**

Ils correspondent aux **valeurs des poids** et des **biais**. Ils comprennent aussi les **hyperparamètres** comme le **taux d'apprentissage**, la **taille des lots**, l'**optimiseur**...

- **Fonctions auxiliaires**

Elles comprennent les fonctions de **pré-traitement** (normalisation, standardisation, tokenisation...) et de **post-traitement** (récupération des étiquettes, tokenisation inverse...). Elles incluent aussi les **fonctions de perte**, les **métriques**...

#### Formats spécifiques à une bibliothèque

- **Fichiers <a href="https://docs.python.org/3/library/pickle.html" target="_blank">Pickle</a> et <a href="https://joblib.readthedocs.io/en/stable/" target="_blank">Joblib</a> (pour la bibliothèque <a href="https://scikit-learn.org/stable/" target="_blank">Scikit-Learn</a>)**

``````{admonition} ⚠️ **RISQUE DE CYBERSECURITE** ⚠️

Utilisez **UNIQUEMENT** des fichiers Pickle ou Joblib dont vous **connaissez la source**. Il est très simple d'intéger un virus informatique qui sera **directement exécuté** à la désérialisation du fichier.

Il existe des systèmes d'**empreintes numériques uniques** pour valider que le fichier Pickle ou Joblib n'a **pas été altéré** entre la source de confiance et vous.

Voici un **exemple simple en Python** pour générer cette **empreinte numérique** (⚠️ **en entreprise**, prenez contact avec le **service responsable de la cybersécurité** pour connaître la politique de l'entreprise) :

```python
import hashlib

def generate_file_hash(filename):
    sha256_hash = hashlib.sha256()
    with open(filename, "rb") as f:
        # Lire et mettre à jour le hachage en blocs de 4K
        for byte_block in iter(lambda: f.read(4096), b""):
            sha256_hash.update(byte_block)
    return sha256_hash.hexdigest()

# Exemple d'utilisation avec un fichier modèle
model_filename = 'model.pkl'  # Ou 'model.joblib'
model_hash = generate_file_hash(model_filename)

# Sauvegardez model_hash de manière sécurisée pour une vérification future
```

Voici le code pour la **vérification de la non-altération** du fichier :

```python
# Le hachage du modèle est sauvegardé dans `saved_hash`

def validate_file_integrity(filename, saved_hash):
    current_hash = generate_file_hash(filename)
    return current_hash == saved_hash

# Vérifiez l'intégrité avant de charger le fichier
if validate_file_integrity(model_filename, saved_hash):
    print("Le fichier est intègre")
else:
    print("Le fichier a été altéré ou corrompu.")
```

``````

Les **exemples de code** présentés sont **très simples** et ne contiennent que le modèle d'apprentissage automatique. Pour des **vrais projets**, il est possible de **sérialiser l'intégralité des pipelines** scikit-learn avec les **étapes de pré et de post-traitement**. 

La bibliothèque <a href="https://docs.python.org/fr/3/library/pickle.html" target="_blank">Pickle</a> fait partie de la <a href="https://docs.python.org/fr/3/library/index.html" target="_blank">bibliothèque standard de Python</a>, il n'y a donc **pas d'installation de paquets** à faire. Pour l'**entrainement** et la **sérialisation** au format pickle :

```python
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
import pickle

# Chargement des données
iris = load_iris()
X_train, X_test, y_train, y_test = train_test_split(iris.data, iris.target, test_size=0.2, random_state=42)

# Entraînement du modèle
model = DecisionTreeClassifier()
model.fit(X_train, y_train)

# Sérialisation du modèle
with open('model.pkl', 'wb') as file:
    pickle.dump(model, file)
```

Pour la **désérialisation** au format pickle et l'**inférence** :

```python
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
import pickle

# Chargement des données
iris = load_iris()
X_train, X_test, y_train, y_test = train_test_split(iris.data, iris.target, test_size=0.2, random_state=42)

# Désérialisation du modèle
with open('model.pkl', 'rb') as file:
    loaded_model = pickle.load(file)

# Utilisation du modèle chargé pour faire des prédictions
predictions = loaded_model.predict(X_test)
print(predictions)
```
```{admonition} 🏴‍☠️ Illustration du risque de cybersécurité
Pour voir qu'il est **très simple d'exécuter du code arbitraire** à l'intérieur d'un fichier pickle, vous pouvez télécharger et charger ce fichier Pickle à la place du modèle d'arbre de décision : <a href="./_static/malicious_model.pkl">🔽</a>
```
___
La bibliothèque externe Joblib est **optimisée pour sauvegarder** et **charger des objets Python** contenant de **grandes quantités de données numériques** (comme des tableaux Numpy) de manière **plus efficace et rapide** que Pickle.

```{admonition} ⚠️ **RISQUE DE CYBERSECURITE** ⚠️
Comme la bibliothèque Joblib **s'appuie sur Pickle**, il y a les **mêmes risques de cybersécurité** à utiliser des fichiers Joblib de source inconnue.
```

Pour l'**entrainement** et la **sérialisation** avec le format Joblib : 

```python
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from joblib import load

# Chargement des données
iris = load_iris()
X_train, X_test, y_train, y_test = train_test_split(iris.data, iris.target, test_size=0.2, random_state=42)

# Entraînement du modèle
model = DecisionTreeClassifier()
model.fit(X_train, y_train)

# Sérialisation du modèle avec joblib
dump(model, 'model.joblib')
```

Pour la **désérialisation** avec le format Joblib et l'**inférence** : 

```python
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from joblib import load

# Chargement des données
iris = load_iris()
X_train, X_test, y_train, y_test = train_test_split(iris.data, iris.target, test_size=0.2, random_state=42)

# Désérialisation du modèle avec joblib
loaded_model = load('model.joblib')

# Utilisation du modèle chargé pour faire des prédictions
predictions = loaded_model.predict(X_test)
print(predictions)
```
___

Une **nouvelle bibliothèque** Python nommée <a href="https://skops.readthedocs.io/en/stable/" target="_blank">skops</a> a été créée pour **résoudre** entre autres les **problèmes de cybersécurité** de Pickle et Joblib. La conférence de <a href="https://www.euroscipy.org" target="_blank">EuroSciPy</a> 2023 présente cette bibliothèque :

<div class="iframe-wrapper">
    <div class="container">
        <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube.com/embed/kfEaIPAYcLM" title="EuroSciPy 2023 -  Let’s exploit pickle, and `skops` to the rescue!" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
</div>

<a href="https://youtu.be/kfEaIPAYcLM" target="_blank">EuroSciPy 2023 - Let’s exploit pickle, and skops to the rescue!</a>, EuroSciPy, 23min 23s, 2023

___

- **Fichiers <a href="https://www.tensorflow.org/guide/saved_model?hl=fr" target="_blank">SavedModel</a> et <a href="https://www.tensorflow.org/guide/keras/serialization_and_saving" target="_blank">Keras V3</a> (pour les bibliothèques TensorFlow et Keras)**

Le fichier SavedModel contient entre autres la **structure du graphe de calcul**, les **poids des paramètres** du modèle et les **signatures** définissant les **entrées** et les **sorties**.

Le format SavedModel peut être **servi de différentes manières** comme par exemple <a href="https://www.tensorflow.org/tfx/guide/serving" target="_blank">TensorFlow Serving</a>, <a href="https://www.tensorflow.org/lite" target="_blank">TensorFlow Lite</a>, <a href="https://www.tensorflow.org/js" target="_blank">TensorFlow.js</a>...

Pour l'**entrainement** et la **sérialisation** du modèle au format SavedModel en Keras avec Tensorflow :

```python
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.datasets import mnist
from tensorflow.keras.utils import to_categorical

# Chargement et préparation des données
(train_images, train_labels), (test_images, test_labels) = mnist.load_data()
train_images = train_images.reshape((60000, 28 * 28)).astype('float32') / 255
train_labels = to_categorical(train_labels)

# Construction du modèle
model = Sequential([
    Dense(512, activation='relu', input_shape=(28 * 28,)),
    Dense(10, activation='softmax')
])

# Compilation du modèle
model.compile(optimizer='rmsprop',
              loss='categorical_crossentropy',
              metrics=['accuracy'])

# Entraînement du modèle
model.fit(train_images, train_labels, epochs=5, batch_size=128)

# Sauvegarde du modèle au format SavedModel
model.save('modele_mnist')
```

Pour la désérialisation du modèle au format SavedModel dans Keras avec Tensorflow :

```python
import tensorflow as tf
from tensorflow.keras.datasets import mnist
import matplotlib.pyplot as plt

(train_images, train_labels), (test_images, test_labels) = mnist.load_data()
test_images = test_images.reshape((10000, 28 * 28)).astype('float32') / 255

# Chargement du modèle sauvegardé
loaded_model = tf.keras.models.load_model('modele_mnist')

# Préparation des données d'entrée pour l'inférence
test_sample = test_images[0:1]

# Inférence avec le modèle chargé
predictions = loaded_model.predict(test_sample)
print(predictions)

# Conversion des probabilités de classe en étiquettes de classe
predicted_class = tf.argmax(predictions, axis=1)
print(predicted_class.numpy())

# Affichage de l'image de test
plt.imshow(test_sample.reshape((28, 28)), cmap='gray')
plt.axis('off')
plt.show()
```

- **Fichiers TorchScript (pour la bibliothèque PyTorch)**

Avec la bibliothèque Pytorch, on peut soit enregistrer uniquement les poids du modèle grâce à la propiété <a href="https://pytorch.org/tutorials/recipes/recipes/saving_and_loading_models_for_inference.html" target="_blank">state_dict</a> du modèle soit l'architecture et les poids du modèle.

Entrainement et sérialisation avec Pytorch :

```python
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor

# Chargement et préparation des données
train_data = datasets.MNIST(
    root="data",
    train=True,
    download=True,
    transform=ToTensor(),
)

train_loader = DataLoader(train_data, batch_size=128, shuffle=True)

# Construction du modèle
class SimpleNN(nn.Module):
    def __init__(self):
        super(SimpleNN, self).__init__()
        self.flatten = nn.Flatten()
        self.dense_layers = nn.Sequential(
            nn.Linear(28*28, 512),
            nn.ReLU(),
            nn.Linear(512, 10),
            nn.Softmax(dim=1)
        )

    def forward(self, input_data):
        x = self.flatten(input_data)
        logits = self.dense_layers(x)
        return logits

model = SimpleNN()

# Compilation du modèle
loss_fn = nn.CrossEntropyLoss()
optimizer = optim.RMSprop(model.parameters())

# Entraînement du modèle
def train(dataloader, model, loss_fn, optimizer):
    size = len(dataloader.dataset)
    model.train()
    for batch, (X, y) in enumerate(dataloader):
        pred = model(X)
        loss = loss_fn(pred, y)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if batch % 100 == 0:
            loss, current = loss.item(), batch * len(X)
            print(f"Fonction de perte : {loss:>7f}  [{current:>5d}/{size:>5d}]")

epochs = 5
for t in range(epochs):
    print(f"Epoque {t+1}\n-------------------------------")
    train(train_loader, model, loss_fn, optimizer)
print("Fin de l'entrainement !")

# Sauvegarde du modèle
torch.save(model.state_dict(), "modele_mnist_pytorch.pth")
```

#### Formats interopérables

Le format ONNX (Open Neural Network Exchange) permet de transférer des modèles entre différentes bibliothèques comme TensorFlow, PyTorch... Le format ONNX peut aussi servir de pont entre le monde de la recherche et de la production {cite}`someki2022espnetonnx`.
