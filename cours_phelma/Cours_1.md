<style>

   .iframe-wrapper {
  max-width: 60%;
  margin: auto; /* pour centrer le conteneur */
}

    /* Pour la gestion des iframes */
 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 📚 Cours : les bonnes pratiques pour démarrer un projet d'intelligence artificielle

## Définition de l'intelligence artificielle

L'**intelligence artificielle (IA)** est un domaine de l'informatique qui vise à créer des systèmes capables de réaliser des tâches qui nécessiteraient normalement l'intelligence humaine. Il s'agit d'un **concept global** regroupant toute forme de calcul ou système **simulant l'intelligence humaine**.

L'**apprentissage automatique** ou **machine learning** en anglais est un **sous-ensemble de l'intelligence artificielle**. Il fait référence aux algorithmes capables d'**apprendre** et de s'**adapter** en analysant des **données**. L'**apprentissage profond** ou **deep learning** en anglais est une branche plus spécifique de l'apprentissage automatique qui utilise des **réseaux neuronaux profonds** pour analyser des **ensembles de données complexes**.

Au sein de ces technologies, il existe **différents types d'apprentissage** :
- L'**apprentissage supervisé** implique des **données étiquetées** pour guider l'algorithme. Il existe différents types d'apprentissage supervisé : la **classification** (si les étiquettes sont des données discrêtes), la **régression** (si les étiquettes sont des données continues), la **détection d'anomalies** et la prédiction de **séries temporelles**.
- L'**apprentissage non supervisé** en revanche permet à l'algorithme de découvrir des **motifs (patterns)** dans des données non étiquetées. Les principaux types d'apprentissage non supervisé sont : la réduction de dimensions, le regroupement (clustering), la modèlisation de distributions et les cartes auto-organisatrices (self organizing maps).
- L'**apprentissage par renforcement** implique un processus où l'algorithme apprend par **essais** et **erreurs**.

## Histoire de l'intelligence artificielle

<div class="iframe-wrapper">
    <div class="container">
        <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/JS12eb1cTLE" title="deeplearning.ai's Heroes of Deep Learning: Yann LeCun" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
</div>

<a href="https://youtu.be/JS12eb1cTLE" target="_blank">deeplearning.ai's Heroes of Deep Learning: Yann LeCun</a>, DeepLearningAI, 27min 48s, 05/04/18

___

<div class="iframe-wrapper">
    <div class="container">
        <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/FwFduRA_L6Q" title="Convolutional Network Demo from 1989" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
</div>

<a href="https://youtu.be/FwFduRA_L6Q" target="_blank">Convolutional Network Demo from 1989</a>, Yann LeCun, 1min 01s, 02/06/2014

___

## Pertinence des usages

La pertinence de l'intelligence artificielle dans les projets d'entreprise repose sur une **définition claire du problème** et la mise en place de **métriques métiers précises**. Avant de lancer un projet d'IA, il est essentiel de définir des **objectifs métiers concrets** et de les **transposer en métriques** spécifiques à la science des données (accuracy, précision...).

```{admonition} Exemple 🏭
Pour **illustrer** la notion de métriques métiers et de science des données, on peut prendre l'exemple de la **maintenance prédictive** dans l'industrie. Un exemple de **métrique métier** pourrait être le **temps d'arrêt annuel de la machine** avec pour objectif de le réduire de 20% par exemple. Un exemple de **métrique de science des données** est la proportion du nombre de pannes détectées par le modèle en se basant sur des **données historiques de fonctionnement** de la machine (température, pression, vibrations...).
```

Les **données** jouent un **rôle central** non seulement en **quantité** mais aussi en **qualité** et **pertinence** par rapport aux **objectifs du projet**. De nombreux **échecs de projets d'IA** peuvent être attribués à un **manque de données suffisantes ou pertinentes** mais aussi à des **objectifs mal définis**.

## Choix des modèles

Après la **collecte** et le **traitement des données**, une étape cruciale est le **choix des modèles** d'apprentissage automatique à entraîner. L'**objectif principal** est de sélectionner le **modèle le plus simple** qui réponde efficacement aux **exigences spécifiques du projet**. Opter pour un modèle simplifié offre **plusieurs avantages significatifs** :

- une meilleure **compréhension** des résultats (interprétabilité des modèles)
- une **réduction du temps** nécessaire à l'entraînement
- des **coûts moindres** tant sur le plan **environnemental** que **financier** pour l'entraînement et l'exécution
- une **maintenance** et **des mises à jour facilitées**
- des **réentraînements plus aisés**
- ...

```{admonition} En Python 🐍
La bibliothèque <a href="https://scikit-learn.org/stable/" target="_blank">scikit-learn</a> a créé un schéma pour **aider au choix du modèle** le plus pertinent suivant les données d'entrée :

<a href="./_static/ml_map.png" target="_blank">
    <img src="./_static/ml_map.png" alt="ML map scikit-learn" height="400px"/>
</a>
``````

### Modèles naïfs

Ces modèles jouent un rôle fondamental dans l'établissement d'**une base de référence** (comme pour la calibration d'une balance) : ils servent de **point zéro** de la connaissance extraite des données. Ils sont essentiels pour :

- **Tester et valider les différentes étapes** de l'entrainement du modèle du point de vue du code
- **Tester et valider le code d'évaluation** du modèle
- **Confirmer les hypothèses initiales** concernant les résultats attendus
- **Détecter des erreurs** dans le code d'entrainement des modèles ou dans les données

Bien que ces modèles basiques **ne soient pas directement utilisés** dans la solution finale, il est crucial de réfléchir aux **indicateurs de performance attendus** pour un modèle de ce type **dès les premières étapes** de la conception du projet.

``````{admonition} Bibliothèque Python 🐍
La bibliothèque <a href="https://scikit-learn.org/stable/" target="_blank">Scikit-Learn</a> possède une classe pour créer un modèle de classification naïf (<a href="https://scikit-learn.org/stable/modules/generated/sklearn.dummy.DummyClassifier.html" target="_blank">DummyClassifier</a>) et une pour un modèle de régression naïf (<a href="https://scikit-learn.org/stable/modules/generated/sklearn.dummy.DummyRegressor.html" target="_blank">DummyRegressor</a>). Ces modèles **n'apprennent rien** des caractéristiques (features). Par contre, ces modèles peuvent **utiliser des informations statistiques** sur les étiquettes (labels) : c'est le cas en **régression** avec les stratégies **"mean", "median" et "quantile"** ou en **classification** avec les stratégies **"most_frequent", "prior" et "stratified"**. Les autres stratégies **n'utilisent aucune information** sur les données : **"uniform" et "constant"** pour la **classification** et **"constant"** pour la **régression**. Voici un exemple pour un modèle de **régression** avec une stratégie **"mean"** :

```python
import numpy as np
from sklearn.dummy import DummyRegressor

X = np.array([1.0, 2.0, 3.0, 4.0])
y = np.array([2.0, 3.0, 5.0, 10.0])
dummy_regr = DummyRegressor(strategy="mean")
dummy_regr.fit(X, y)
print(f"Prédiction : {dummy_regr.predict(X)}")
print(f"Métrique R2 : {dummy_regr.score(X, y)}")
```

```bash
Prédiction : [5. 5. 5. 5.]
Métrique R2 : 0.0
```

L'avantage des ces modèles est qu'ils sont construits de la **même façon que tous les autres modèles** de la bibliothèque <a href="https://scikit-learn.org/stable/" target="_blank">Scikit-Learn</a> (ils héritent de la classe <a href="https://scikit-learn.org/stable/modules/generated/sklearn.base.BaseEstimator.html" target="_blank">BaseEstimator</a>). On peut donc leur appliquer les <a href="https://scikit-learn.org/stable/modules/model_evaluation.html" target="_blank">fonctions d'évaluation</a> des modèles de Scikit-Learn ou les intégrer dans un <a href="https://scikit-learn.org/stable/modules/compose.html" target="_blank">pipeline</a> d'entrainement pour le tester.
``````

### Modèles simples

#### Régression linéaire simple et régularisée

La régression linéaire simple est un modèle de base en apprentissage automatique qui établit une **relation linéaire** entre une **variable dépendante** et une ou plusieurs **variables indépendantes**. Pour **éviter le <a href="https://fr.wikipedia.org/wiki/Surapprentissage" target="_blank">surapprentissage</a>** et **améliorer la robustesse du modèle**, des **techniques de <a href="https://fr.wikipedia.org/wiki/R%C3%A9gularisation_(math%C3%A9matiques)" target="_blank">régularisation</a>** comme <a href="https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Ridge.html" target="_blank">Ridge</a>, <a href="https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Lasso.html" target="_blank">Lasso</a> ou <a href="https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.ElasticNet.html" target="_blank">Elastic Net</a> sont utilisées. Elles ajoutent une pénalité pour **limiter la complexité du modèle**.

#### Régression logistique

La régression logistique utilisée pour les problèmes de **classification binaire**. Elle modélise la probabilité qu'une entrée appartienne à une classe spécifique. Contrairement à la régression linéaire, elle utilise une <a href="https://fr.wikipedia.org/wiki/Fonction_logistique_(Verhulst)" target="_blank">fonction logistique</a> pour produire des valeurs de sortie comprises **entre 0 et 1** interprétables comme des **probabilités**.

#### Perceptron

Le perceptron est l'algorithme **le plus simple** de réseau de neurones. Il s'agit d'un classificateur linéaire qui utilise une fonction d'activation pour décider si un ensemble d'entrées appartient à une classe spécifique ou non. Malgré sa simplicité, il a jeté les **bases des réseaux de neurones** plus complexes. Si la fonction d'activation utilisée pour le perceptron est une fonction **sigmoïd** alors le perceptron est semblable à une **régression logistique** (mais historiquement <a href="https://fr.wikipedia.org/wiki/Frank_Rosenblatt" target="_blank">Frank Rosenblatt</a> a utilisé une fonction d'activation échelon et l'entrainement est aussi effectué différemment).

#### Modèle des k plus proches voisins

Le modèle des k plus proches voisins (KNN) est un algorithme **non paramétrique** utilisé pour la **classification** et la **régression**. Il fonctionne en trouvant les k échantillons les **plus proches** dans l'**espace des caractéristiques** et en faisant des prédictions basées sur la **majorité des voisins** pour la **classification** ou la **moyenne** pour la **régression**.

#### Modèle bayésien naïf

Le modèle bayésien naïf repose sur l'application du <a href="https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_de_Bayes" target="_blank">théorème de Bayes</a> avec une hypothèse de naïveté : on suppose l'indépendance entre les caractéristiques (features). Bien que cette hypothèse soit **rarement vraie en pratique**, ces modèles sont **étonnamment efficaces** et sont largement utilisés dans la classification de textes et le filtrage anti-spam.

### Modèles plus complexes

#### Machine à vecteurs de support (Support Vector Machine - SVM)

La machine à vecteurs de support (SVM) est un **modèle puissant** utilisé principalement pour la classification mais aussi pour la régression. Elle fonctionne en trouvant un **hyperplan** dans un espace multidimensionnel qui **sépare au mieux** les différentes classes. Le SVM est particulièrement efficace dans les **espaces de grande dimension**. Grâce à son **noyau**, il peut de traiter des données **linéairement non séparables** mais la sélection et le paramétrage du noyau peuvent être complexes.

#### Arbre de décision

Les arbres de décision sont des modèles qui représentent des **séries de décisions** sous forme d'**arborescence**. Ils sont utilisés pour la **classification** et la **régression**. Le modèle se construit en divisant l'ensemble de données en **sous-ensembles** basés sur des critères formant ainsi l'arbre. Ils sont **intuitifs** et **faciles à interpréter** mais peuvent devenir **très complexes** et sujets au **surapprentissage**. Les arbres de décision servent également de **base à des modèles plus avancés** comme les forêts aléatoires (Random Forests) et certains algorithmes de boosting.

### Modèles ensemblistes

Les méthodes ensemblistes sont essentielles en apprentissage automatique car elles permettent d'**améliorer la performance** des modèles simples en les **combinant de manière intelligente**. Elles sont particulièrement utiles pour **réduire le surapprentissage** et améliorer la **robustesse des prédictions**.

#### Bootstrap aggregating (Bagging)

Le Bootstrap Aggregating (Bagging) est une méthode qui vise à **améliorer la stabilité** et l'**exactitude** des algorithmes d'apprentissage automatique. Elle fonctionne en créant **plusieurs sous-ensembles de données** à partir de l'ensemble de données d'origine **avec remplacement** et en **entraînant** un modèle **sur chaque sous-ensemble**.

Le modèle de **forêts aléatoires** (Random Forest) est un exemple classique de bagging où de **nombreux arbres de décision** sont entraînés sur différents sous-ensembles de données.

Le **Pasting** est une variante du bagging où les échantillons sont sélectionnés **sans remplacement**.

#### Boosting

Le Boosting est une technique qui construit **séquentiellement** plusieurs modèles où chaque nouveau modèle tente de **corriger les erreurs** du modèle précédent. Cela aboutit à un **modèle final plus fort** et **plus précis**.

AdaBoost (Adaptive Boosting) et Gradient Boosting sont deux méthodes de boosting. AdaBoost **ajuste les poids** des instances mal classées pour les modèles suivants, tandis que le Gradient Boosting **optimise la perte des modèles** précédents en se concentrant sur les erreurs.

#### Stacking

Le Stacking consiste à **combiner plusieurs modèles** de classification ou de régression différents. Le stacking utilise un **modèle final (méta-modèle)** pour effectuer une prédiction finale basée sur les prédictions de tous les modèles sous-jacents.

En stacking, différents modèles comme les arbres de décision, les régressions linéaires ou les SVM peuvent être utilisés comme modèles de base. Un autre modèle, souvent un **modèle simple** comme une régression linéaire, est utilisé comme **méta-modèle** pour intégrer leurs prédictions.

### Modèles de réseaux de neurones profonds

#### Réseaux de neurones denses

Les réseaux de **neurones denses (complétement connectés ou perceptrons multicouches)** sont la forme la plus basique des réseaux de neurones. Chaque neurone dans une couche est **connecté à tous les neurones** de la couche précédente. Ces modèles sont particulièrement efficaces pour apprendre des modèles complexes dans des **données structurées** comme des tableaux de données.

#### Réseaux de neurones de convolution

Les réseaux de neurones de convolution (CNN) sont spécialisés dans le traitement de données avec une **structure matricielle** comme les images. Ils utilisent des **filtres de convolution** pour capturer les **relations spatiales** et les **motifs** dans les données. Les CNN sont largement utilisés dans des applications de **vision par ordinateur** comme la **reconnaissance et la classification d'images**.

#### Réseaux de neurones récurrents

Les réseaux de neurones récurrents (RNN) sont conçus pour gérer des **séquences de données** comme du **texte** ou des **séries temporelles**. Ils se caractérisent par leur capacité à maintenir une **"mémoire"** des entrées précédentes dans la séquence.

#### Transformeurs

Les transformeurs représentent une avancée majeure dans le **traitement du langage naturel (NLP)**. Contrairement aux RNN, les transformeurs traitent les séquences de données **dans leur ensemble**. Ils permettent ainsi un **apprentissage parallèle** et une **meilleure capture des dépendances à longue distance**. Ils sont la base de modèles comme BERT ou GPT qui révolutionnent les tâches de **compréhension** et de **génération de texte**.

### Modèles de fondation

Les <a href="https://fr.wikipedia.org/wiki/Mod%C3%A8le_de_fondation" target="_blank">modèles de fondation</a> (foundation models) sont un concept **relativement nouveau** et en **pleine évolution**. Ces modèles sont des modèles de réseaux de neurones profonds (deep learning) de **grande échelle** et **pré-entraînés sur de vastes ensembles de données**. Ces données sont souvent **hétérogènes** et **proviennent principalement d'Internet** (par web scraping).

```{admonition} Web scraping et crawling 🕷️
Le <a href="https://fr.wikipedia.org/wiki/Robot_d%27indexation" target="_blank">web crawling</a> est une technique utilisée principalement par les **moteurs de recherche** pour **indexer** le contenu du web **à grande échelle** en parcourant systématiquement les sites en **suivant les liens**. À l'inverse, le <a href="https://fr.wikipedia.org/wiki/Web_scraping" target="_blank">web scraping</a> est l'**extraction ciblée de données** à partir de sites web.

⚖️ Ces deux techniques soulèvent des **questions légales et éthiques**, notamment en ce qui concerne le **respect du droit d'auteur**, la **confidentialité des données** et les **conditions d'utilisation des sites web**. L'utilisation de ces techniques doit être **conforme aux lois et réglementations**.

🐍 Différentes bibliothèques Python peuvent être utilisées pour faire du web scraping :

- <a href="https://www.crummy.com/software/BeautifulSoup/bs4/doc/" target="_blank">BeautifulSoup</a>

Utilisée conjointement avec une bibliothèque d'appels réseaux comme <a href="https://requests.readthedocs.io" target="_blank">requests</a> ou <a href="https://docs.python.org/fr/3/library/urllib.request.html" target="_blank">urllib</a>, il s'agit de la solution **la plus simple** pour débuter en web scraping en Python lorsque les pages web ne contiennent pas de <a href="https://fr.wikipedia.org/wiki/JavaScript" target="_blank">Javascript</a>.

- <a href="https://selenium-python.readthedocs.io" target="_blank">Selenium</a>

Si les pages web à scraper contiennent des élèments en **Javascript**, la bibliothèque Selenium sera nécessaire.

- <a href="https://scrapy.org" target="_blank">Scrapy</a>

La bibliothèque Scrapy est **plus complexe** à prendre en main mais elle possède de **nombreuses fonctionnalités**.
```

Ces modèles sont conçus pour être adaptés à une **large gamme de tâches** et applications sans nécessiter un entraînement spécifique pour chaque nouvelle tâche.

Voici les **caractéristiques communes** aux différents modèles de fondation :

- Très grande taille

- Apprentissage non supervisé ou faiblement supervisé

- Polyvalence

- Transférabilité

Voici quelques exemples de modèles de fondation :

- GPT (Generative Pretrained Transformer) pour la génération de texte
- BERT (Bidirectional Encoder Representations from Transformers) pour la compréhension du langage naturel
- DALL-E pour la génération d'images

Ces modèles présentent un certain nombre d'enjeux :

- Coût environnementaux et financiers

- Biais et éthique

- Accessibilité et contrôle

## Réglementation

La **réglementation de l'intelligence artificielle** est essentielle pour assurer son utilisation **éthique** et **sécurisée**. L'Union Européenne a pris des mesures importantes avec le **Règlement Général sur la Protection des Données (RGPD)** qui impose des règles strictes sur la **gestion des données personnelles**. Cette réglementation a un impact sur l'intelligence artificielle car les modèles d'apprentissage automatique **s'appuient sur des données** pour s'entrainer. En plus du RGPD, l'Union Européenne développe des réglementations spécifiques à l'intelligence artificielle (AI act) visant à garantir la **fiabilité**, l'**équité**, la **transparence** et la **responsabilité** dans les applications d'intelligence artificielle.

## Composants électroniques

L'électronique joue un rôle fondamental dans le **fonctionnement** et l'**efficacité de l'intelligence artificielle** avec plusieurs composants clés tels que les <a href="https://fr.wikipedia.org/wiki/Processeur" target="_blank">CPU (Central Processing Units)</a>, <a href="https://fr.wikipedia.org/wiki/Processeur_graphique" target="_blank">GPU (Graphics Processing Units)</a>, <a href="https://fr.wikipedia.org/wiki/Tensor_Processing_Unit" target="_blank">TPU (Tensor Processing Units)</a>, <a href="https://fr.wikipedia.org/wiki/Application-specific_integrated_circuit" target="_blank">ASIC (Application-Specific Integrated Circuits)</a> et <a href="https://fr.wikipedia.org/wiki/Circuit_logique_programmable" target="_blank">FPGA (Field-Programmable Gate Array)</a>.

Chacun de ces composants a des **caractéristiques** et des **utilisations spécifiques** :
- **CPU** : **polyvalents**, ils sont utilisés pour des tâches de calcul général, mais peuvent être limités dans des opérations IA intensives.
- **GPU** : initialement conçus pour le **traitement graphique**, ils se sont révélés extrêmement efficaces pour le **parallélisme** nécessaire dans les calculs IA notamment dans le deep learning.
- **ASIC** : circuits intégrés personnalisés pour une **application spécifique**, ils offrent une **efficacité** et une **performance maximales** pour des tâches IA déterminées.
- **FPGA** : circuits intégrés qui peuvent être **configurés après leur fabrication**. Ils offrent une flexibilité importante par rapport aux ASICs.
- **TPU** : développés spécifiquement pour les opérations IA par Google pour sa bibliothèque <a href="https://www.tensorflow.org/?hl=fr" target="_blank">Tensorflow</a>, ils offrent une **efficacité accrue** pour les calculs liés aux réseaux neuronaux. Il s'agit d'un **type d'ASIC**.

Le choix entre ces composants implique un **compromis** entre **spécialisation** et **efficacité**. Les **CPU** et **GPU** offrent une **flexibilité plus grande**, les rendant utiles pour une variété de tâches. Les **ASICs**, **FPGA** et **TPU** peuvent offrir une **meilleure efficacité énergétique** et des **performances** accrues pour des **applications IA spécifiques**.

L'accès à ces composants spécialisés pour l'intelligence artificielle est souvent **mutualisé** à cause de son **coût élevé**. Il s'effectue souvent à travers un service de **cloud computing** privé (serveurs propres à l'entreprise ou à l'université) ou publique (serveurs mis à disposition par une entreprise) où les composants sont **virtualisés**.  

Les **circuits intégrés spécialisés** (ASIC et FPGA) sont souvent utilisés pour effectuer de l'**inférence** dans le cadre de projets  d'**intelligence artificielle embarquée** ou en **périphérie** (edge AI).

## Modes de déploiement des modèles

### Rôle central des API

Les API (Interfaces de Programmation d'Applications) jouent un rôle central à **toutes les étapes du cycle de vie** des modèles d'intelligence artificielle (IA) depuis la **récupération des données** jusqu'au **déploiement des modèles**. Leur rôle peut être décomposé en plusieurs aspects clés :

- **Récupération des Données**

Les API permettent l'accès aux **sources de données externes** comme les bases de données, les services web et les capteurs IoT. Grâce aux API, on peut **intégrer facilement diverses sources de données** dans des applications d'IA sans se soucier des différences entre les formats de données, les protocoles de communication ou les langages de programmation utilisés.

``````{admonition} API du service public 🇫🇷
Le site <a href="https://api.gouv.fr/" target="_blank">api.gouv.fr</a> référence les **API du service public** mises à la disposition des collectivités, des ministères, des entreprises et des citoyens pour construire des services **informatiques au service de tous**.

Pour appeler des API en Python, on peut utiliser la bibliothèque <a href="https://fr.python-requests.org" target="_blank">requests</a> (ou <a href="https://docs.python.org/fr/3/library/urllib.request.html#module-urllib.request" target="_blank">urllib.request</a> de la <a href="https://docs.python.org/fr/3/library/index.html" target="_blank">bibliothèque standard</a>). Voici l'exemple d'une <a href="https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol" target="_blank">requête GET</a> sur une API du service public pour connaître les jours fériés :
```python
import requests

url = "https://data.education.gouv.fr/api/explore/v2.1/catalog/datasets/fr-en-calendrier-scolaire/records"
# Paramètres à envoyer avec la requête
params = {
    'where': 'location="Grenoble" and start_date>2023',
    'order_by': 'start_date',
    'limit': 3,
}
response = requests.get(url, params=params)

# La réponse est en format JSON
data = response.json()
print(data)
```
La variable Python `data` contient un **dictionnaire** de valeurs.
``````

- **Automatisation des Pipelines d'IA**

Les API jouent un rôle crucial dans l'**automatisation des workflows d'IA**, en permettant la **communication** et la **coordination** entre différents services et composants du **pipeline**.

- **Déploiement des Modèles d'IA**

Les API sont essentielles pour intégrer les modèles d'IA dans des applications finales. Elles permettent aux applications de communiquer avec les modèles hébergés sur des serveurs ou dans le cloud pour **obtenir des prédictions**. Grâce aux API, les modèles d'IA peuvent être **accessibles depuis diverses plateformes et appareils**. Les API aident à gérer la sécurité et le trafic réseau lors de l'accès aux modèles d'IA. Elles peuvent implémenter des mécanismes d'authentification, de limitation de débit et d'autres contrôles de sécurité.

- **Mise à jour et Maintenance**

Les API facilitent la mise à jour des modèles d'IA sans perturber l'utilisation du service par les clients. Elles permettent également la **surveillance en temps réel** des performances des modèles et de leur utilisation.

### A l'aide de de serveurs informatiques distants (cloud computing)

Le cloud computing joue un rôle très important dans l'**entraînement** et le **déploiement des outils d'intelligence artificielle** pour plusieurs raisons :

- **Ressources informatiques évolutives**

Le cloud offre des ressources informatiques **évolutives et flexibles**. Les algorithmes d'IA, en particulier ceux impliquant l'apprentissage profond, nécessitent souvent des **capacités de traitement et de stockage importantes** que le cloud peut fournir **sans les contraintes d'une infrastructure physique**.

- **Accessibilité et collaboration**

Le cloud permet un **accès facile** et **à distance** aux ressources. Il **facilite la collaboration** entre les équipes dispersées géographiquement. Cela est particulièrement utile pour les projets d'IA où les **données**, les **modèles** et les **résultats** doivent être partagés et accessibles en **temps réel**.

- **Économies d'échelle**

Avec le cloud, les entreprises payent **uniquement pour les ressources utilisées** (décompté à la minute ou à l'heure d'utilisation). Cela rend la recherche en IA et le développement de solutions **plus accessibles** même pour les petites entreprises ou les startups.

- **Déploiement rapide**

Le cloud permet un **déploiement rapide** des modèles d'IA. Une fois qu'un modèle est entrainé, il peut être déployé sur le cloud pour une **utilisation immédiate** sans le temps et les efforts nécessaires pour configurer une infrastructure dédiée.

### A la périphérie pour l'inférence (Edge AI)

L'**intelligence artificielle en périphérie** fait référence à des systèmes d'IA **déployés localement** sur des appareils en périphérie du réseau comme des **smartphones**, des **capteurs IoT** ou des **microcontrôleurs** plutôt que de s'appuyer sur le cloud. Cette approche présente plusieurs avantages :
- réduction de la latence
- diminution de la bande passante nécessaire
- meilleure confidentialité des données

L'utilisation de frameworks tels que <a href="https://www.tensorflow.org/lite/microcontrollers?hl=fr" target="_blank">TensorFlow Lite pour les microcontrôleurs</a> dans le cadre de l'Edge AI présente des contraintes spécifiques. <a href="https://www.tensorflow.org/lite/guide?hl=fr" target="_blank">TensorFlow Lite</a> est une **version allégée de TensorFlow**, conçue pour les appareils à **faible puissance** et à **faible bande passante**.

### A la périphérie pour l'entrainement grâce à l'apprentissage fédéré (Federated AI)

L'<a href="https://fr.wikipedia.org/wiki/Apprentissage_f%C3%A9d%C3%A9r%C3%A9" target="_blank">apprentissage fédéré</a> est une **technique innovante** où l'entraînement des modèles d'IA est **distribué** sur **plusieurs appareils périphériques**. Cette méthode présente deux avantages principaux :

- **Confidentialité**

Les données sensibles restent sur l'appareil de l'utilisateur, il n'y a **pas de transferts de données** vers des serveurs centraux. Ce sont les **mises à jour du modèle** (gradients ou paramètres) qui sont envoyées aux serveurs centraux pour l'agrégation. Des techniques de <a href="https://fr.wikipedia.org/wiki/Confidentialit%C3%A9_diff%C3%A9rentielle" target="_blank">confidentialité différentielle</a> (differential privacy) peuvent être utilisées pour améliorer la confidentialité.

- **Efficacité des données**

L'apprentissage fédéré permet d'exploiter les données disponibles **localement** sur les appareils des utilisateurs. Ces données sont **contextuellement pertinentes** et conduisent donc à des modèles **plus précis et plus robustes**. Il n'est **pas nécessaire de transférer** de grandes quantités de données vers et depuis un serveur central. Cela **réduit la latence** et les **coûts associés à la transmission de données** sur le réseau.

___

L'apprentissage fédéré implique des **défis** en termes de **coordination** et de **communication** entre les appareils mais offre une voie prometteuse pour une IA **plus décentralisée** et **respectueuse de la vie privée**. De nombreuses recherches sont actuellement en cours pour résoudre les défis de l'apprentissage fédéré.

```{admonition} Bibliothèque Python 🐍
<a href="https://www.tensorflow.org/federated?hl=fr" target="_blank">TensorFlow Federated (TFF)</a> est un framework open-source conçu pour faciliter l'implémentation de l'apprentissage fédéré. TFF sert de plateforme pour la **recherche** et l'**expérimentation** dans le domaine de l'apprentissage fédéré permettant l'exploration de nouvelles techniques et approches dans ce domaine émergent.
```

## Langages de programmation

Les **langages de programmation** jouent un rôle fondamental dans le **développement de l'intelligence artificielle**. Chaque langage offre des avantages spécifiques adaptés à différents aspects de l'IA. Voici un aperçu détaillé des langages principaux :

- **Python**

Python est **extrêmement populaire dans le domaine de l'IA** pour sa **simplicité**, sa **lisibilité** et sa **vaste collection de bibliothèques spécialisées** comme TensorFlow, PyTorch, Scikit-learn, et Keras.

Sa capacité à **couvrir toute la chaîne de l'IA**, depuis la collecte des données jusqu'au déploiement, en passant par l'infrastructure et l'intégration avec des services cloud, en fait un **choix polyvalent pour toutes sortes d'entreprises**.

- **R**

Prisé par les **statisticiens et les chercheurs**, R est idéal pour l'**analyse de données et les statistiques**. Il est souvent utilisé dans la recherche et le développement de modèles statistiques complexes.

R dispose de nombreuses bibliothèques pour l'**analyse statistique et la visualisation de données** ce qui le rend idéal pour les **tâches d'exploration de données et de modélisation statistique**.

- **Java**

Utilisé principalement dans les **grandes entreprises**, Java est apprécié pour sa **robustesse**, sa **sécurité** et sa **portabilité**.

Sa capacité à gérer de **grandes applications** et sa compatibilité avec de nombreux systèmes le rendent adapté pour les systèmes d'IA dans des **environnements d'entreprise**.

- **C/C++**

Favorisé pour les **systèmes embarqués** et les applications nécessitant des **performances optimales** comme les jeux vidéo et les simulations.

C/C++ est reconnu pour sa rapidité et son contrôle à **bas niveau** ce qui est crucial pour les applications nécessitant une grande efficacité de traitement.

- **JavaScript**

Utilisé pour les applications d'**IA sur le web** notamment grâce à des bibliothèques comme TensorFlow.js.

JavaScript permet l'intégration d'**éléments d'IA directement dans les applications web**, rendant l'IA **plus accessible** sur les plateformes en ligne.

- **Julia**

Un **nouveau venu** dans le domaine de l'IA, connu pour sa **vitesse de traitement des données** et sa **facilité d'utilisation**.

Julia combine la **rapidité** des langages compilés comme C avec la **simplicité** des langages interprétés comme Python ce qui en fait un choix prometteur pour les calculs haute performance en IA.

Les langages Prolog et Lisp ont joué un rôle clé dans les **premiers jours de l'IA**, particulièrement dans le développement des **systèmes experts** et le **traitement du langage naturel**. Bien que leur **utilisation en entreprise soit limitée** aujourd'hui, surtout en comparaison avec Python, ils restent importants dans la **recherche académique** et pour **comprendre les fondements de l'IA**.